<?php
namespace Clarity\OnFleet\Helper;

use Anorgan\Onfleet\Response;
use Magento\Framework\App\ObjectManager;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use RuntimeException;

class API extends \Anorgan\Onfleet\Client
{
    /**
     * @var \Anorgan\Onfleet\Client
     */
    protected $client;
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * API constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->objectManager = ObjectManager::getInstance();
        $this->scopeConfig = $this->objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
        $apiKey = $this->getApiKey();

        parent::__construct($apiKey, $config);
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->scopeConfig->getValue(
            'clarity_onfleet/onfleet_api/onfleet_api_key',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue(
            'clarity_onfleet/onfleet_api/active',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param \Psr\Http\Message\UriInterface|string $uri
     * @param array $options
     */
    public function get($uri, array $options = [])
    {
        return parent::get($uri, $options + $this->getAuth());
    }

    /**
     * @return array|mixed|null
     */
    protected function getAuth()
    {
        return $this->getConfig('defaults');
    }

    /**
     * @param string|UriInterface $uri
     * @param array $options
     * @throws \Exception
     * @return ResponseInterface
     */
    public function post($uri, array $options = [])
    {
        try {
            $response = \GuzzleHttp\Client::post($uri, $options + $this->getAuth());
            return Response::fromResponse($response);
        } catch (ClientException $e) {
            $error   = Response::fromResponse($e->getResponse())->json(true);
            $message = $error['message']['message'];
            if (isset($error['message']['cause'])) {
                if (is_array($error['message']['cause'])) {
                    $message .= ' '.implode(', ', $error['message']['cause']);
                } else {
                    $message .= ' '.$error['message']['cause'];
                }
            }
            throw new RuntimeException('Error while calling post on '.$uri.': '.$message);
        }
    }

}