<?php

namespace Clarity\OnFleet\Helper;

use Anorgan\Onfleet\Task;
use Clarity\Kitchfix\Helper\Date;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;

class Data extends AbstractHelper
{
    /**
     * @var  \Anorgan\Onfleet\Client
     */
    protected $client;
    /**
     * @var Order
     */
    protected $order;
    /**
     * @var \Anorgan\Onfleet\Organization
     */
    protected $merchant;
    /**
     * @var OrderRepository
     */
    protected $orderRepository;
    /**
     * @var \Clarity\SubscriptionRandomizer\Model\Orders
     */
    protected $randomizedOrder;
    /**
     * @var \Clarity\Kitchfix\Helper\Data
     */
    private $dataHelper;
    /**
     * @var Date
     */
    private $dateHelper;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param API $client
     * @param OrderRepository $orderRepository
     * @param \Clarity\Kitchfix\Helper\Data $dataHelper
     * @param Date $dateHelper
     * @internal param \Clarity\SalesEmails\Helper\Data $salesEmailsHelper
     */
    public function __construct(
        Context $context,
        \Clarity\OnFleet\Helper\API $client,
        OrderRepository $orderRepository,
        \Clarity\Kitchfix\Helper\Data $dataHelper,
        \Clarity\Kitchfix\Helper\Date $dateHelper
    ) {
        $this->client = $client;
        $this->orderRepository = $orderRepository;
        $this->dataHelper = $dataHelper;
        $this->dateHelper = $dateHelper;

        parent::__construct($context);
    }

    /**
     * Verify if OnFleet API solution is Enabled
     *
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->client->isEnabled();
    }

    /**
     * Send order as Task to OnFleet
     *
     * @param $order
     * @return bool
     */
    public function sendTask($order)
    {
        if (!$this->isEnabled()) {
            return true;
        }
        try {
            $this->initOrders($order);

            if (!$this->isValidOrder($order)) {
                return false;
            }

            $task = $this->getTask();
            $this->_logger->error(json_encode($task));
            $result = $this->client->createTask($task);

            if ($result) {
                $this->updateOnFleetSentOrder($result);
            }
        } catch (\Exception $e) {
            $this->logError($e);
            $this->addErrorComment($e);
        }
    }

    /**
     * @param $order
     */
    protected function initOrders($order)
    {
        if ($this->dataHelper->isRandomizedOrder($order)) {
            $this->randomizedOrder = $order;
        }
        $this->order = $this->dataHelper->getRealOrder($order);
    }

    /**
     * @return int|null
     */
    protected function isValidOrder()
    {
        // Only Meals Store can be send to OnFleet
        if ($this->order->getStoreId()==\Clarity\Kitchfix\Helper\Data::STORE_GROCERY) {
            return false;
        }
        // Check if is not a GiftCard
        $isNotVirtual = !$this->dataHelper->orderHasGiftCard($this->order);
        // Check if is a randomized && Randomized Home Delivery
        $isRandomizedHomeDelivery = (bool)$this->randomizedOrder
            && $this->dataHelper->shippingMethodCodeIsHomeDelivery($this->order->getShippingMethod());

        // Check if is a common order without subscriptions
        $isCommonOrder = !(bool)$this->randomizedOrder && !$this->dataHelper->orderHasSubscription($this->order)
            && $this->dataHelper->shippingMethodCodeIsHomeDelivery($this->order->getShippingMethod());

        return $isNotVirtual && ($isRandomizedHomeDelivery || $isCommonOrder);
    }

    /**
     * @return array
     */
    private function getTask()
    {
        return Task::createAutoAssignedArray(
            $this->getDestination(),
            $this->getRecipient(),
            $this->getMerchant(),
            $this->getExecutor(),
            $this->getTaskNotes(),
            Task::AUTO_ASSIGN_MODE_DISTANCE,
            null, // Task AutoAssignTeam
            $this->getTaskCompleteAfter(),
            $this->getTaskCompleteBefore()
        );
    }

    /**
     * @return array
     */
    protected function getRecipient()
    {
        return [[
            'name'   => $this->getRecipientName(),
            'phone'  => $this->getRecipientPhone(),
            'notes'  => $this->getRecipientNotes()
        ]];
    }

    /**
     * @return \Anorgan\Onfleet\Destination
     */
    protected function getDestination()
    {
        return $this->client->createDestination([
            'address' => [
                'unparsed' => $this->getUnparsedShippingAddress(),
            ],
            'notes' => $this->getDestinationNotes()
        ]);
    }

    /**
     * @return string
     */
    protected function getUnparsedShippingAddress()
    {
        $shippingAddress = $this->order->getShippingAddress();

        $unparsedShippingAddress = $shippingAddress->getStreetLine(1) . " " . $shippingAddress->getStreetLine(2);
        $unparsedShippingAddress .= ','. $shippingAddress->getCity();
        $unparsedShippingAddress .= ','. $shippingAddress->getRegionCode();
        $unparsedShippingAddress .= ','. $shippingAddress->getCountryId();

        return $unparsedShippingAddress;
    }

    /**
     * @return mixed
     */
    private function getDestinationNotes()
    {
        return 'Delivery Instructions: ' . $this->order->getDeliveryInstructions()?: 'No' . "\n"
            . 'Leave Meals Unattended: ' . ($this->order->getLeaveMealsUnattended() ? 'Yes' : 'No');
    }

    /**
     * @return string
     */
    private function getTaskNotes()
    {
        $notes = "ORDER ID: {$this->order->getIncrementId()}";
        if ($this->randomizedOrder) {
            $notes .= "\nSUBSCRIPTION ID: {$this->randomizedOrder->getSubscriptionId()}";
        }
        return $notes;

    }

    /**
     * @return \Carbon\Carbon|Data
     */
    private function getTaskCompleteAfter()
    {
        return $this->getRealDeliveryDate()->addHour(5);
    }

    /**
     *
     * @return \Carbon\Carbon|false|int
     */
    private function getTaskCompleteBefore()
    {
        return null;
    }

    /**
     * @return \Carbon\Carbon|false|int
     */
    private function getRealDeliveryDate()
    {
        $deliveryDate = $this->order->getDeliveryDate();
        if ($this->randomizedOrder) {
            $deliveryDate = $this->randomizedOrder->getDeliveryDate();
        }
        return $this->dateHelper->getCarbonDate($deliveryDate);
    }

    /**
     * @return string
     */
    private function getRecipientName()
    {
        $billingAddress = $this->order->getBillingAddress();
        return $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
    }

    /**
     * @return string
     */
    private function getRecipientPhone()
    {
        return $this->order->getShippingAddress()->getTelephone();
    }

    /**
     * @return string
     */
    private function getRecipientNotes()
    {
        $deliveryType = $this->getRecipientNoteDeliveryType();
        $deliveryDate = $this->getRealDeliveryDate()->format('M, d Y');
        $deliveryTimeslot = $this->order->getDeliveryTimeslot();

        return $deliveryType . ' ' . $deliveryDate . ' ' . $deliveryTimeslot;
    }

    /**
     * @return \Anorgan\Onfleet\Organization
     */
    public function getMerchant()
    {
        if (!$this->merchant) {
            $this->merchant = $this->client->getMyOrganization();
        }
        return $this->merchant;
    }

    /**
     * @return \Anorgan\Onfleet\Organization
     */
    private function getExecutor()
    {
        return $this->getMerchant();
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    private function getRecipientNoteDeliveryType()
    {
        return $this->dataHelper->shippingMethodCodeIsHomeDelivery($this->order->getShippingMethod()) ? __('Home Delivery') : __('Hub Pick-Up');
    }

    /**
     * @param \Exception $e
     */
    protected function addErrorComment(\Exception $e)
    {
        $comment = 'An error has occurred with OnFleet: ' . $e->getMessage();
        $this->addComment($comment);
    }

    /**
     * @param $comment
     * @internal param $error
     */
    protected function addComment($comment)
    {
        $history = $this->order->addStatusHistoryComment($comment);
        $history->setIsVisibleOnFront(false);
        $history->setIsCustomerNotified(false);
        $history->save();
    }

    /**
     * @param \Exception $e
     */
    protected function logError(\Exception $e)
    {
        $error = "ORDER ID: ".$this->order->getEntityId()
            ."\n ERROR: ".$e->getMessage();

        $this->_logger->error($error);
    }

    /**
     * Update new column OnFleetSent update
     */
    private function updateOnFleetSentOrder($result)
    {
        if ($this->randomizedOrder) {
            $this->randomizedOrder->setOnfleetSent(true);
            $this->randomizedOrder->save();
            $comment = "Randomized ID {$this->randomizedOrder->getId()} was sent successful to OnFleet \n"
                . "OnFleet ID: {$result->getShortId()}\n"
                . "Tracking Url: {$result->getTrackingURL()}";
            $this->addComment($comment);
        } else {
            $this->order->setOnfleetSent(true);
            $this->orderRepository->save($this->order);
            $comment = "This order was sent successful to OnFleet \n"
                . "OnFleet ID: {$result->getShortId()}\n"
                . "Tracking Url: {$result->getTrackingURL()}";
            $this->addComment($comment);
        }
    }

}