<?php
namespace Clarity\OnFleet\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;
use Psr\Log\LoggerInterface;

class SendOrderTask implements ObserverInterface
{
    /**
     * @var \Clarity\OnFleet\Helper\Data
     */
    protected $helper;

    /**
     * SendOrderTask constructor.
     *
     * @param \Clarity\OnFleet\Helper\Data $helper
     */
    public function __construct(
        \Clarity\OnFleet\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @param EventObserver $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->helper->isEnabled()) {
            $this->helper->sendTask($observer->getOrder());
        }
    }
}