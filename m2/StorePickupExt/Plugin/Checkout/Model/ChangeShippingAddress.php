<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storepickup
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace Clarity\StorePickupExt\Plugin\Checkout\Model;

class ChangeShippingAddress extends \Magento\Checkout\Model\ShippingInformationManagement {

    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;
    /**
     * @var \Magento\Checkout\Api\ShippingInformationManagementInterface
     */
    protected $shippingInformationManagement;
    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @codeCoverageIgnore
     */
    protected $_checkoutSession;

    /**
     * @var \Magestore\Storepickup\Model\StoreFactory
     */
    protected $_storeCollection;
    /**
     * @var \Magento\Sales\Api\Data\OrderAddressInterface
     */
    protected $_orderAddressInterface;

    protected $_cartTotalsRepository;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magestore\Storepickup\Model\StoreFactory $storeCollection,
        \Magento\Sales\Api\Data\OrderAddressInterface $orderAddressInterface,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Checkout\Api\ShippingInformationManagementInterface $shippingInformationManagement,
        \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalsRepository
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_storeCollection = $storeCollection;
        $this->_orderAddressInterface = $orderAddressInterface;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->shippingInformationManagement = $shippingInformationManagement;
        $this->cartTotalsRepository = $cartTotalsRepository;
    }

    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $quote = $this->_checkoutSession->getQuote();
        $attributes = '';
        $shippingMethod = $addressInformation->getShippingMethodCode();

        $items = $quote->getAllItems();
        $subscription = false;
        foreach ($items as $item) {
            if ($item->getName() == 'Subscription') {
                $subscription = true;
            }
        }
        if ($subscription) {
            if($shippingMethod=="storedelivery"){
                $isChicago = $this->_checkoutSession->getIsChicago();
                if ($isChicago) {
                    // 1. Chicago – City
                    $addressInformation->setShippingMethodCode('storedelivery_ch');
                    $addressInformation->setShippingCarrierCode('storedelivery_ch');
                } else {
                    // 2. Chicago – Suburbs
                    $addressInformation->setShippingMethodCode('storedelivery_sb');
                    $addressInformation->setShippingCarrierCode('storedelivery_sb');
                }
            } else {
                $isFree = $this->_checkoutSession->getIsFreePickup();
                if ( ! $isFree) {
                    // 3. Hubs: Anything that's NOT labeled as "Home Delivery"
                    $addressInformation->setShippingMethodCode('storepickup_not_hd');
                    $addressInformation->setShippingCarrierCode('storepickup_not_hd');
                } else {
                    // 4. Kitchen + Gold Coast Store
                    $addressInformation->setShippingMethodCode('storepickup_kc_gcs');
                    $addressInformation->setShippingCarrierCode('storepickup_kc_gcs');
                }
            }
        } else {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $session = $objectManager->get('\Magento\Checkout\Model\Session');
            $attributes = $addressInformation->getExtensionAttributes();
            $session->setShippingAmount($attributes->getDeliveryPrice());
            $session->setBaseShippingAmount($attributes->getDeliveryPrice());
            /*if($shippingMethod=="storedelivery"){
                $attributes = $addressInformation->getExtensionAttributes();
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $session = $objectManager->get('\Magento\Checkout\Model\Session');
                if($attributes->getDeliveryPrice()) {
                    $totalDeliveryPrice = ($attributes->getDeliveryPrice() > $attributes->getDeliveryPriceExt())? $attributes->getDeliveryPrice(): $attributes->getDeliveryPriceExt();
                    $session->setShippingAmount($totalDeliveryPrice * 4);
                    $session->setBaseShippingAmount($totalDeliveryPrice * 4);
                }
            } else {
                $session->setShippingAmount($attributes->getDeliveryPrice());
                $session->setBaseShippingAmount($attributes->getDeliveryPrice());
            }*/
        }

        $extAttributes = $addressInformation->getExtensionAttributes();
        $deliveryDate = $extAttributes->getDeliveryDate();
        $productionDate = $this->getProductionDate($deliveryDate, $shippingMethod);
        $deliveryComment = $extAttributes->getDeliveryComment();
        $deliveryDate2 = $extAttributes->getDeliveryDateExt();
        $productionDate2 = $this->getProductionDate($deliveryDate2, $shippingMethod);
        $deliveryComment2 = $extAttributes->getDeliveryCommentExt();
        $deliveryInstructions = $extAttributes->getDeliveryInstructions();
        $deliveryTimeslot = $extAttributes->getDeliveryTimeslot();
        $deliveryTimeslotExt = $extAttributes->getDeliveryTimeslotExt();
        $leaveMealsUnattended = (int) $extAttributes->getLeaveMealsUnattended();

        $quote->setDeliveryDate( date("Y-m-d H:i:s",strtotime($deliveryDate)));
        $quote->setDeliveryComment($deliveryComment);
        $quote->setProductionDate($productionDate);
        $quote->setDeliveryDateExt( date("Y-m-d H:i:s",strtotime($deliveryDate2)));
        $quote->setDeliveryCommentExt($deliveryComment2);
        $quote->setProductionDateExt($productionDate2);
        $quote->setDeliveryInstructions($deliveryInstructions);
        $quote->setDeliveryTimeslot($deliveryTimeslot);
        $quote->setDeliveryTimeslotExt($deliveryTimeslotExt);
        $quote->setLeaveMealsUnattended($leaveMealsUnattended);

        if($attributes)
            $quote->setDeliveryData($attributes);
    }

    public function getProductionDate($date, $shippingMethod = '') {
        try {
            $hour = date("H",strtotime($date));
            $hour = (int)$hour;
            $date = date("Y-m-d",strtotime($date));

            if($shippingMethod != 'storedelivery' && $hour >= 0  &&  $hour < 12) {
                $date = new \DateTime($date);
                $date->sub(new \DateInterval('P1D'));
                $date  = $date->format('Y-m-d H:i:s');
            }
        } catch(\Exception $e) {
            $message = $e->getMessage();
        }
        return $date;
    }
}