<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     clarity_Storepickup
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:ObjectManager/etc/config.xsd">
    <preference for="Magestore\Storepickup\Model\StoreUrlPathGeneratorInterface" type="Magestore\StorePickupExt\Model\StoreUrlPathGenerator" />
    <preference for="Magestore\StorePickupExt\Model\StoreUrlRewriteGeneratorInterface" type="Magestore\StorePickupExt\Model\StoreUrlRewriteGenerator" />
    <type name="Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory">
        <arguments>
            <argument name="collections" xsi:type="array">
                <item name="storepickupext_store_listing_data_source" xsi:type="string">Clarity\StorePickupExt\Model\ResourceModel\Store\Grid\Collection</item>
                <item name="storepickupext_tag_listing_data_source" xsi:type="string">Clarity\StorePickupExt\Model\ResourceModel\Tag\Grid\Collection</item>
                <item name="storepickupext_holiday_listing_data_source" xsi:type="string">Clarity\StorePickupExt\Model\ResourceModel\Holiday\Grid\Collection</item>
                <item name="storepickupext_specialday_listing_data_source" xsi:type="string">Clarity\StorePickupExt\Model\ResourceModel\Specialday\Grid\Collection</item>
                <item name="storepickupext_schedule_listing_data_source" xsi:type="string">Clarity\StorePickupExt\Model\ResourceModel\Schedule\Grid\Collection</item>
            </argument>
        </arguments>
    </type>
    <virtualType name="StorepickupGridFilterPool" type="Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool">
        <arguments>
            <argument name="appliers" xsi:type="array">
                <item name="regular" xsi:type="object">Magento\Framework\View\Element\UiComponent\DataProvider\RegularFilter</item>
                <item name="fulltext" xsi:type="object">Magento\Framework\View\Element\UiComponent\DataProvider\FulltextFilter</item>
            </argument>
        </arguments>
    </virtualType>
    <type name="Clarity\StorePickupExt\Model\ResourceModel\Store\Grid\Collection">
        <arguments>
            <argument name="mainTable" xsi:type="string">magestore_storepickup_schedule</argument>
            <argument name="eventPrefix" xsi:type="string">storepickupext_store_grid_collection</argument>
            <argument name="eventObject" xsi:type="string">store_grid_collection</argument>
            <argument name="resourceModel" xsi:type="string">Clarity\StorePickupExt\Model\ResourceModel\Store</argument>
            <argument name="loadBaseimage" xsi:type="boolean">true</argument>
        </arguments>
    </type>
    <virtualType name="StoreGridDataProvider" type="Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider">
        <arguments>
            <argument name="collection" xsi:type="object" shared="false">Clarity\StorePickupExt\Model\ResourceModel\Store\Grid\Collection</argument>
            <argument name="filterPool" xsi:type="object" shared="false">StorePickupExtGridFilterPool</argument>
        </arguments>
    </virtualType>
    <type name="Clarity\StorePickupExt\Model\ResourceModel\Tag\Grid\Collection">
        <arguments>
            <argument name="mainTable" xsi:type="string">clarity_storepickupext_tag</argument>
            <argument name="eventPrefix" xsi:type="string">storepickupext_tag_grid_collection</argument>
            <argument name="eventObject" xsi:type="string">tag_grid_collection</argument>
            <argument name="resourceModel" xsi:type="string">Clarity\StorePickupExt\Model\ResourceModel\Tag</argument>
        </arguments>
    </type>
    <virtualType name="TagGridDataProvider" type="Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider">
        <arguments>
            <argument name="collection" xsi:type="object" shared="false">Clarity\StorePickupExt\Model\ResourceModel\Tag\Grid\Collection</argument>
            <argument name="filterPool" xsi:type="object" shared="false">StorePickupExtGridFilterPool</argument>
        </arguments>
    </virtualType>
    <type name="Clarity\StorePickupExt\Model\ResourceModel\Specialday\Grid\Collection">
        <arguments>
            <argument name="mainTable" xsi:type="string">clarity_storepickupext_specialday</argument>
            <argument name="eventPrefix" xsi:type="string">storepickupext_specialday_grid_collection</argument>
            <argument name="eventObject" xsi:type="string">specialday_grid_collection</argument>
            <argument name="resourceModel" xsi:type="string">Clarity\StorePickupExt\Model\ResourceModel\Specialday</argument>
        </arguments>
    </type>
    <virtualType name="SpecialdayGridDataProvider" type="Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider">
        <arguments>
            <argument name="collection" xsi:type="object" shared="false">Clarity\StorePickupExt\Model\ResourceModel\Specialday\Grid\Collection</argument>
            <argument name="filterPool" xsi:type="object" shared="false">StorePickupExtGridFilterPool</argument>
        </arguments>
    </virtualType>
    <type name="Clarity\StorePickupExt\Model\ResourceModel\Holiday\Grid\Collection">
        <arguments>
            <argument name="mainTable" xsi:type="string">clarity_storepickupext_holiday</argument>
            <argument name="eventPrefix" xsi:type="string">storepickupext_holiday_grid_collection</argument>
            <argument name="eventObject" xsi:type="string">holiday_grid_collection</argument>
            <argument name="resourceModel" xsi:type="string">Clarity\StorePickupExt\Model\ResourceModel\Holiday</argument>
        </arguments>
    </type>
    <virtualType name="HolidayGridDataProvider" type="Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider">
        <arguments>
            <argument name="collection" xsi:type="object" shared="false">Clarity\StorePickupExt\Model\ResourceModel\Holiday\Grid\Collection</argument>
            <argument name="filterPool" xsi:type="object" shared="false">StorePickupExtGridFilterPool</argument>
        </arguments>
    </virtualType>
    <type name="Clarity\StorePickupExt\Model\ResourceModel\Schedule\Grid\Collection">
        <arguments>
            <argument name="mainTable" xsi:type="string">magestore_storepickup_schedule</argument>
            <argument name="eventPrefix" xsi:type="string">storepickupext_schedule_grid_collection</argument>
            <argument name="eventObject" xsi:type="string">schedule_grid_collection</argument>
            <argument name="resourceModel" xsi:type="string">Clarity\StorePickupExt\Model\ResourceModel\Schedule</argument>
        </arguments>
    </type>
    <virtualType name="ScheduleGridDataProvider" type="Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider">
        <arguments>
            <argument name="collection" xsi:type="object" shared="false">Clarity\StorePickupExt\Model\ResourceModel\Schedule\Grid\Collection</argument>
            <argument name="filterPool" xsi:type="object" shared="false">StorePickupExtGridFilterPool</argument>
        </arguments>
    </virtualType>
</config>
