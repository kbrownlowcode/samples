define(
    [
        'jquery'
    ],
    function ($) {
        return {
            baseUrl:null,
            data:{},
            callBack:null,
            deliveryType:null,
            self:null,
            setBaseUrl:function(baseUrl) {
                this.baseUrl = baseUrl;
            },
            setDeliveryType: function (deliveryType) {
                this.deliveryType = deliveryType;
            },
            getDeliveryType: function () {
                return this.deliveryType;
            },
            setData:function(data) {
                this.data = data;
            },
            checkStore:function(callBack) {
                if(this.deliveryType == "pickup") {
                    $( "body" ).trigger( "beforeCheckStorePickup" );
                }
                this.callBack = callBack;
                this.sendAjax('storepickupext/zipcode');
            },
            checkDeliveryType: function (callBack) {
                this.sendAjax('storepickupext/deliverytype');
            },
            sendAjax:function(endPoint) {
                var self = this;
                if(this.deliveryType) {
                    this.data.delivery_type = this.deliveryType;
                }
                $.ajax({
                    url: this.baseUrl+endPoint,
                    type: "GET",
                    data: this.data,
                    dataType: "json",
                    success: function (d) {
                        if(self.callBack) {
                            self.callBack(d);
                        }
                    }
                });
            }
        }

    });