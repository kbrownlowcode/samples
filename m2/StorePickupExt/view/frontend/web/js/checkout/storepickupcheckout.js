define(
    [
        'jquery',
        'Magento_Checkout/js/model/quote',
        'mage/url',
        'mage/translate',
        'mage/template',
        'jquery/ui',
        'Clarity_StorePickupExt/js/checkout/storepickupcheckout'
    ],
    function ($,quote, url, $t, mageTemplate, newMap) {
        var liststoreJson = window.liststoreJson;
        var subscriptionQty = window.subscriptionInfo;
        var addressInformation = {};
        addressInformation.billing_address = {};
        addressInformation.shipping_address = {};
        addressInformation.extension_attributes = {};

        function getSelectListStoreHtml() {
            var $wrapperelectHtml = $('<div class="list-store-to-pickup"><label>' + $t('Select Store:') + '</label></div>');
            var $selectStoreHtml = $('<select class="list-store-select disable-ul"></select>');
            $selectStoreHtml.append('<option class="show-tag-li store-item" value="">' + $t('Select a store to pickup') + '</option>');
            $.each(liststoreJson, function (index, el) {
                $selectStoreHtml.append('<option class="show-tag-li store-item" value="' + el.storepickup_id + '">' + el.store_name + '</option>');
            });
            $wrapperelectHtml.append($selectStoreHtml);
console.log('deliveryblock');
            var deliveryblockbk = '   <div class="margin-bottom">' +
                '       <div class="arrow-wrap">' +
                '           <h4 class="btn-tumeric-arrow">Delivery Options</h4>' +
                '       </div>' +
                '       <p><strong>Your order qualifies for special delivery options.</strong></p>' +
                '       <p>Because you ordered over $70 worth of meals, you can split your delivery between two days. Please select your delivery day and time and tell us which items you\'d like delivered each day.</p>' +
                '   </div>';
            var deliveryblock =
                '<div id="delivery-content" style="display: block;">' +
                '   <div class="margin-bottom zip-wrapper">' +
                '       <label>Check Zip for Delivery Availibility</label>' +
                '       <input type="text" class="postcode_delivery form-lg grey"><button class="check_zip_btn">Check</button>' +
                '   </div>' +
                '   <div class="delivery_info">' +
                '   </div>' +
                '</div>';

            $wrapperelectHtml.append(deliveryblock);

            return $wrapperelectHtml;
        }
        //$("#pick-up-selection").click(function (value) {
        quote.shippingMethod.subscribe(function (value) {
            var storePickupInformation = "<div class ='storedelivery-information'></div>";
            console.log('quote.shippingMethod.subscribe_delivery');
            if (quote.shippingMethod().carrier_code == 'storedelivery') {
                $('.totals.fee.excl').show();
                $('.checkout-shipping-address2').show();

                $('.field-select-billing').show();
                $('.billing-address-details').show();
                $('.billing-address-form').parent().hide();
                $('.billing-address-form').show();
                $('.billing-address-same-as-shipping-block').show();
                $('.billing-address-same-as-shipping-block').attr('checked', true);
                $('.billing-address-same-as-shipping-block').trigger('click');

                $('#selected-shipping-method').val('storedelivery');

                if (!($('.info-store-checkout').length > 0) || $('.list-store-select').val() == "" || ((isDisplayPickuptime) && (($('#shipping_date').val() == '') || ($('#shipping_time').val() == '-1')))) {
                    if ($('.checkbox.active').length == 0) {
                        $('#shipping-method-buttons-container').hide();
                    }
                }
                if ((!$('.storedelivery-information').length > 0)) {
                //if (!($('.list-store-to-pickup').length > 0)) {
                    $('#checkout-shipping-method-load').append(storePickupInformation);
                    var deliveryblock =
                        '<div id="delivery-content" style="display: block;">' +
                        '   <div class="margin-bottom zip-wrapper">' +
                        '       <label>Check Zip for Delivery Availibility</label>' +
                        '       <input type="text" class="postcode_delivery form-lg grey"><button class="check_zip_btn">Check</button>' +
                        '   </div>' +
                        '   <div class="delivery_info">' +
                        '   </div>' +
                        '</div>';
                    $('.storedelivery-information').append(deliveryblock);
                    //$('.storedelivery-information').append(select_store_by_map);
                    // $('#select_store_by_map').click(function () {
                    //     $('#popup-mpdal').modal('openModal');
                    //     //if(typeof newMap != 'undefined'){
                    //     //var   map = newMap.getNewMap();
                    //     google.maps.event.trigger(newmap, "resize");  //Silver and Richard
                    //     //}
                    // });
                    $(".check_zip_btn").click(function(e){
                        e.preventDefault();
                        zipcodeLookup();
                    });
                    //change Store function
                    $('.list-store-select').change(function () {
                        $('#shipping-method-buttons-container').hide();
                        if ($('#shipping_date_div').length > 0) {
                            $('#shipping_date_div').show();
                            $('#shipping_date').hide();
                            $('#shipping_time_div').hide();
                        } else if (isDisplayPickuptime) {
                            // $('.storedelivery-information').append(storepickup_date_box);
                            // $("#shipping_date").change(function () {
                            //     $('#shipping-method-buttons-container').hide();
                            //     $('.overlay-bg-checkout').show();
                            //     showTimeBox($('#shipping_date').val(), $('.list-store-select').val());
                            // });
                        }
                        $('.overlay-bg-checkout').show();
                        $.each(liststoreJson, function (index, el) {
                            if (el.storepickup_id == $('.list-store-select').val()) {
                                storePikcuplatitude = el.latitude;
                                storePikcuplongitude = el.longitude;
                                var store_information = '<h3>' + el.store_name + '</h3><br/>' + '<p>' + $t('Store address: ') + el.address + '</p>' + '<p>' + $t('Store Phone: ') + el.phone + '</p>';
                                if ($('.info-store-checkout').length > 0) {
                                    $('.info-store-checkout').html(store_information);
                                    $('.info-store-checkout').show();
                                } else {
                                    var info_store = '<div class ="info-store-checkout">' + '<h3>' + el.store_name + '</h3><br/>' + '<p>' + $t('Store address: ') + el.address + '</p>' + '<p>' + $t('Store Phone: ') + el.phone + '</p>' + '</div>';
                                    $('#select_store_by_map').after(info_store);
                                }
                                $.ajax(
                                    {
                                        url: url.build("storepickup/checkout/changestore"),
                                        type: "post",
                                        dateType: "text",
                                        data: {
                                            store_id: $('.list-store-select').val()
                                        },
                                        success: function (result) {
                                            if (isDisplayPickuptime) showDateBox(); else {
                                                $('.overlay-bg-checkout').hide();
                                                $('#shipping-method-buttons-container').show();
                                            }
                                        }
                                    });
                            }
                        });
                        if ($('.list-store-select').val() == "") {
                            $('.overlay-bg-checkout').hide();
                            $('.info-store-checkout').hide();
                            if ($('#shipping_date_div').length > 0) $('#shipping_date_div').hide();
                            if ($('#shipping_time_div').length > 0) $('#shipping_time_div').hide();
                        }
                    });
                }
                $.each(liststoreJson, function (index, el) {
                    if (el.storepickup_id == defaultStore && !($('.info-store-checkout').length > 0)) {
                        $('.list-store-select').val(defaultStore).trigger('change');
                    }
                });
                $('.storedelivery-information').show();
            } else {
                $('.storedelivery-information').hide();
                $('.totals.fee.excl').hide();
            }

        });

        quote.paymentMethod.subscribe(function () {
            console.log('quote.paymentMethod.subscribe.delivery');
            if (quote.shippingMethod() && quote.shippingMethod().carrier_code == 'storedelivery') {
                $('.field-select-billing').show();
                $('.billing-address-details').show();
                $('.billing-address-form').parent().hide();
                $('.billing-address-form').show();
                $('.billing-address-same-as-shipping-block').show();
                $('.billing-address-same-as-shipping-block').attr('checked', true);
                $('.billing-address-same-as-shipping-block').trigger('click');


                $('#payment .form-login').hide();
                $('#selected-shipping-method').val('storedelivery');

                $('.action-update').click(function () {
                    $('.billing-address-form').hide();
                    $('.billing-address-details').show();
                    $('.action-update').hide();
                });
                $('.action-edit-address').click(function () {
                    $('.action-update').show();
                    $('.billing-address-form').show();
                });
            }
        });

        zipcodeLookup = function() {
            console.log('zipcodeLookup');
            //$("#checkout").css("display","block");
            var val = $(".postcode_delivery").val();
            if(val == null || val=='')
                return;

            var data = {zipcode:val};
            $.ajax({
                url: url.build("storepickupext/metrics/table"),
                type:"GET",
                data:data,
                success: function(d){
                    var html = d;
                    $(".delivery_info").html(html);
                    //$("#checkout").css("display","none");
                    bindTimeSlotClick();

                    var gratuityBlock =

                    '<div class="delivery-options-after-calendar">' +
                    '   <div class="leave-meals-unattended-wrapper">' +
                    '       <input type="checkbox" name="leave_meals_unattended" id="leave_meals_unattended" value="1"/><label for="leave_meals_unattended">OK to leave meals unattended</label>' +
                    '   </div>' +
                    '   <div class="delivery-instructions-wrapper">' +
                    '       <label>Please add any instructions for delivery and where to leave your meals in case you aren\'t home.</label>' +
                    '       <textarea id="delivery_instructions" name="delivery_instructions" rows="7"></textarea>' +
                    '   </div>' +
                    '</div>' +


                    '<div class="margin-bottom gratuity-wrapper">' +
                        '<div class="arrow-wrap">' +
                        '<h4 class="btn-tumeric-arrow">Gratuity</h4>' +
                        '</div>' +
                        '<p>' +
                        'Leave your delivery drive a tip.' +
                    '</p>' +
                    '<form id="tip_fee_form" method="post" action="#">' +
                    '    <div class="input-group chia-seed">' +
                        '<ul class="gratuity-options"><li rel=".10">10%</li><li rel=".15">15%</li><li rel=".20">20%</li><li rel=".25">25%</li></ul>' +
                    '    <select name="tip_fee" id="gratuity-dropbox" class="form-control form-lg grey" style="display: none">' +
                    '    <option value="">SELECT ONE</option>' +
                    '<option value=".10">%10</option>' +
                    '    <option value=".15">%15</option>' +
                    '    <option value=".20">%20</option>' +
                    '    <option value=".25">%25</option>' +
                    '    </select>' +
                    '    <span class="input-group-btn">' +
                    '    <button id="tip_fee_btn"' +
                    'class="btn"' +
                    'type="button">Apply Tip</button>' +
                    '</span>' +
                    '</div>' +
                    '</form>' +
                    '</div>';

                    $(".delivery_info").append(gratuityBlock);

                    $('ul.gratuity-options').on('click', 'li', function() {
                        var self = $(this);
                        var value = self.attr('rel');
                        $('ul.gratuity-options li').removeClass('selected');
                        self.addClass('selected');
                        $('#gratuity-dropbox').val(value);

                    })

                    $("#tip_fee_btn").click(function(e) {
                        $('.overlay-bg-checkout').show();
                        console.log('tip_fee_form_submit_triggered');
                        $.ajax({
                            type: "POST",
                            url: url.build("storetips/addtip"),
                            data: $("#tip_fee_form").serialize(), // serializes the form's elements.
                            success: function(data)
                            {
                                //alert(data); // show response from the php script.
                            }
                        });

                        e.preventDefault(); // avoid to execute the actual submit of the form.
                        $('.overlay-bg-checkout').hide();
                    });

                    var selectedElement = $(".delivery-option-times .active span");
                    addressInformation.extension_attributes.delivery_price = selectedElement.attr("value");
                    addressInformation.extension_attributes.delivery_date = selectedElement.attr("date");
                    var val = selectedElement.html();
                    if(val) {
                        val = val.replace(/\s/g,'');
                        val = val.substring(1);
                        val = parseInt(val);
                        $(".cart-box .delivery_cost").html("$"+(formatMoney(val,2,'.',',')));
                    }
                    $(".cart-box .time-description").html(selectedElement.attr("time_slot"));
                    //runTotal();

                }
            });
        };

        bindTimeSlotClick = function() {
            console.log('bindTimeSlotClick');
            var lastTimeSlot = '';

            $('.delivery-option-times .checkbox').click(function(){
                console.log('delivery-option-times-click');
                $('#shipping-method-buttons-container').show();

                var element = $(this).find("span"),
                    deliveryType = 'Delivery',
                    qty = 0,
                    deliveryOptionTimes = $('.delivery-option-times .checkbox');

                deliveryOptionTimes.each(function () {
                    if ($(this).hasClass('active')) {
                        qty++;
                    }
                });

                if (subscriptionQty <= 5) {
                    deliveryOptionTimes.removeClass("active");

                    $('[name="storepickup_id"]').val(element.attr("storepickup_id"));
                    $('[name="delivery_price"]').val(element.attr("value"));
                    $('[name="delivery_date"]').val(element.attr("date"));
                    $('[name="delivery_timeslot"]').val(element.attr("time_slot"));
                    $('[name="delivery_comment"]').val(element.attr("time_slot")+" "+deliveryType);

                } else {
                    deliveryOptionTimes.each(function () {
                        if ($(this).hasClass('active')) {
                            if ($(this).find("span").attr("time_slot") != lastTimeSlot.attr("time_slot")) {
                                $(this).removeClass("active")
                            }
                        }
                    });

                    if (lastTimeSlot == '') {
                        $('[name="storepickup_id"]').val(element.attr("storepickup_id"));
                        $('[name="delivery_price"]').val(element.attr("value"));
                        $('[name="delivery_timeslot"]').val(element.attr("time_slot"));
                        $('[name="delivery_date"]').val(element.attr("date"));
                        $('[name="delivery_comment"]').val(element.attr("time_slot")+" "+deliveryType);
                    } else if (element.attr("date") > lastTimeSlot.attr("date")) {
                        $('[name="delivery_price_ext"]').val(element.attr("value"));
                        $('[name="delivery_date_ext"]').val(element.attr("date"));
                        $('[name="delivery_timeslot_ext"]').val(element.attr("time_slot"));
                        $('[name="delivery_comment_ext"]').val(element.attr("time_slot")+" "+deliveryType);

                        $('[name="storepickup_id"]').val(lastTimeSlot.attr("storepickup_id"));
                        $('[name="delivery_price"]').val(lastTimeSlot.attr("value"));
                        $('[name="delivery_date"]').val(lastTimeSlot.attr("date"));
                        $('[name="delivery_timeslot"]').val(lastTimeSlot.attr("time_slot"));
                        $('[name="delivery_comment"]').val(lastTimeSlot.attr("time_slot")+" "+deliveryType);

                    } else {
                        $('[name="storepickup_id"]').val(element.attr("storepickup_id"));
                        $('[name="delivery_price"]').val(element.attr("value"));
                        $('[name="delivery_date"]').val(element.attr("date"));
                        $('[name="delivery_timeslot"]').val(element.attr("time_slot"));
                        $('[name="delivery_comment"]').val(element.attr("time_slot")+" "+deliveryType);

                        $('[name="delivery_price_ext"]').val(lastTimeSlot.attr("value"));
                        $('[name="delivery_date_ext"]').val(lastTimeSlot.attr("date"));
                        $('[name="delivery_timeslot_ext"]').val(lastTimeSlot.attr("date"));
                        $('[name="delivery_comment_ext"]').val(lastTimeSlot.attr("time_slot")+" "+deliveryType);
                    }
                }

                var val = element.html();
                val = val.replace(/\s/g,'');
                if(val) {
                    val = val.substring(1);
                    val = parseInt(val);
                    $(".cart-box .delivery_cost").html("$"+(formatMoney(val,2,'.',',')));
                }
                $(".cart-box .time-description").html(element.attr("time_slot"));

                if (qty > 1) {
                    deliveryOptionTimes.each(function () {
                        if ($(this).hasClass('active')) {
                            if ($(this).hasClass('active')) {
                                $(this).removeClass("active");
                            }
                        }
                        if ($(this).find("span").attr("date") == element.attr("date") ||
                                $(this).find("span").attr("date") == lastTimeSlot.attr("date")) {
                            $(this).addClass("active");
                        }
                    });
                } else {
                    $('[name="shipping_cost"]').each(function(){
                     element.closest('div').removeClass('active');
                     });
                     element.closest('div').addClass('active');
                }
                lastTimeSlot = element;
                //runTotal();
            });
        };

        formatMoney = function(val ,c, d, t) {
            var n = val,
                c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "." : d,
                t = t == undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

        function showDateBox() {

            $.ajax({
                url: url.build("storepickup/checkout/disabledate"),
                type: "post",
                dateType: "json",
                data: {
                    store_id: $('.list-store-select').val()
                },
                success: function (result) {
                    result = $.parseJSON(result);
                    $("#shipping_date").val("");
                    $("#shipping_date").datepicker("destroy");
                    $("#shipping_date").datepicker({
                        minDate: -0,
                        dateFormat: 'mm/dd/yy',
                        beforeShowDay: function (day) {
                            var formatdate = $.datepicker.formatDate('mm/dd/yy', day);
                            return [($.inArray(formatdate, result.holiday) == -1) && ($.inArray(day.getDay(), result.schedule) == -1)];
                        }
                    });
                    $('#shipping_date').show();
                    $('.overlay-bg-checkout').hide();
                }
            });
        }
        function showTimeBox(shipping_date_val, store_id_val) {
            if (!($('#shipping_time_div').length > 0)) {
                $('.storedelivery-information').append(storepickup_time_box);
                //change Time function
                $("#shipping_time").change(function () {
                    if (($("#shipping_time").val() != '-1') && ($('#shipping_date_div').length > 0)) {
                        $('.overlay-bg-checkout').show();
                        $.ajax({
                            url: url.build("storepickup/checkout/changetime"),
                            type: "post",
                            dateType: "json",
                            data: {
                                store_id: $('.list-store-select').val(),
                                shipping_date: $("#shipping_date").val(),
                                shipping_time: $("#shipping_time").val()
                            },
                            success: function (result) {
                                $('#shipping-method-buttons-container').show();
                                $('.overlay-bg-checkout').hide();
                            }
                        });
                    } else $('#shipping-method-buttons-container').hide();
                });
            }
            $('#shipping_time_div').show();
            $('#shipping_time').hide();

            $.ajax({
                url: url.build("storepickup/checkout/changedate"),
                type: "post",
                dateType: "json",
                data: {
                    shipping_date: shipping_date_val,
                    store_id: store_id_val
                },
                success: function (result) {
                    result = $.parseJSON(result);
                    $('#shipping_time').html("");
                    var selecttime = '<option value="-1">Select time to pickup</option>';
                    if (!result.error) {
                        $('#shipping_time').append(selecttime + result.html);
                        $('#shipping_time').show();
                        $('.overlay-bg-checkout').hide();
                    } else alert(result.error);
                }
            });
        }
    });