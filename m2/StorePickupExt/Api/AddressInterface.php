<?php
/**
 * Created by PhpStorm.
 * User: anthonygordon
 * Date: 6/8/17
 * Time: 8:45 AM
 */

namespace Clarity\StorePickupExt\Api;

interface AddressInterface {


    /**
     * Get id
     *
     * @return int|null
     */
    public function getCustomShippingPrice();

    /**
     * Set id
     *
     * @param int $id
     * @return $this
     */
    public function setCustomShippingPrice($id);

}