<?php
namespace Clarity\StorePickupExt\Setup;


use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('clarity_shipping_timeslots')
        )->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Customer Subscription Log ID'
        )->addColumn(
            'package_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Package ID'
        )->addColumn(
                'timeslot_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Timeslot Time'
            )->addColumn(
                'monday_cost',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Monday Cost'
            )->addColumn(
                'tuesday_cost',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Tuesday Cost'
            )->addColumn(
                'wednesday_cost',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Wednesday Cost'
            )->addColumn(
                'thursday_cost',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Thursday Cost'
            )->addColumn(
                'thursday_cost',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Thursday Cost'
            )->addColumn(
                'friday_cost',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Friday Cost'
            )->addColumn(
                'saturday_cost',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Saturday Cost'
            )->addColumn(
                'sunday_cost',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Sunday Cost'
            )->addColumn(
                'time',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Time'
            )->setComment(
                'Table To Show Price'
            );
        $setup->getConnection()->createTable($table);

        $table = $setup->getConnection()->newTable(
            $setup->getTable('clarity_timeslot_package_store')
        )->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Customer Subscription Log ID'
            )->addColumn(
                'store_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [],
                'Store ID'
            )->addColumn(
                'package_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [],
                'Package ID'
            )->setComment(
                'Package Table'
            );
        $setup->getConnection()->createTable($table);

        $table = $setup->getConnection()->newTable(
            $setup->getTable('clarity_timeslot_packages')
        )->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Customer Subscription Log ID'

        )->addColumn(
            'is_pickup_only',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            [],
            'Package Name'
        )->addColumn(
                'package_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Package Name'
            )->setComment(
                'Package Name'
            );
        $setup->getConnection()->createTable($table);

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'delivery_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Delivery Date',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'delivery_comment',
            [
                'type' => 'text',
                'nullable' => false,
                'comment' => 'Delivery Comment',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'production_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Production Date',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_invoice'),
            'delivery_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Delivery Date',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_invoice'),
            'production_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Production Date',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'delivery_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Delivery Date',
            ]
        );

                $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'production_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Production Date',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'delivery_comment',
            [
                'type' => 'text',
                'nullable' => false,
                'comment' => 'Delivery Comment',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'delivery_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Delivery Date',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'production_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Production Date',
            ]
        );

        $setup->endSetup();
    }
}