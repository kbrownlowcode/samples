<?php
/**
 * Created by PhpStorm.
 * Company: Rapicart
 * Web: https://www.rapicart.com
 * User: Pablo Garcia
 * Email: pablo.garcia@rapicart.com
 * Date: 15/11/17
 * Time: 21:08
 */

namespace Clarity\StorePickupExt\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context){
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.1') < 0) {

            // Get module table
            //$tableName = $setup->getTable('table_name');

            // Check if the table already exists
            //if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $installer->getConnection()->addColumn(
                    $installer->getTable('quote'),
                    'delivery_date_ext',
                    [
                        'type' => 'datetime',
                        'nullable' => false,
                        'comment' => 'Delivery Date',
                    ]
                );

                $installer->getConnection()->addColumn(
                    $installer->getTable('quote'),
                    'delivery_comment_ext',
                    [
                        'type' => 'text',
                        'nullable' => false,
                        'comment' => 'Delivery Comment',
                    ]
                );

                $installer->getConnection()->addColumn(
                    $installer->getTable('quote'),
                    'production_date_ext',
                    [
                        'type' => 'datetime',
                        'nullable' => false,
                        'comment' => 'Production Date',
                    ]
                );

                $installer->getConnection()->addColumn(
                    $installer->getTable('sales_invoice'),
                    'delivery_date_ext',
                    [
                        'type' => 'datetime',
                        'nullable' => false,
                        'comment' => 'Delivery Date',
                    ]
                );

                $installer->getConnection()->addColumn(
                    $installer->getTable('sales_invoice'),
                    'production_date_ext',
                    [
                        'type' => 'datetime',
                        'nullable' => false,
                        'comment' => 'Production Date',
                    ]
                );

                $installer->getConnection()->addColumn(
                    $installer->getTable('sales_order'),
                    'delivery_date_ext',
                    [
                        'type' => 'datetime',
                        'nullable' => false,
                        'comment' => 'Delivery Date',
                    ]
                );

                $installer->getConnection()->addColumn(
                    $installer->getTable('sales_order'),
                    'production_date_ext',
                    [
                        'type' => 'datetime',
                        'nullable' => false,
                        'comment' => 'Production Date',
                    ]
                );

                $installer->getConnection()->addColumn(
                    $installer->getTable('sales_order'),
                    'delivery_comment_ext',
                    [
                        'type' => 'text',
                        'nullable' => false,
                        'comment' => 'Delivery Comment',
                    ]
                );

            //}
        }

        $setup->endSetup();
    }
}