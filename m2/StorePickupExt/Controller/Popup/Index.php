<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Clarity\StorePickupExt\Controller\Popup;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Index extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;
    protected $_objectManager;
    protected $_store;
    protected $_selectedPackage;
    protected $_request;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magento\Framework\App\Request\Http $request
    )
    {
        $this->pageFactory = $pageFactory;
        $this->_request = $request;
        return parent::__construct($context);
    }
    /**
     * Initialize coupon
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute() {
        $resultPage = $this->pageFactory->create();
        $blockInstance = $resultPage->getLayout()->getBlock('deliverymethod.popup');
        echo $blockInstance->toHtml();
        exit;
    }
}
