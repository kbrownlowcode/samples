<?php
namespace Clarity\StorePickupExt\Controller\Zipcode;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;
    protected $_objectManager;
    protected $_storeCollection;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magestore\Storepickup\Model\ResourceModel\Store\Collection $storeCollection
    )
    {
        $this->_storeCollection = $storeCollection;
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute() {
        $isQulified = false;
        $storeInfo = array();
        $storeInfo['delivery_selected'] = false;
        $returnHtml = false;
        if(isset($_GET['return_html']) && $_GET['return_html']) {
            $returnHtml = true;
        }
        $qualifiedStore = '';
        if(isset($_GET['zipcode']) && $_GET['zipcode']) {
            $qualifiedStore = $this->findStoreByZip($_GET['zipcode']);
        } else if(isset($_GET['store_id']) && $_GET['store_id']) {

            $qualifiedStore= $this->findStoreById($_GET['store_id']);
        }
        if($qualifiedStore) {
            $isQulified = true;
            $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
            $storeManager = $this-> _objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $redirectUrl = $storeManager->getStore()->getCurrentUrl(false);
            $storeInfo = array();
            $storeInfo['id'] = $qualifiedStore->getId();
            $storeInfo['zipcode'] = $qualifiedStore->getZipcode();
            $storeInfo['fulladdress'] = $qualifiedStore->getAddress().", ".
            $qualifiedStore->getCity().", ".$qualifiedStore->getState().", ".
            $qualifiedStore->getZipcode();
            if(isset($_GET['delivery_type']) && $_GET['delivery_type']) {
                $storeInfo['delivery_type'] = $_GET['delivery_type'];
            }
            if(isset($_GET['delivery_price']) && $_GET['delivery_price']) {
                $storeInfo['delivery_price'] = $_GET['delivery_price'];
            }
            if(isset($_GET['delivery_date']) && $_GET['delivery_date']) {
                $storeInfo['delivery_date'] = $_GET['delivery_date'];
            }
            $storeInfo['delivery_selected'] = true;
            $checkoutSession->setData("storeInfo", $storeInfo);
        }
        if($returnHtml) {
            $resultPage = $this->pageFactory->create();
            $blockInstance = $resultPage->getLayout()->getBlock('deliverymethod.message');
            if($blockInstance) {
                if($isQulified) {
                    $blockInstance->setData('message','Congratulations! The chosen zip code is qualified for delivery.');
                } else {
                    $blockInstance->setData('message','The chosen zip code is not qualified for delivery.');
                }
                $data = array();
                $data['html'] = $blockInstance->toHtml();
                echo json_encode($data);
                exit;
            }
        }
        echo $isQulified;
        exit;
    }

    protected function findStoreByZip() {
        $isQulified = false;
        $qualifiedStore = '';
        if(isset($_GET['zipcode'])) {
            $zipcode = trim($_GET['zipcode']);
            $this->_storeCollection = $this->addTagsToFilter($this->_storeCollection,['Delivery']);
            $this->_storeCollection->addFieldToFilter('zipcode', ['eq' => "$zipcode"]);
            $this->_storeCollection->load();
            if(!$this->_storeCollection->count()) {
                return false;
            }
        }
        return $this->_storeCollection->getFirstItem();
    }

    protected function findStoreById() {
        $isQulified = false;
        $qualifiedStore = '';
        if(isset($_GET['store_id'])) {
            $id = trim($_GET['store_id']);
            foreach($this->_storeCollection as $store) {
                $storeId = trim($store->getId());
                if($storeId == $id) {
                    $isQulified = true;
                    $qualifiedStore = $store;
                    break;
                }
            }
        }
        return $qualifiedStore;
    }


    protected function addTagsToFilter($collection, $tagNames = array()) {
        $connection = $collection->getResource()->getConnection();

        $select = $connection->select()->from(
            $collection->getTable(\Magestore\Storepickup\Setup\InstallSchema::SCHEMA_TAG),
            'tag_id'
        )->where(
            'tag_name IN(?)',
            $tagNames
        );
        $tagIds =  $connection->fetchCol($select);
        $select = $connection->select()->from(
            $collection->getTable(\Magestore\Storepickup\Setup\InstallSchema::SCHEMA_STORE_TAG),
            'storepickup_id'
        )->where(
            'tag_id IN(?)',
            $tagIds
        );
        $collection->addFieldToFilter('storepickup_id', ['in' => $connection->fetchCol($select)]);
        return $collection;
    }
}
?>