<?php
namespace Clarity\StorePickupExt\Controller\Deliverytype;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;
    protected $_objectManager;
    protected $_store;
    protected $_selectedPackage;
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magestore\Storepickup\Model\Store $store
    )
    {
        $this->pageFactory = $pageFactory;
        $this->_store = $store;
        return parent::__construct($context);
    }

    public function execute() {
        $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
        if(isset($_GET['deliverytype']) && $_GET['deliverytype']) {
            $checkoutSession->setData('store_delivery_option',$_GET['deliverytype']);
        }
        exit;
    }
}
?>