<?php
namespace Clarity\StorePickupExt\Controller\Adminhtml\Metrics;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;


class Table extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;
    protected $_objectManager;
    protected $_storeCollection;
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magestore\Storepickup\Model\ResourceModel\Store\Collection $storeCollection
    )
    {
        $this->pageFactory = $pageFactory;
        $this->_storeCollection = $storeCollection->addTagsToFilter(array('3'));;
        return parent::__construct($context);
    }

    public function execute() {
        $isQulified = "false";
        $resultPage = $this->pageFactory->create();
        //$blockInstance = $resultPage->getLayout()->getBlock('storepickupCheckoutMetrics')->setData('shipping_type','delivery');;
        $blockInstance = $resultPage->getLayout()->createBlock(
            'Clarity\StorePickupExt\Block\Wrapper'
        )->setData('area', 'frontend')
            ->setData('shipping_type','delivery')
            ->setData('order_id',$this->getRequest()->getParam('order'))
            ->setTemplate('Clarity_StorePickupExt::storepickupcheckouttable.phtml');
        $zipcode = '';
        if(isset($_GET['zipcode']) && $_GET['zipcode']) {
            $zipcode = $_GET['zipcode'];
        }
        foreach($this->_storeCollection as $store) {
            if(trim($store->getZipcode()) == $zipcode) {
                $obj =   \Magento\Framework\App\ObjectManager::getInstance();
                $storePackage = $obj->create('Clarity\StorePickupExt\Model\StorePackage')->load($store->getId(),'store_id');
                $checkoutSession = $obj->get('Magento\Checkout\Model\Session');
                $storepickup_session = array();
                $storepickup_session['store_id'] = $store->getId();
                $blockInstance->setData('store_id', $store->getId());
                $checkoutSession->setData('storepickup_session',$storepickup_session);
                if($storePackage && $storePackage->getPackageId()) {
                    $blockInstance->setPackageId($storePackage->getPackageId());
                    break;
                }
            }
        }
        echo $blockInstance->toHtml();
        exit;
    }
}
?>