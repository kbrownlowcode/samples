<?php
namespace Clarity\StorePickupExt\Controller\Adminhtml\Metrics;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;


class Pickup extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;
    protected $_objectManager;
    protected $_store;
    protected $_selectedPackage;
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magestore\Storepickup\Model\Store $store
    )
    {
        $this->pageFactory = $pageFactory;
        $this->_store = $store;
        return parent::__construct($context);
    }

    public function execute() {
        $isQulified = "false";
        $resultPage = $this->pageFactory->create();
        //$blockInstance = $resultPage->getLayout()->getBlock('storepickupCheckoutMetrics')->setData('shipping_type','pickup');
        $blockInstance = $resultPage->getLayout()->createBlock(
            'Clarity\StorePickupExt\Block\Wrapper'
        )->setData('area', 'frontend')
            ->setData('shipping_type','pickup')
        ->setTemplate('Clarity_StorePickupExt::storepickupcheckouttable.phtml');
        if(isset($_GET['storeId']) && $_GET['storeId']) {
            $obj =   \Magento\Framework\App\ObjectManager::getInstance();
            $this->_store->load($_GET['storeId']);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $storePackageCollection = $objectManager
                ->create('Clarity\StorePickupExt\Model\ResourceModel\StorePackage\Collection');
            $storePackageCollection->addFieldToFilter('store_id',['eq'=>$this->_store->getId()]);

            foreach($storePackageCollection as $storePackage) {
                $package = $objectManager
                    ->create('Clarity\StorePickupExt\Model\Package');
                $package->load($storePackage->getPackageId());
                if($package && $package->getIsPickupOnly()) {
                    $this->_selectedPackage = $package;
                    break;
                }
            }
            $storepickup_session = array();
            $storepickup_session['store_id'] = $this->_store->getId();
            $checkoutSession = $obj->get('Magento\Checkout\Model\Session');
            $checkoutSession->setData('storepickup_session',$storepickup_session);
            if($this->_selectedPackage && $this->_selectedPackage->getId()) {
                $blockInstance->setPackageId($this->_selectedPackage->getId());
            }
            echo $blockInstance->toHtml();
        }
        exit;
    }
}
?>