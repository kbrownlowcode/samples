<?php

/**
 * Magestore.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storepickup
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace Clarity\StorePickupExt\Controller\Adminhtml\Schedule;

use Magento\Framework\Controller\ResultFactory;

/**
 * Index Grid Schedule Action.
 *
 * @category Magestore
 * @package  Magestore_Storepickup
 * @module   Storepickup
 * @author   Magestore Developer
 */
class Timeslots extends \Clarity\StorePickupExt\Controller\Adminhtml\Schedule
{
    /**
     * Index action.
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if($_POST) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $schedule = $objectManager
                ->get('Clarity\StorePickupExt\Model\Schedule');
            $request =  $this->_request->getPost();

            if(!$_POST['id'] || !isset($_POST['id']))
                unset($_POST['id']);
            $packageId = $this->_session->getPackageId();
            $schedule->setData($_POST);
            $schedule->setPackageId($packageId);
            $schedule->save();
        }
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $this->initPage($resultPage);
        $resultPage->getConfig()->getTitle()->prepend(__('New Time Slot'));

        return $resultPage;
    }
}
