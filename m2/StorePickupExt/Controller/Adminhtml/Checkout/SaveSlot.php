<?php
/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Giftvoucher
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace Clarity\StorePickupExt\Controller\Adminhtml\Checkout;

class SaveSlot extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;
    /**
     * @var \Magento\Checkout\Api\ShippingInformationManagementInterface
     */
    protected $shippingInformationManagement;
    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @codeCoverageIgnore
     */
    protected $_checkoutSession;

    /**
     * @var \Magestore\Storepickup\Model\StoreFactory
     */
    protected $_storeCollection;
    /**
     * @var \Magento\Sales\Api\Data\OrderAddressInterface
     */
    protected $_orderAddressInterface;

    protected $_cartTotalsRepository;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_backendSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magestore\Storepickup\Model\StoreFactory $storeCollection,
        \Magento\Sales\Api\Data\OrderAddressInterface $orderAddressInterface,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Checkout\Api\ShippingInformationManagementInterface $shippingInformationManagement,
        \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalsRepository,
        \Magento\Backend\Model\Session $backendSession
    ) {
        parent::__construct($context);
        $this->_checkoutSession = $checkoutSession;
        $this->_storeCollection = $storeCollection;
        $this->_orderAddressInterface = $orderAddressInterface;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->shippingInformationManagement = $shippingInformationManagement;
        $this->cartTotalsRepository = $cartTotalsRepository;
        $this->_backendSession = $backendSession;
    }

    public function execute() {
        $quote = $this->_checkoutSession->getQuote();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $objectManager->get('\Magento\Checkout\Model\Session');

        $storepickup_id = $this->getRequest()->getParam('storepickup_id');

        $deliveryPrice = $this->getRequest()->getParam('delivery_price');
        $deliveryDate = $this->getRequest()->getParam('delivery_date');
        $deliveryTimeslot = $this->getRequest()->getParam('delivery_timeslot');
        $deliveryComment = $this->getRequest()->getParam('delivery_comment');

        $deliveryPriceExt = $this->getRequest()->getParam('delivery_price_ext');
        $deliveryDateExt = $this->getRequest()->getParam('delivery_date_ext');
        $deliveryTimeslotExt = $this->getRequest()->getParam('delivery_timeslot_ext');
        $deliveryCommentExt = $this->getRequest()->getParam('delivery_comment_ext');

        $totalDeliveryPrice = $deliveryPrice + $deliveryPriceExt;
        $session->setShippingAmount($totalDeliveryPrice);
        $session->setBaseShippingAmount($totalDeliveryPrice);

        $productionDate = $this->getProductionDate($deliveryDate);
        $productionDateExt = $this->getProductionDate($deliveryDateExt);

        $storepickup_ext_backend = $this->_backendSession->getData('storepickup_ext_backend');
        $storepickup_ext_backend['storepickup_id'] = $storepickup_id;

        $storepickup_ext_backend['delivery_date'] = $deliveryDate;
        $storepickup_ext_backend['production_date'] = $productionDate;
        $storepickup_ext_backend['delivery_timeslot'] = $deliveryTimeslot;
        $storepickup_ext_backend['delivery_comment'] = $deliveryComment;

        $storepickup_ext_backend['delivery_date_ext'] = $deliveryDateExt;
        $storepickup_ext_backend['production_date_ext'] = $productionDateExt;
        $storepickup_ext_backend['delivery_timeslot_ext'] = $deliveryTimeslotExt;
        $storepickup_ext_backend['delivery_comment_ext'] = $deliveryCommentExt;

        $this->_backendSession->setData('storepickup_ext_backend',$storepickup_ext_backend);

        $quote->setStorepickupId($storepickup_id);

        $quote->setDeliveryDate( date("Y-m-d H:i:s",strtotime($deliveryDate)));
        $quote->setProductionDate($productionDate);
        $quote->setDeliveryTimeslot($deliveryTimeslot);
        $quote->setDeliveryComment($deliveryComment);

        $quote->setDeliveryDateExt( date("Y-m-d H:i:s",strtotime($deliveryDateExt)));
        $quote->setProductionDateExt($productionDateExt);
        $quote->setDeliveryTimeslot($deliveryTimeslotExt);
        $quote->setDeliveryCommentExt($deliveryCommentExt);
    }

    public function getProductionDate($date) {
        try {
            $hour = date("H",strtotime($date));
            $hour = (int)$hour;
            $date = date("Y-m-d",strtotime($date));

            if($hour >= 0  &&  $hour < 12) {
                $date = new \DateTime($date);
                $date->sub(new \DateInterval('P1D'));
                $date  = $date->format('Y-m-d');
            }
        } catch(\Exception $e) {
            $message = $e->getMessage();
        }
        return $date;
    }
}
