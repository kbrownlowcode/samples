<?php
namespace Clarity\StorePickupExt\Observer;

use Magento\Framework\Event\ObserverInterface;

class SaveCustomOptions implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_objectManager = $objectManager;
    }

    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $this->_objectManager->get("\Magento\Framework\App\Request\Http");
        $params = $request->getParam('options');

        /** @var \Magento\Checkout\Model\Cart $cart */
        $cart = $observer->getCart();
        $quote = $cart->getQuote();
        $items = $quote->getItems();

        /** @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($items as $item) {
            /** @var \Magento\Quote\Model\Quote\Item\Option $buyRequest */
            $buyRequest = $item->getBuyRequest();
            $newOptions = isset($params[$item->getItemId()]) ? $params[$item->getItemId()] : [];
            $buyRequest->setData('options', $newOptions);
            $cart->updateItem($item->getItemId(), $buyRequest);
        }
        $cart->save();
    }
}