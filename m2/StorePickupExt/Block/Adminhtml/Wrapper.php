<?php

/**
 * Magestore.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storepickup
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace Clarity\StorePickupExt\Block\Adminhtml;

use \Magento\Checkout\Model\Session as CheckoutSession;

/**
 * @category Magestore
 * @package  Magestore_Storepickup
 * @module   Storepickup
 * @author   Magestore Developer
 */
class Wrapper extends \Magestore\Storepickup\Block\Wrapper
{
    protected $_template = 'Magestore_Storepickup::wrapper.phtml';
    /**
     * @var \Magestore\Storepickup\Model\ResourceModel\Store\CollectionFactory
     *
     */

    protected $_package_id = null;

    //protected $_storeCollectionFactory;
    protected $_checkoutSession;


    /**
     * Block constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array                                            $data
     */
    public function __construct(
        \Magestore\Storepickup\Block\Context $context,
        //\Magestore\Storepickup\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory,
        CheckoutSession $checkoutSession,
        array $data = []
    ) {
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
        //$this->_storeCollectionFactory = $storeCollectionFactory;
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function setCollection($collection = null) {
        $this->_collection = $collection;
    }

    public function getCollection() {
        return $this->_collection;
    }

    public function getSubscriptionInfo()
    {
        $quote = $this->_checkoutSession->getQuote();
//        foreach ($quote->getItemsCollection() as $item) {
//            echo $item->getSku();
//            $data = $item->getTypeInstance()->getConfigurableOptions($item);
//            $options = $item->getOptions();
//            echo $options;
//        }
        $qtyMeals = 0;
        $quoteItems = $quote->getAllItems();
        foreach ($quoteItems as $quoteItem) {
            if ($quoteItem->getParentItemId()) {
                $subProduct = explode(' ', $quoteItem->getName());
                $qtyMeals = ($subProduct[0])? $subProduct[0]: 5;
            }
        }
        return \Zend_Json::encode($qtyMeals);
    }
    public function getListStore()
    {
        /** @var \Magestore\Storepickup\Model\ResourceModel\Store\Collection $collection */
        $collection = $this->_storeCollectionFactory->create();
        $collection->addFieldToFilter('status','1')->addFieldToSelect(['storepickup_id', 'store_name','address','phone','latitude','longitude']);
        $collection->setOrder('store_name', \Magento\Framework\Data\Collection\AbstractDb::SORT_ORDER_ASC);
        return \Zend_Json::encode($collection->getData());
    }

    public function getStoreMetrics() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $schedule = $objectManager
            ->get('Clarity\StorePickupExt\Model\Schedule');
        $collection = $schedule->getCollection();
        return $collection->load();
    }

    public function getStoreMetricsByPackageId() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $schedule = $objectManager
            ->get('Clarity\StorePickupExt\Model\Schedule');
        $collection = $schedule->getCollection();
        $collection->addFieldToFilter('package_id', $this->getPackageId());
        return $collection->load();
    }

    public function setPackageId($id) {
        $this->_package_id = $id;
    }

    public function getPackageId() {
        return $this->_package_id;
    }
}
