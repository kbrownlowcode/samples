<?php
namespace Clarity\StorePickupExt\Block\Adminhtml\Store\Edit;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
class BackButton extends GenericButton implements ButtonProviderInterface
{     
    public function getButtonData()
    {
        
        return [
            'label' => __('Back'),
            'on_click' => "window.history.back()",
            'class' => 'back',
            'sort_order' => 10    
        ];
    }
}
