<?php

/**
 * Magestore.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storepickup
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace Clarity\StorePickupExt\Block;

use \Magento\Checkout\Model\Session as CheckoutSession;
use Clarity\Kitchfix\Model\KitchfixLogic;

/**
 * @category Magestore
 * @package  Magestore_Storepickup
 * @module   Storepickup
 * @author   Magestore Developer
 */
class Wrapper extends \Magestore\Storepickup\Block\Wrapper
{
    protected $_template = 'Magestore_Storepickup::wrapper.phtml';
    /**
     * @var \Magestore\Storepickup\Model\ResourceModel\Store\CollectionFactory
     *
     */

    protected $_package_id = null;

    //protected $_storeCollectionFactory;
    protected $_checkoutSession;

    protected $_storeCollection;

    protected $_kitchfixData;
    /**
     * Block constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array                                            $data
     */
    public function __construct(
        \Magestore\Storepickup\Block\Context $context,
        //\Magestore\Storepickup\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory,
        CheckoutSession $checkoutSession,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magestore\Storepickup\Model\StoreFactory $storeCollection,
        KitchfixLogic $kitchfixLogic,
        \Clarity\Kitchfix\Helper\Data $kitchfixData,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        array $data = []
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->_storeCollection = $storeCollection;
        $this->kitchfixLogic = $kitchfixLogic;
        $this->orderRepository = $orderRepository;
        $this->_kitchfixData = $kitchfixData;
        parent::__construct($context, $data);
        //$this->_storeCollectionFactory = $storeCollectionFactory;
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function setCollection($collection = null) {
        $this->_collection = $collection;
    }

    public function getCollection() {
        return $this->_collection;
    }

    public function getSubscriptionInfo()
    {
        $quote = $this->_checkoutSession->getQuote();

        $qtyMeals = 0;
        $quoteItems = $quote->getAllItems();
        foreach ($quoteItems as $quoteItem) {
            if ($quoteItem->getParentItemId()) {
                $subProduct = explode(' ', $quoteItem->getName());
                $qtyMeals = ($subProduct[0])? intval($subProduct[0]): 5;
            }
        }
        return \Zend_Json::encode($qtyMeals);
    }
    public function getListStore()
    {
        /** @var \Magestore\Storepickup\Model\ResourceModel\Store\Collection $collection */
        $collection = $this->_storeCollectionFactory->create();
        $collection->addTagsToFilter(array('4'))->addFieldToSelect(['storepickup_id', 'store_name','address','phone','latitude','longitude']);
        //$collection->addFieldToFilter('status','1')->addFieldToSelect(['storepickup_id', 'store_name','address','phone','latitude','longitude']);
        $collection->setOrder('store_name', \Magento\Framework\Data\Collection\AbstractDb::SORT_ORDER_ASC);
        return \Zend_Json::encode($collection->getData());
    }

    public function getStoreMetrics() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $schedule = $objectManager
            ->get('Clarity\StorePickupExt\Model\Schedule');
        $collection = $schedule->getCollection();
        return $collection->load();
    }

    public function getStoreMetricsByPackageId() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $schedule = $objectManager
            ->get('Clarity\StorePickupExt\Model\Schedule');
        $collection = $schedule->getCollection();
        $collection->addFieldToFilter('package_id', $this->getPackageId());
        return $collection->load();
    }

    public function setPackageId($id) {
        $this->_package_id = $id;
    }

    public function getPackageId() {
        return $this->_package_id;
    }

    public function isSubscription($zipCode, $storeId)
    {
        $quote = $this->_checkoutSession->getQuote();
        $quoteItems = $quote->getAllItems();
        foreach ($quoteItems as $quoteItem) {
            if ($quoteItem->getName() == 'Subscription') {
                $this->isChicago($zipCode);
                $this->isFreePickup($storeId);
                return true;
            }
        }
        return false;
    }

    public function isChicago($zipCode) {
        $chicagoZipCodes = array('60612','60640','60625','60613','60641','60626','60614','60642','60601','60657','60602','60659','60616','60644','60603','60630','60660','60604','60631','60661','60618','60646','60605','60647','60606','60674','60607','60634','60690','60480','60651','60608','60699','60622','60701','60623','60610','60624','60654','60611','60639','60805','60655','60453','60628','60455','60638','60615','60643','60629','60456','60616','60457','60617','60458','60632','60459','60619','60633','60465','60620','60649','60621','60608','60636','60499','60652','60609','60637','60501','60623','60653');
        if (in_array($zipCode, $chicagoZipCodes)) {
            $this->_checkoutSession->setIsChicago(1);
            return true;
        } else {
            $this->_checkoutSession->setIsChicago(0);
            return false;
        }
    }

    public function getShippingMethod ($isChicago = null, $isSuburbs = null, $isFreePickup = null, $isPickup = null) {
        return $this->kitchfixLogic->getShippingMethodCode($isChicago, $isSuburbs, $isFreePickup, $isPickup);
    }

    public function isFreePickup($storeId) {
        /** @var \Magestore\Storepickup\Model\ResourceModel\Store\Collection $collection */
        $collection = $this->_storeCollectionFactory->create();
        $collection->addFieldToFilter('storepickup_id',$storeId)->addFieldToSelect(['storepickup_id', 'store_name','address','phone','latitude','longitude']);
        $store = $collection->getFirstItem();
        $pat1 = 'Kitchfix Gold Coast';
        $pat2 = 'Our Kitchen';
        if ($store) {
            if( strpos( $store->getStoreName(), $pat1 ) !== false || strpos( $store->getStoreName(), $pat2 ) !== false ) {
                $this->_checkoutSession->setIsFreePickup(1);
                return true;
            } else {
                $this->_checkoutSession->setIsFreePickup(0);
                return false;
            }
        }
        return false;
    }

    public function getShippingPriceSubscription($zipCode, $storeId) {
        if($zipCode){
            $isChicago = $this->_checkoutSession->getIsChicago();
            if ($isChicago) {
                // 1. Chicago – City
                return 24;
            } else {
                // 2. Chicago – Suburbs
                return 48;
            }
        } else if($storeId) {
            $isFree = $this->_checkoutSession->getIsFreePickup();
            if ( ! $isFree) {
                // 3. Hubs: Anything that's NOT labeled as "Home Delivery"
                return 4;
            } else {
                // 4. Kitchen + Gold Coast Store
                return 0;
            }
        }
        return -1;
    }

    public function getStoreName($storeId) {
        /** @var \Magestore\Storepickup\Model\ResourceModel\Store\Collection $collection */
        $collection = $this->_storeCollectionFactory->create();
        $collection->addFieldToFilter('storepickup_id',$storeId)->addFieldToSelect(['storepickup_id', 'store_name','address','phone','latitude','longitude']);
        $store = $collection->getFirstItem();
        if ($store) {
            return $store->getStoreName();
        }
        return false;
    }

    public function getTypeMenu()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $objectManager->get('Magento\Customer\Model\Session');

        return $session->getCategory();
    }

    public function getTypeMenuFromOrder($order)
    {
        $nextWeek = $this->orderRepository->get($order)->getNextWeek();
        return $nextWeek;
    }

    public function getCategoryCollection($productId)
    {
        $product = $this->_productRepository->getById($productId);
        $categoryIds = $product->getCategoryIds();
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*')
            ->addIsActiveFilter()
            ->addAttributeToFilter('entity_id', $categoryIds);

        return $collection;
    }

    public function getHolidays()
    {
        $date = array();
        $closed = array();
        $holiday_date = array();
        $storeId = $this->getStoreId();
        $collectionstore = $this->_storeCollection->create();
        $store = $collectionstore->load($storeId,'storepickup_id');
        $holidaysdata = $store->getHolidaysData();
        foreach($holidaysdata as $holidays){
            foreach($holidays['date'] as $_date){
                $holiday_date[]=date("m/d/Y", strtotime($_date));
            }
        }
        if(!$store->isOpenday('monday')) $closed[]=1;
        if(!$store->isOpenday('tuesday')) $closed[]=2;
        if(!$store->isOpenday('wednesday')) $closed[]=3;
        if(!$store->isOpenday('thursday')) $closed[]=4;
        if(!$store->isOpenday('friday')) $closed[]=5;
        if(!$store->isOpenday('saturday')) $closed[]=6;
        if(!$store->isOpenday('sunday')) $closed[]=0;
        $date['holiday'] = $holiday_date;
        $date['schedule'] = $closed;

        return $date;
    }

    public function getSpecialDayData()
    {
        $storeId = $this->getStoreId();
        $collectionstore = $this->_storeCollection->create();
        $store = $collectionstore->load($storeId,'storepickup_id');
        $specialDayData = $store->getSpecialdaysData();
        $specialDayArray = array();
        $j = 0;
        foreach ($specialDayData as $specialDay) {

            $specialDayArray[$j]['dates'] = $specialDay['date'];
            $specialDayArray[$j]['times'] = array(
                'time_open'=> $specialDay['time_open'],
                'time_close' => $specialDay['time_close']
            );
            $j++;
        }
        return $specialDayArray;

    }

    public function isHubIDAnException($pickupHubId)
    {
        return $this->_kitchfixData->isHubIDAnException($pickupHubId);
    }
}
