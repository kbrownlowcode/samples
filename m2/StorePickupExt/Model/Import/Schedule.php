<?php 

namespace Clarity\StorePickupExt\Model\Import;

use Clarity\StorePickupExt\Model\Import\Schedule\RowValidatorInterface as ValidatorInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\Framework\App\ResourceConnection;

class Schedule extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity
{

    const PACKAGE_ID = 'package_id';
    const NAME = 'timeslot_name';
    const MONDAY_COST = 'monday_cost';
    const TUESDAY_COST = 'tuesday_cost';
    const WEDNESDAY_COST = 'wednesday_cost';
    const THURSDAY_COST = 'thursday_cost';
    const FRIDAY_COST = 'friday_cost';
    const SATURDAY_COST = 'saturay_cost';
    const SUNDAY_COST = 'sunday_cost';
    const TIME = 'time';

    const TABLE_ENTITY = 'clarity_shipping_timeslots';

    protected $_messageTemplates = [
        ValidatorInterface::ERROR_PACKAGE_ID_IS_EMPTY => 'Package ID is missing.',
    ];

    protected $_permanentAttributes = [
        self::PACKAGE_ID
    ];

    protected $needColumnCheck = true;

    protected $groupFactory;

    protected $validColumnNames = [
        self::PACKAGE_ID,
        self::NAME,
        self::MONDAY_COST,
        self::TUESDAY_COST,
        self::WEDNESDAY_COST,
        self::THURSDAY_COST,
        self::FRIDAY_COST,
        self::SATURDAY_COST,
        self::SUNDAY_COST,
        self::TIME,
    ];

    protected $logHistory = true;

    protected $_validators = [];

    protected $_connection;

    protected $resource;


    public function __construct (
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Magento\Customer\Model\GroupFactory $groupFactory
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->_resource = $resource;
        $this->_connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
        $this->groupFactory = $groupFactory;
    }

    public function getEntityTypeCode() {
        return 'id';
    }

    public function validateRow( array $rowData, $rowNum)
    {
        $package_id = false;

        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
        // BEHAVIOR_DELETE use specific validation logic
       // if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            if (!isset($rowData[self::TITLE]) || empty($rowData[self::TITLE])) {
                $this->addRowError(ValidatorInterface::ERROR_TITLE_IS_EMPTY, $rowNum);
                return false;
            }

        return !$this->getErrorAggregator()->isRowInvalid($rowNum);

    }

    protected function _importData()
    {
        if(\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior())
        {
            $this->deleteEntity();

        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            $this->replaceEntity();

        } elseif(\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $this->getBehavior()) {
            $this->saveEntity();
        }

        return true;

    }

    public function saveEntity()
    {
        $this->saveAndReplaceEntity();
        return this;
    }

    public function replaceEntity()
    {
        $this->saveAndReplaceEntity();
        return this;
    }

    public function deleteEntity()
    {

    }


    protected function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $listTitle = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError(ValidatorInterface::ERROR_TITLE_IS_EMPTY, $rowNum);
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }

                $rowTtile= $rowData[self::TITLE];
                $listTitle[] = $rowTtile;
                $entityList[$rowTtile][] = [
                    self::PACKAGE_ID => $rowData[self::PACKAGE_ID],
                    self::NAME => $rowData[self::NAME],
                    self::MONDAY_COST => $rowData[self::MONDAY_COST],
                    self::TUESDAY_COST => $rowData[self::TUESDAY_COST],
                    self::WEDNESDAY_COST => $rowData[self::WEDNESDAY_COST],
                    self::THURSDAY_COST => $rowData[self::THURSDAY_COST],
                    self::FRIDAY_COST => $rowData[self::FRIDAY_COST],
                    self::SATURDAY_COST => $rowData[self::SATURDAY_COST],
                    self::SUNDAY_COST => $rowData[self::SUNDAY_COST],
                    self::TIME => $rowData[self::TIME],
                ];
            }
            if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                if ($listTitle) {
                    if ($this->deleteEntityFinish(array_unique(  $listTitle), self::TABLE_ENTITY)) {
                        $this->saveEntityFinish($entityList, self::TABLE_ENTITY);
                    }
                }
            } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                $this->saveEntityFinish($entityList, self::TABLE_ENTITY);
            }
        }
        return $this;
    }

    protected function saveEntityFinish(array $entityData, $table)
    {
        if ($entityData) {
            $tableName = $this->_connection->getTableName($table);
            $entityIn = [];
            foreach ($entityData as $id => $entityRows) {
                    foreach ($entityRows as $row) {
                        $entityIn[] = $row;
                    }
            }
            if ($entityIn) {
                $this->_connection->insertOnDuplicate($tableName, $entityIn,[
                    self::PACKAGE_ID,
                    self::NAME,
                    self::MONDAY_COST,
                    self::TUESDAY_COST,
                    self::WEDNESDAY_COST,
                    self::THURSDAY_COST,
                    self::FRIDAY_COST,
                    self::SATURDAY_COST,
                    self::SUNDAY_COST,
                    self::TIME,
            ]);
            }
        }
        return $this;
    }
    protected function deleteEntityFinish(array $listTitle, $table)
    {
        if ($table && $listTitle) {
                try {
                    $this->countItemsDeleted += $this->_connection->delete(
                        $this->_connection->getTableName($table),
                        $this->_connection->quoteInto('customer_group_code IN (?)', $listTitle)
                    );
                    return true;
                } catch (\Exception $e) {
                    return false;
                }

        } else {
            return false;
        }
    }



    public function getValidColumnNames()
    {
        return $this->validColumnNames;
    }



}