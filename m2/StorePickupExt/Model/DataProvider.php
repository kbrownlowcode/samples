<?php
namespace Clarity\StorePickupExt\Model;

use Clarity\StorePickupExt\Model\ResourceModel\Schedule\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{


    protected $_loadedData;
    protected $_request;
    protected $_session;


    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $employeeCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $scheduleCollectionFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Backend\Model\Session $session,
        array $meta = [],
        array $data = []
    ) {
        $this->_session = $session;
        $this->_request = $request;
        $this->collection = $scheduleCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        $packageId = $this->_session->getPackageId();
        if (!$this->getCollection()->isLoaded()) {
            if($packageId) {
                $this->getCollection()->addFieldToFilter('package_id',$packageId);
                $this->getCollection()->load();
            }
        }

        if($this->name == "slot_form_data_source") {
            $items = $this->collection->getItems();
            foreach ($items as $schedule) {
                $this->_loadedData[$schedule->getId()] = $schedule->getData();
            }
            return $this->_loadedData;
        }

        $items = $this->getCollection()->toArray();

        return [
            'totalRecords' => $this->getCollection()->getSize(),
            'items' => $items['items'],
        ];
    }
}