<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storepickup
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace Clarity\StorePickupExt\Model\ResourceModel;


/**
 * Resource Model Store.
 *
 * @category Magestore
 * @package  Magestore_Storepickup
 * @module   Storepickup
 * @author   Magestore Developer
 */
class Store extends \Magestore\Storepickup\Model\ResourceModel\Store
{
    /**
     * Perform actions after object load
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        parent::_afterLoad($object);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storePackageCollection = $objectManager
            ->get('Clarity\StorePickupExt\Model\ResourceModel\StorePackage\Collection');
        $string = sprintf("store_id='%s' AND package_id='%s'",$this->getStorepickupId(),$this->getPackageId());
        $storePackageCollection->getSelect()->where($string);
        $storePackageCollection->load();
        if($storePackageCollection->count()) {
            $storePackage = $storePackageCollection->getFirstItem();
            $object->setPackageId($storePackage->getPackageId());
        }
        return $this;
    }

}
