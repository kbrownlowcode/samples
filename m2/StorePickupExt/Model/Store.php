<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storepickup
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace Clarity\StorePickupExt\Model;

use Magento\Framework\Exception\LocalizedException;
use Magestore\Storepickup\Model\Schedule\Option\WeekdayStatus;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Model Store.
 * @method \Magestore\Storepickup\Model\ResourceModel\Store _getResource()
 * @category Magestore
 * @package  Magestore_Storepickup
 * @module   Storepickup
 * @author   Magestore Developer
 */
class Store extends \Magestore\Storepickup\Model\Store
{
    /**
     * {@inheritdoc}
     */
    public function afterSave()
    {
        parent::afterSave();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storePackageCollection = $objectManager
            ->create('Clarity\StorePickupExt\Model\ResourceModel\StorePackage\Collection');
        $storePackageCollection->addFieldToFilter('store_id',['eq'=>$this->getStorepickupId()]);
        //$storePackageCollection->addFieldToFilter('package_id',['eq'=>$this->getPackageId()]);
        $storePackageCollection->load();
        if($storePackageCollection->count()) {
            foreach($storePackageCollection as $storePackage) {
                $package = $objectManager
                    ->create('Clarity\StorePickupExt\Model\Package');
                $package->load($storePackage->getPackageId());
                if($package && $package->getIsPickupOnly()
                    && $this->getPackagePickupId() != $package->getId()) {
                    $storePackage->delete();
                }
                if($package && !$package->getIsPickupOnly()
                    && $this->getPackageId() != $package->getId()) {
                    $storePackage->delete();
                }
                if($package && !$package->getIsPickupOnly()
                    && $this->getPackageId() != $package->getId()) {

                    $storePackage->setStoreId($this->getStorepickupId());
                    $storePackage->setPackageId($this->getPackageId());
                    $storePackage->save();
                }
                if($package && $package->getIsPickupOnly()
                    && $this->getPackagePickupId() == $package->getId()) {

                    $storePackage->setStoreId($this->getStorepickupId());
                    $storePackage->setPackageId($this->getPackagePickupId());
                    $storePackage->save();
                }
            }
        } else {
            if($this->getPackagePickupId()) {
                $storePackage = $objectManager
                    ->create('Clarity\StorePickupExt\Model\StorePackage');
                $storePackage->setStoreId($this->getStorepickupId());
                $storePackage->setPackageId($this->getPackagePickupId());
                $storePackage->save();
            }
            if($this->getPackageId()) {
                $storePackage = $objectManager
                    ->create('Clarity\StorePickupExt\Model\StorePackage');
                $storePackage->setStoreId($this->getStorepickupId());
                $storePackage->setPackageId($this->getPackageId());
                $storePackage->save();
            }
        }


        $this->_saveSerializeData()
            ->_saveImageData()
            ->_makeUrlRewrite();

        return $this;
    }

    /**
     * Perform actions after object load
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public  function afterLoad()
    {
        parent::afterLoad();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storePackageCollection = $objectManager
            ->create('Clarity\StorePickupExt\Model\ResourceModel\StorePackage\Collection');
        $string = sprintf("store_id='%s'",$this->getStorepickupId());
        $storePackageCollection->getSelect()->where($string);
        $storePackageCollection->load();
        if($storePackageCollection->count()) {
            foreach($storePackageCollection as $storePackage) {
                $package = $objectManager
                    ->get('Clarity\StorePickupExt\Model\Package');
                $package->load($storePackage->getPackageId());
                if($package->getIsPickupOnly()) {
                    $this->setPackagePickupId($storePackage->getPackageId());
                } else {
                    $this->setPackageId($storePackage->getPackageId());
                }
            }
        }
        return $this;
    }

}
