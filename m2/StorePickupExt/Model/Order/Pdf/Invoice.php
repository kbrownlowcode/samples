<?php
/**
 * Created by PhpStorm.
 * User: mpineda
 * Date: 13/11/17
 * Time: 16:24
 */

namespace Clarity\StorePickupExt\Model\Order\Pdf;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Model\Order;
use Magestore\Storepickup\Model\Order\Pdf\Invoice as StorePickupInvoice;

class Invoice extends StorePickupInvoice
{
    protected $paradoxHelper;
    /**
     * @var \Clarity\Kitchfix\Helper\Data
     */
    private $dataHelper;

    /**
     * Invoice constructor.
     *
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     * @param Order\Pdf\Config $pdfConfig
     * @param Order\Pdf\Total\Factory $pdfTotalFactory
     * @param Order\Pdf\ItemsFactory $pdfItemsFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param Order\Address\Renderer $addressRenderer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\ResolverInterface $localeResolver
     * @param \Magestore\Storepickup\Helper\Data $storepickupHelper
     * @param \ParadoxLabs\Subscriptions\Helper\Data $paradoxHelper
     * @param \Clarity\Kitchfix\Helper\Data $dataHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magestore\Storepickup\Helper\Data $storepickupHelper,
        \ParadoxLabs\Subscriptions\Helper\Data $paradoxHelper,
        \Clarity\Kitchfix\Helper\Data $dataHelper,
        array $data = []
    ) {
        $this->paradoxHelper = $paradoxHelper;
        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfConfig,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $storeManager,
            $localeResolver,
            $storepickupHelper,
            $data
        );
        $this->dataHelper = $dataHelper;
    }

    public function getPdf($invoices = [])
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($invoices as $invoice) {
            if ($invoice->getStoreId()) {
                $this->_localeResolver->emulate($invoice->getStoreId());
                $this->_storeManager->setCurrentStore($invoice->getStoreId());
            }
            $page = $this->newPage();
            $order = $invoice->getOrder();
            /* Insert Logo */
            $this->insertLogo($page, $invoice->getStore());
            /* Insert Contact Info */
            $this->insertContactInfo($page, $invoice->getStore());
            /* Insert Order Info Shipping, Payment, Addresses */
            $this->insertOrder(
                $page,
                $order,
                $this->_scopeConfig->isSetFlag(
                    self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $order->getStoreId()
                )
            );
            /* Insert Order Items */
            $this->insertOrderItems($page, $pdf, $order, $invoice);
            /* Add totals */
            $this->insertTotals($page, $invoice);

            if ($invoice->getStoreId()) {
                $this->_localeResolver->revert();
            }
        }
        $this->_afterGetPdf();
        return $pdf;
    }

    protected function insertOrderItems(&$page, &$pdf, $order, $invoice)
    {
        $this->y -= 25;
        /* Insert Header Table */
        $this->_drawHeader($page);
        /* Add body */
        foreach ($invoice->getAllItems() as $item) {
            if ($item->getOrderItem()->getParentItem()) {
                continue;
            }
            /* Draw item */
            $this->_drawItem($item, $page, $order);
            $page = end($pdf->pages);
        }
    }

    protected function insertOrder(&$page, $obj, $putOrderId = true)
    {
        if ($obj instanceof \Magento\Sales\Model\Order) {
            $shipment = null;
            $order = $obj;
        } elseif ($obj instanceof \Magento\Sales\Model\Order\Shipment) {
            $shipment = $obj;
            $order = $shipment->getOrder();
        }

        $this->y = $this->y ? $this->y : 815;
        $top = 735;

        /* Insert Main Header with Order Info and Customer/Order Type */
        $this->insertMainHeader($page, $order, $top, $putOrderId);
        $top -= 15;
        $this->insertFirstBlockData($page, $order, $top);
        $top -= 15;
        $this->insertSecondBlockData($page, $order, $top);
        $this->y = $top;
    }

    /**
     * Insert Shipments
     *
     * Disabled method @TODO needs to be refactored
     *
     * @param $page
     * @param $order
     * @param $shipment
     */
    protected function insertShipments(&$page, $order, $shipment)
    {
        $topMargin = 0;
        $yShipments = $this->y;
        $totalShippingChargesText = "(" . __(
                'Total Shipping Charges'
            ) . " " . $order->formatPriceTxt(
                $order->getShippingAmount()
            ) . ")";

        $page->drawText($totalShippingChargesText, 285, $yShipments - $topMargin, 'UTF-8');
        $yShipments -= $topMargin + 10;

        $tracks = [];
        if ($shipment) {
            $tracks = $shipment->getAllTracks();
        }
        if (count($tracks)) {
            $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
            $page->setLineWidth(0.5);
            $page->drawRectangle(285, $yShipments, 510, $yShipments - 10);
            $page->drawLine(400, $yShipments, 400, $yShipments - 10);
            //$page->drawLine(510, $yShipments, 510, $yShipments - 10);

            $this->_setFontRegular($page, 9);
            $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
            //$page->drawText(__('Carrier'), 290, $yShipments - 7 , 'UTF-8');
            $page->drawText(__('Title'), 290, $yShipments - 7, 'UTF-8');
            $page->drawText(__('Number'), 410, $yShipments - 7, 'UTF-8');

            $yShipments -= 20;
            $this->_setFontRegular($page, 8);
            foreach ($tracks as $track) {
                $maxTitleLen = 45;
                $endOfTitle = strlen($track->getTitle()) > $maxTitleLen ? '...' : '';
                $truncatedTitle = substr($track->getTitle(), 0, $maxTitleLen) . $endOfTitle;
                $page->drawText($truncatedTitle, 292, $yShipments, 'UTF-8');
                $page->drawText($track->getNumber(), 410, $yShipments, 'UTF-8');
                $yShipments -= $topMargin - 5;
            }
        } else {
            $yShipments -= $topMargin - 5;
        }

        $currentY = min(0, $yShipments);

        // replacement of Shipments-Payments rectangle block
        $page->drawLine(25, 0, 25, $currentY);
        //left
        $page->drawLine(25, $currentY, 570, $currentY);
        //bottom
        $page->drawLine(570, $currentY, 570, 0);
        //right

        $this->y = $currentY;
        $this->y -= 15;
    }

    /**
     * Draw header for item table
     *
     * @param \Zend_Pdf_Page $page
     * @return void
     */
    protected function _drawHeader(\Zend_Pdf_Page $page)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.9));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y - 15);
        $this->y -= 10;
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

        //columns headers
        $lines[0][] = ['text' => __('Qty'), 'feed' => 50, 'align' => 'right'];

        $lines[0][] = ['text' => __('Size'), 'feed' => 105, 'align' => 'right'];

        $lines[0][] = ['text' => __('Products'), 'feed' => 150];

        $lines[0][] = ['text' => __('Price'), 'feed' => 430, 'align' => 'right'];

        $lines[0][] = ['text' => __('Tax'), 'feed' => 495, 'align' => 'right'];

        $lines[0][] = ['text' => __('Subtotal'), 'feed' => 565, 'align' => 'right'];

        $lineBlock = ['lines' => $lines, 'height' => 5];

        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

    /**
     * Insert Contact Info
     *
     * @param \Zend_Pdf_Page &$page
     * @param null $store
     * @return void
     */
    protected function insertContactInfo(&$page, $store)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 10);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = 815;
        $addressText = explode(
            "\n",
            $this->_scopeConfig->getValue(
                'sales/identity/address',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $store
            )
        );
        foreach ($addressText as $value) {
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 45, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        $this->getAlignRight($_value, 130, 440, $font, 10),
                        $top,
                        'UTF-8'
                    );
                    $top -= 16;
                }
            }
        }
        $this->y = $this->y > $top ? $top : $this->y;
    }

    /**
     * Insert Main Header Gray
     *
     * @param \Zend_Pdf_Page &$page
     * @param $order Order
     * @param $top
     * @param $putOrderId
     */
    protected function insertMainHeader(&$page, $order, &$top, $putOrderId)
    {
        $this->insertDarkGrayHeader($page, $top, 25, 570, 50);
        $this->setDocHeaderCoordinates([25, $top, 570, $top - 55]);

        if ($putOrderId) {
            $this->_setFontBold($page, 12);
            $page->drawText(__('Order # ') . $order->getRealOrderId(), 35, $top -= 20, 'UTF-8');
        }

        /* Insert Text with General, New, Subscription */
        $this->insertCustomerOrderType($page, $order, $top);

        $this->_setFontBold($page, 12);
        $page->drawText(
            __('Order Date: ') .
            $this->_localeDate->formatDate(
                $this->_localeDate->scopeDate(
                    $order->getStore(),
                    $order->getCreatedAt(),
                    true
                ),
                \IntlDateFormatter::MEDIUM,
                false
            ),
            35,
            $top -= 15,
            'UTF-8'
        );
    }

    /**
     * Insert Customer or Order Type
     * General, New or Subscription
     *
     * @param \Zend_Pdf_Page &$page
     * @param $order Order\
     * @param $top
     */
    protected function insertCustomerOrderType(&$page, $order, &$top)
    {
        $x = 450;
        // Draw Customer Order Type General, New, Subscription
        $this->_setFontRegular($page, 25);
        $customerOrderType = 'General';

        if ($this->getIsFirstOrderByCustomer($order)) {
            $customerOrderType = 'New';
        } else if ($this->isSubscription($order)) {
            $customerOrderType = 'Subscription';
            $x = 420;
        }
        $page->drawText(__($customerOrderType), $x, $top - 10, 'UTF-8');
    }

    public function getIsFirstOrderByCustomer($order)
    {
        $customerOrders = $order->getCollection()
            ->addFieldToFilter('customer_id', $order->getCustomerId())
            ->getAllIds();

        return count($customerOrders) <= 1;
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param $top
     * @param $x1
     * @param $x2
     * @param int $height
     * @param int $font
     * @param float $lineWidth
     */
    protected function insertLightGrayHeader(&$page, &$top, $x1, $x2, $height=25, $font=10, $lineWidth=0.5)
    {
        $this->insertRectangle(
            $page,
            $top,
            $x1,
            $x2,
            $height,
            $font,
            $lineWidth,
            0.9,
            0.5
        );
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param $top
     * @param $x1
     * @param $x2
     * @param int $height
     * @param int $font
     * @param float $lineWidth
     */
    protected function insertDarkGrayHeader(&$page, &$top, $x1, $x2, $height=25, $font=10, $lineWidth=0.5)
    {
        $this->insertRectangle(
            $page,
            $top,
            $x1,
            $x2,
            $height,
            $font,
            $lineWidth,
            0.45,
            0.45
        );
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param $top
     * @param $x1
     * @param $x2
     * @param int $backGrayScale
     * @param float $lineGrayScale
     * @param int $height
     * @param int $font
     * @param float $lineWidth
     */
    protected function insertRectangle(&$page, &$top, $x1, $x2, $height=25, $font=10, $lineWidth=0.5, $backGrayScale=1, $lineGrayScale=0.5)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale($backGrayScale));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale($lineGrayScale));
        if ($lineWidth >= 0) {
            $page->setLineWidth($lineWidth);
        }
        $page->drawRectangle($x1, $top, $x2, $top - $height);
        $this->_setFontRegular($page, $font);
    }

    /**
     * Insert the Second Block Data
     *
     * @param $page
     * @param $order
     * @param $top
     */
    protected function insertSecondBlockData(&$page, $order, &$top)
    {
        $this->insertRectangle($page, $top, 25, 570, 105);
        $this->insertDeliveryInfo($page, $order, $top);
    }

    /**
     * Insert the First Block Data
     *
     * @param $page
     * @param $order
     * @param $top
     */
    protected function insertFirstBlockData(&$page, $order, &$top)
    {
        $heightRectangle = $this->getHeightByAddress($order) + 55;
        $this->insertRectangle($page, $top, 25, 570, $heightRectangle);
        $this->insertSoldTo($page, $order, $top);
        $this->insertDeliverTo($page, $order, $top);
        $top -= $heightRectangle; //Move Top position adding (rest) height rectangle
    }

    /**
     * Insert Sold To Block
     *
     * @param $page
     * @param $order
     * @param $top
     */
    protected function insertSoldTo(&$page, $order, &$top)
    {
        $this->insertLightGrayHeader($page, $top, 25, 570);
        $this->_setFontBold($page, 12);
        $page->drawText(__('Sold to:'), 35, $top - 15, 'UTF-8');
        $this->insertBillingAddress($page, $order, $top);
    }

    /**
     * Insert Deliver To Block
     *
     * @param $page
     * @param $order
     * @param $top
     */
    protected function insertDeliverTo(&$page, $order, &$top)
    {
        $this->insertLightGrayHeader($page, $top, 275, 570);
        $this->_setFontBold($page, 12);
        $page->drawText(__('Deliver to:'), 285, $top - 15, 'UTF-8');
        $this->insertShippingAddress($page, $order, $top);
    }

    /**
     * Insert Delivery Info
     *
     * @param $page
     * @param $order
     * @param $top
     */
    protected function insertDeliveryInfo(&$page, $order, &$top)
    {
        $this->insertLightGrayHeader($page, $top, 25, 570);
        $this->_setFontBold($page, 12);
        $top -= 15;
        $page->drawText(__('Delivery Info'), 35, $top, 'UTF-8');
        $this->_setFontRegular($page, 12);
        $this->insertDeliveryInfoData($page, $order, $top);
    }

    /**
     * Insert Delivery Info Data
     *
     * Delivery Date, Production Date, Time Slot
     *
     * @param $page
     * @param $order
     * @param $top
     */
    protected function insertDeliveryInfoData(&$page, $order, &$top)
    {
        // Delivery Type
        $deliveryTypeVal = $this->getOrderIsHomeDelivery($order) ? __('Home Delivery') : __('Hub Pick-Up');
        $deliveryTypeTxt = __('Delivery Type: ') . $deliveryTypeVal;
        $top -= 32;
        $page->drawText($deliveryTypeTxt, 35, $top, 'UTF-8');

        $realOrder = $this->dataHelper->getRealOrder($order);

        // Delivery Date
        $devDate = __('Delivery Date: ') . $this->formatDate($order->getDeliveryDate()) . ' ' . $realOrder->getDeliveryTimeslot();
        $top -= 20;
        $page->drawText($devDate, 35, $top, 'UTF-8');

        // Production Date
        $prodDate = __('Production Date: ') . $this->formatDate($order->getProductionDate());
        $top -= 20;
        $page->drawText($prodDate, 35, $top, 'UTF-8');

        $top -= 10;
    }

    /**
     * Insert the billing Address
     *
     * @param $page
     * @param $order
     * @param $top
     */
    protected function insertBillingAddress(&$page, $order, &$top)
    {
        $this->insertBaseAddress($page, $top, 35, $this->getFormattedBillingAddress($order));
    }

    /**
     * Insert the shipping Address
     *
     * @param $page
     * @param $order
     * @param $top
     */
    protected function insertShippingAddress(&$page, $order, &$top)
    {
        $this->insertBaseAddress($page, $top, 285, $this->getFormattedShippingAddress($order));
    }

    /**
     * Process and renders an address
     *
     * @param $page
     * @param $top
     * @param $x
     * @param $address
     * @return array|int
     * @internal param $order
     */
    protected function insertBaseAddress(&$page, &$top, $x, $address)
    {
        $this->_setFontRegular($page, 12);
        $newTop = $top - 25;
        foreach ($address as $value) {
            if ($value !== '') {
                $text = [];
                foreach ($this->string->split($value, 45, true, true) as $_value) {
                    $text[] = $_value;
                }
                foreach ($text as $part) {
                    $page->drawText(strip_tags(ltrim($part)), $x, $newTop - 20, 'UTF-8');
                    $newTop -= 18;
                }
            }
        }
    }

    /**
     * Retrieve the Biilling Address Formatted
     *
     * @param $order
     * @return array|int
     */
    protected function getFormattedBillingAddress($order)
    {
        return $this->_formatAddress($this->addressRenderer->format($order->getBillingAddress(), 'pdf'));
    }

    /**
     * Retrieve the Shipping Address Formatted
     *
     * @param $order
     * @return array|int
     */
    protected function getFormattedShippingAddress($order)
    {
        if ($shippingAddress = $order->getShippingAddress()) {
            return $this->_formatAddress($this->addressRenderer->format($shippingAddress, 'pdf'));
        }
        return 0;
    }

    /**
     * @param $order
     * @return mixed
     */
    protected function getShippingMethod($order)
    {
        return $order->getShippingDescription();
    }

    /**
     * @param $order
     * @return array
     */
    protected function getPaymentMethod($order)
    {
        $paymentInfo = $this->_paymentData->getInfoBlock($order->getPayment())->setIsSecureMode(true)->toPdf();
        $paymentInfo = htmlspecialchars_decode($paymentInfo, ENT_QUOTES);
        $payment = explode('{{pdf_row_separator}}', $paymentInfo);
        foreach ($payment as $key => $value) {
            if (strip_tags(trim($value)) == '') {
                unset($payment[$key]);
            }
        }
        reset($payment);
        return $payment;
    }

    /**
     * Retrieves the height of the Address if is rendered
     *
     * @param $order
     * @return int|mixed
     */
    protected function getHeightByAddress($order)
    {
        $addressesHeight = $this->_calcAddressHeight($this->getFormattedBillingAddress($order));
        if ($shippingAddress = $this->getFormattedShippingAddress($order)) {
            $addressesHeight = max($addressesHeight, $this->_calcAddressHeight($shippingAddress));
        }
        return $addressesHeight;
    }

    /**
     * Retrieve if the order is HomeDelivery
     *
     * @param $order
     * @return bool
     */
    protected function getOrderIsHomeDelivery($order)
    {
        if ($this->isRandomizedOrder($order))  {
            return strtolower($order->getType()) == 'delivery';
        }
        return $order->getShippingMethod() == 'storedelivery_storedelivery';
    }

    /**
     * Format DateTime
     *
     * @param $datetime
     * @return string
     */
    protected function formatDateTime($datetime)
    {
        return $this->_formatDateTime($datetime);
    }

    /**
     * Format formatDate
     *
     * @param $datetime
     * @return string
     */
    protected function formatDate($datetime)
    {
        return $this->_formatDateTime($datetime, 'D, d M Y');
    }

    /**
     * Format DateTime
     *
     * @param $datetime
     * @return string
     */
    private function _formatDateTime($datetime, $format = 'D, d M Y H:i:s')
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /* @var $objDate DateTime */
        $objDate = $objectManager->create(\Magento\Framework\Stdlib\DateTime\DateTime::class);

        return $objDate->date($format, $datetime);
    }

    public function isSubscription($order)
    {
        // Check if is a Weekly Subscription Order/Randomizer Order
        $isSubscription = $this->isRandomizedOrder($order);
        // Check if is a Subscription
        if (!$isSubscription) {
            foreach ($order->getAllVisibleItems() as $item) {
                if ($this->paradoxHelper->isItemSubscription($item) === true) {
                    return true;
                }
            }
        }
        return $isSubscription;
    }

    public function isRandomizedOrder($order)
    {
        return $order instanceof \Clarity\SubscriptionRandomizer\Model\Orders;
    }
}