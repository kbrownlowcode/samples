<?php
namespace Clarity\StorePickupExt\Model\Order\Pdf\Items\Invoice;

use \Magento\Sales\Model\Order\Pdf\Items\Invoice\DefaultInvoice as BaseDefaultInvoice;

class DefaultInvoice extends BaseDefaultInvoice
{
    protected $linkManagement;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * DefaultInvoice constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Tax\Helper\Data $taxData
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\ConfigurableProduct\Api\LinkManagementInterface $linkManagement
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Tax\Helper\Data $taxData,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filter\FilterManager $filterManager,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\ConfigurableProduct\Api\LinkManagementInterface $linkManagement,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->productRepository = $productRepository;
        $this->linkManagement = $linkManagement;

        parent::__construct(
            $context,
            $registry,
            $taxData,
            $filesystem,
            $filterManager,
            $string,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getItem()
    {
        $item = parent::getItem();
        // Check if is a subscription product the qty is fixed to 1
        if ($this->getOrder() instanceof \Clarity\SubscriptionRandomizer\Model\Orders) {
            $item->setQty(1);
        }
        return $item;
    }

    /**
     * Draw Item
     */
    public function draw()
    {
        $order = $this->getOrder();
        /** @var \Magento\Sales\Model\Order\Invoice\Item $item */
        $item = $this->getItem();
        $pdf = $this->getPdf();
        $page = $this->getPage();
        $isRandomized = $pdf->isRandomizedOrder($order);
        $lines = [];

        // Draw Qty
        $lines[0][] = ['text' => $item->getQty() * 1, 'feed' => 50, 'align' => 'right'];

        // Draw Size
        $lines[0][] = ['text' => $this->getItemSize($isRandomized), 'feed' => 85];

        // Draw Product name
        $lines[0][] = ['text' => $this->string->split($this->getItemName(), 60, true, true), 'feed' => 150];

        // Draw item Prices
        $i = 0;
        $prices = $this->getItemPricesForDisplay();
        $feedPrice = 430;
        $feedSubtotal = 565;
        foreach ($prices as $priceData) {
            if (isset($priceData['label'])) {
                // draw Price label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedPrice, 'align' => 'right'];
                // draw Subtotal label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedSubtotal, 'align' => 'right'];
                $i++;
            }
            // draw Price
            $lines[$i][] = [
                'text' => $priceData['price'],
                'feed' => $feedPrice,
                'font' => 'bold',
                'align' => 'right',
            ];
            // draw Subtotal
            $lines[$i][] = [
                'text' => $priceData['subtotal'],
                'feed' => $feedSubtotal,
                'font' => 'bold',
                'align' => 'right',
            ];
            $i++;
        }

        // draw Tax
        $lines[0][] = [
            'text' => $order->formatPriceTxt($item->getTaxAmount()),
            'feed' => 495,
            'font' => 'bold',
            'align' => 'right',
        ];
        // custom options
        $options = $this->getItemOptions();
        if ($options) {
            $i = 0;
            foreach ($options as $option) {
                if (strpos(strtolower($option['label']), 'size') !== false) {
                    continue;
                }
                $i++;
                // draw options label
                $lines[$i][] = [
                    'text' => $this->string->split($this->filterManager->stripTags($option['label']), 40, true, true),
                    'font' => 'italic',
                    'feed' => 150,
                ];

                if ($option['value']) {
                    if (isset($option['print_value'])) {
                        $printValue = $option['print_value'];
                    } else {
                        $printValue = $this->filterManager->stripTags($option['value']);
                    }
                    $values = explode(', ', $printValue);
                    foreach ($values as $value) {
                        $i++;
                        $lines[$i][] = [
                            'text' => $this->string->split($value, 30, true, true),
                            'font' => 'bold',
                            'feed' => 165
                        ];
                    }
                }
            }
        }

        $lineBlock = ['lines' => $lines, 'height' => 20];

        $page = $pdf->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $this->setPage($page);
    }

    private function getItemSize($isRandomized=false)
    {
        if ($isRandomized) {
            $product = $this->productRepository->getById($this->getItem()->getProductId());
            return $product->getAttributeText('size');
        }

        $options = $this->getItemOptions();
        foreach ($options as $option) {
            if (strpos(strtolower($option['label']), 'size')!==false) {
                return isset($option['print_value']) ? $option['print_value'] : $option['value'];
            }
        }
    }

    private function getItemName()
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $parents = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')
            ->getParentIdsByChild($this->getItem()->getProductId())
        ;
        if (empty($parents)) {
            return $this->getItem()->getName();
        }
        return $this->productRepository->getById($parents[0])->getName();
    }
}