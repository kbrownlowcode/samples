<?php
namespace Clarity\StorePickupExt\Model;

use Clarity\StorePickupExt\Model\ResourceModel\Package\CollectionFactory;

class StorePackageDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{


    protected $_loadedData;


    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $employeeCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $storePackageCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $storePackageCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }

        if($this->name == "slot_package_form_data_source") {
            $items = $this->collection->getItems();
            foreach ($items as $package) {
                $this->_loadedData[$package->getId()] = $package->getData();
            }
            return $this->_loadedData;
        }

        $items = $this->getCollection()->toArray();

        return [
            'totalRecords' => $this->getCollection()->getSize(),
            'items' => $items['items'],
        ];
    }
}