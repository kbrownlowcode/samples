<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Storepickup
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace Clarity\StorePickupExt\Model;

/**
 * Model Schedule.
 *
 * @category Magestore
 * @package  Magestore_Storepickup
 * @module   Storepickup
 * @author   Magestore Developer
 */
class Schedule extends \Clarity\StorePickupExt\Model\AbstractModelManageStores
{
    /**
     * Model construct that should be used for object initialization.
     */
    public function _construct()
    {
        $this->_init('Clarity\StorePickupExt\Model\ResourceModel\Schedule');
    }

    /**
     * Processing object before save data.
     */
    public function beforeSave()
    {

        return parent::beforeSave();
    }

    public function getTimeSlotDescription($number) {
        $datetime = new \DateTime('+'.$number.' day');
        $day = $datetime->format('D');
        $timeSlotTitle = $day." After ".$this->getTime();
        return $timeSlotTitle;
    }

    public function getTimeStampRelativeByToday($number) {
        $datetime = new \DateTime('+'.$number.' day');
        $day = $datetime->format('m/d/Y');
        return $day;
    }

    public function checkIfTimePassed($day){
        $datetime = new \DateTime('+'.$day.' day');
        $nowDateTime = strtotime("now");
        $toDay = $datetime->format('m/d/Y');
        $stringTime = $toDay." ".$this->getTime();
        $timeSlotTimeStamp = strtotime($stringTime);
        if($nowDateTime < $timeSlotTimeStamp) {
            return false;
        }
        return true;
    }

    public function  getCostRelativeByToday($day) {

        $datetime = new \DateTime('+'.$day.' day');
        $nowDateTime = new \DateTime('Now');

        $toDay = $datetime->format('D');
        $nowDay = $nowDateTime->format('D');
        if($nowDay == $toDay) {
//            $timeSlotTimeStamp = $timeSlotDate
        }
        switch($toDay) {
            case "Mon":
                return $this->getMondayCost();
            case "Tue":
                return $this->getTuesdayCost();
            case "Wed":
                return $this->getWednesdayCost();
            case "Thu":
                return $this->getThursdayCost();
            case "Fri":
                return $this->getFridayCost();
            case "Sat":
                return $this->getSaturdayCost();
            case "Sun":
                return $this->getSundayCost();
        }

    }
}
