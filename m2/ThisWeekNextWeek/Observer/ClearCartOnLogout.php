<?php
namespace Clarity\ThisWeekNextWeek\Observer;

class ClearCartOnLogout implements \Magento\Framework\Event\ObserverInterface {

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Quote\Model\Quote\Item $item
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Model\Quote\Item $item
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->itemModel = $item;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
        foreach ($allItems as $item) {
            $itemId = $item->getItemId();
            $quoteItem=$this->itemModel->load($itemId);//load particular item which you want to delete by his item id
            $quoteItem->delete();//deletes the item
        }
    }

}