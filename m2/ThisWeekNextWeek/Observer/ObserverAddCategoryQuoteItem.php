<?php
namespace Clarity\ThisWeekNextWeek\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class ObserverAddCategoryQuoteItem implements ObserverInterface
{


    /**
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlManager;


    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;


    private $_pageConfig;

    /**
     * @param \Magento\Framework\App\ActionFlag $actionFlag
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     */
    public function __construct(
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\UrlInterface $urlManager,
        \Magento\Framework\View\Page\Config $pageConfig,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->_actionFlag = $actionFlag;
        $this->messageManager = $messageManager;
        $this->_urlManager = $urlManager;
        $this->redirect = $redirect;
        $this->request = $request;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $item = $observer->getQuoteItem();
        $request = $this->request;
        $currentCategory = null;
        if ($request->getParam('category')) {
            $currentCategory = $request->getParam('category');
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $session = $objectManager->get('Magento\Customer\Model\Session');
        $prevCategory = $session->getCategory();

        if ($currentCategory == 'Order') {
            $session->setCategory(1);
        } else if ($currentCategory == 'Next Week\'s Menu') {
            $session->setCategory(2);
        }
    }

}