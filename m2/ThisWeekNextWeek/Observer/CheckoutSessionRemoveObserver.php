<?php
namespace Clarity\ThisWeekNextWeek\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class CheckoutSessionRemoveObserver implements ObserverInterface
{
    private $_sessionManager;

    public function __construct(
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Magento\Catalog\Model\ProductFactory $_productLoader,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    ) {
        $this->_sessionManager = $sessionManager;
        $this->_productloader = $_productLoader;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryFactory = $categoryFactory;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $quote = $quoteItem->getQuote();
        $allItems = $quote->getAllItems();
        $clearCategorySession = true;
        foreach ($allItems as $item) {
            $product = $this->_productloader->create()->load($item->getProductId());
            $cats = $product->getCategoryIds();
            if(count($cats) ){
                $collection = $this->_categoryCollectionFactory->create();
                $categories = $collection->addAttributeToFilter('entity_id', $cats);
                foreach($categories as $category) {
                    $category = $this->_categoryFactory->create()->load($category->getId());
                    $categoryName = $category->getName();
                    if ($categoryName == 'Order' || $categoryName == 'Next Week\'s Menu') {
                        $clearCategorySession = false;
                    }
                }
            }
        }

        if ($clearCategorySession) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $session = $objectManager->get('Magento\Customer\Model\Session');
            $session->setCategory(null);
        }

    }

}