<?php

namespace Clarity\ThisWeekNextWeek\Model;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteIdMaskFactory;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Session
{
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Model\Quote\Item $item
    )
    {
        $this->_customerSession = $customerSession;
        $this->quoteRepository = $quoteRepository;
        $this->_storeManager = $storeManager;
        $this->quoteFactory = $quoteFactory;
        $this->_eventManager = $eventManager;
        $this->itemModel = $item;
    }
    /**
     * Load data for customer quote and merge with current quote
     *
     * @return $this
     */
    public function aroundLoadCustomerQuote(\Magento\Checkout\Model\Session $session, \Closure $closure)
    {
        if (!$this->_customerSession->getCustomerId()) {
            return $this;
        }

        $this->_eventManager->dispatch('load_customer_quote_before', ['checkout_session' => $this]);

        try {
            $customerQuote = $this->quoteRepository->getForCustomer($this->_customerSession->getCustomerId());
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $customerQuote = $this->quoteFactory->create();
        }
        $customerQuote->setStoreId($this->_storeManager->getStore()->getId());

        $session->getQuote()->getBillingAddress();
        $session->getQuote()->getShippingAddress();
        $session->getQuote()->setCustomer($this->_customerSession->getCustomerDataObject())
            ->setTotalsCollectedFlag(false)
            ->collectTotals();
        $this->quoteRepository->save($session->getQuote());

        $allItems = $customerQuote->getAllItems();
        foreach ($allItems as $item) {
            $itemId = $item->getItemId();
            $quoteItem=$this->itemModel->load($itemId);
            $quoteItem->delete();
        }
        $this->quoteRepository->save($session->getQuote());

        return $this;
    }

}
