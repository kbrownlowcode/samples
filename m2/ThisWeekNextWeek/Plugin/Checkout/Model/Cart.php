<?php
/**
 * Created by PhpStorm.
 * Company: Rapicart
 * Web: https://www.rapicart.com
 * User: Pablo Garcia
 * Email: pablo.garcia@rapicart.com
 * Date: 16/11/17
 * Time: 17:02
 */

namespace Clarity\ThisWeekNextWeek\Plugin\Checkout\Model;

use Magento\Framework\Exception\LocalizedException;

class Cart
{
    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $_quote;

    /**
     * Cart constructor.
     *
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->_quote = $checkoutSession->getQuote();
        $this->request = $request;
    }

    /**
     * beforeAddProduct
     *
     * @param      $subject
     * @param      $productInfo
     * @param null $requestInfo
     *
     * @return array
     * @throws LocalizedException
     */
    public function beforeAddProduct($subject, $productInfo, $requestInfo = null)
    {
        $productCollection = $this->_quote->getAllItems();
        if ($productCollection) {
            $request = $this->request;
            $currentCategory = null;
            if ($request->getParam('category')) {
                $currentCategory = $request->getParam('category');
            }

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $session = $objectManager->get('Magento\Customer\Model\Session');
            $prevCategory = $session->getCategory();

            if ($prevCategory == 1 && $currentCategory == 'Next Week\'s Menu') {
                throw new LocalizedException(__('Looks like you\'re ordering from two separate menus! Please place a separate order for each week for which you would like delivery.'));
            }

            if ($prevCategory == 2 && $currentCategory == 'Order') {
                throw new LocalizedException(__('Looks like you\'re ordering from two separate menus! Please place a separate order for each week for which you would like delivery..'));
            }
        }

        return [$productInfo, $requestInfo];
    }
}