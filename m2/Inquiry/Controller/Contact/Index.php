<?php

namespace Clarity\Inquiry\Controller\Contact;

use Clarity\Inquiry\Controller\BaseEmailForm;

class Index extends BaseEmailForm
{
    /**
     * @var array
     */
    protected $fieldMap = [
        'first-name' => 'First Name',
        'last-name' => 'Last Name',
        'email' => 'Email',
        'phone-1' => 'Phone',
        'phone-2' => 'Phone',
        'phone-3' => 'Phone',
        'inquirytype' => 'Inquiry Type',
        'message' => 'Message'
    ];

    protected $requiredFields = [
        'first-name',
        'last-name',
        'email',
        'phone-1',
        'phone-2',
        'phone-3'
    ];

    protected $_emailTemplate = 'clarity_inquiry/email/contact_template';
}