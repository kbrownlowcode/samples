<?php

namespace Clarity\Inquiry\Controller;

use Magento\Framework\Exception\InputException;

class BaseEmailForm extends \Magento\Framework\App\Action\Action
{

    protected $_objectManager;

    protected $_resultPageFactory;

    protected $_request;
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    protected $scopeConfig;

    protected $_escaper;

    protected $storeManager;

    protected $logger;

    /**
     * @var array
     */
    protected $fieldMap = [
        'first-name' => 'First Name',
        'last-name' => 'Last Name',
        'email' => 'Email',
        'phone-1' => 'Phone',
        'phone-2' => 'Phone',
        'phone-3' => 'Phone',
        'inquirytype' => 'Inquiry Type',
        'message' => 'Message'
    ];

    protected $requiredFields = [
        'first-name',
        'last-name',
        'email',
        'phone-1',
        'phone-2',
        'phone-3'
    ];

    protected $_emailsTo = [
        'Partnerships' => 'gabby@kitchfix.com',
        'Media Requests' => 'britta@kitchfix.com',
        'Meal Delivery Customer Service' => 'info@kitchfix.com',
        'Grain-Free Grocery Questions/Custoemr Service' => 'info@kitchfix.com',
        'Wholesale Opportunities' => 'josh@kitchfix.com',
        'Question for the Nutritionist' => 'katie@kitchfix.com',
        'General Inquiries' => 'info@kitchfix.com'
    ];

    protected $_emailTemplate;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Escaper $escaper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->_request = $request;
        $this->_resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->_escaper = $escaper;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Initialize coupon
     *
     * @return \Magento\Framework\View\Result\Page
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // Çheck if Request is POST then send Email
        if ($this->_request->isPost()) {
            return $this->sendMessage();
        }
        // Render form page
        return $this->_resultPageFactory->create();
    }

    private function sendMessage()
    {
        try {
            $fields = $this->getFields();
            $this->validateFields($fields);

            $this->inlineTranslation->suspend();
            $dataObject = new \Magento\Framework\DataObject();
            $dataObject->setData($fields);
            $sender = [
                'name' => $this->_escaper->escapeHtml($fields['first-name']) . " " . $this->_escaper->escapeHtml($fields['last-name']),
                'email' => $this->_escaper->escapeHtml($fields['email']),
            ];
            $this->_transportBuilder
                ->setTemplateIdentifier($this->getTemplateId())
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->storeManager->getStore()->getId(),
                    ]
                )
                ->setTemplateVars(['data' => $dataObject])
                ->setFrom($sender);

            foreach ($this->_emailsTo as $name => $address) {
                $this->_transportBuilder->addTo($address, $name);
            }
            $transport = $this->_transportBuilder->getTransport();

            $transport->sendMessage();

            $this->inlineTranslation->resume();
            $this->messageManager->addSuccessMessage(__('Your message have been sent successfully.'));

        } catch (InputException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch(\Exception $e) {
            $this->messageManager->addErrorMessage(__('We are sorry, an error has occurred to send your message.'));
            $this->logger->error($e);
        }
        return $this->_redirect('*/*');
    }

    /**
     * @return array
     */
    private function getFields()
    {
        return array_intersect_key($this->_request->getParams(), $this->fieldMap);
    }

    /**
     * @param $fields
     * @return array
     * @throws InputException
     */
    private function validateFields($fields)
    {
        foreach ($this->requiredFields as $key) {
            if (!(bool)$fields[$key] || !isset($fields[$key])) {
                throw InputException::requiredField($this->fieldMap[$key]);
            }
        }
        return $fields;
    }

    protected function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function getStore()
    {
        return $this->storeManager->getStore();
    }

    public function getTemplateId()
    {
        return $this->getConfigValue($this->_emailTemplate, $this->getStore()->getStoreId());
    }

}