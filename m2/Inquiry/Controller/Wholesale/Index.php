<?php

namespace Clarity\Inquiry\Controller\Wholesale;

use Clarity\Inquiry\Controller\BaseEmailForm;

class Index extends BaseEmailForm
{
    /**
     * @var array
     */
    protected $fieldMap = [
        'first-name' => 'First Name',
        'last-name' => 'Last Name',
        'email' => 'Email',
        'message' => 'Message'
    ];

    protected $requiredFields = [
        'first-name',
        'last-name',
        'email',
        'message'
    ];

    protected $_emailsTo = [
        'Josh' => 'josh@kitchfix.com'
    ];

    protected $_emailTemplate = 'clarity_inquiry/email/whole_template';
}