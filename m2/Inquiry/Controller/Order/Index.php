<?php

namespace Clarity\Inquiry\Controller\Order;

use Clarity\Inquiry\Controller\BaseEmailForm;

class Index extends BaseEmailForm
{
    /**
     * @var array
     */
    protected $fieldMap = [
        'first-name' => 'First Name',
        'last-name' => 'Last Name',
        'email' => 'Email',
        'organization' => 'Organization',
        'phone-1' => 'Phone',
        'phone-2' => 'Phone',
        'phone-3' => 'Phone',
        'inquirytype' => 'Inquiry Type',
        'message' => 'Message'
    ];

    protected $requiredFields = [
        'first-name',
        'last-name',
        'email'
    ];

    protected $_emailsTo = [
        'Kerry' => 'kerry@kitchfix.com'
    ];

    protected $_emailTemplate = 'clarity_inquiry/email/order_template';
}