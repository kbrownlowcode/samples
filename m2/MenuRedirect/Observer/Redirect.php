<?php

    namespace Clarity\MenuRedirect\Observer;
    
    use Magento\Framework\Event\Observer;
    use Magento\Framework\Event\ObserverInterface;

    class Redirect implements ObserverInterface {

        protected $_responseFactory;
        protected $_url;

        public function __construct(

            \Magento\Framework\App\ResponseFactory $responseFactory,
            \Magento\Framework\UrlInterface $url,
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
        ) {
            $this->_responseFactory = $responseFactory;
            $this->_url = $url;
            $this->scopeConfig = $scopeConfig;
        }

        public function execute(Observer $observer) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $order_category = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('order/category/main');

            $thisweek_id = $this->scopeConfig->getValue('menu_categories/thisweek/main', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $nextweek_id = $this->scopeConfig->getValue('menu_categories/nextweek/main', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $thisweek = $objectManager->create('Magento\Catalog\Model\Category')->load($thisweek_id);
            $nextweek = $objectManager->create('Magento\Catalog\Model\Category')->load($nextweek_id);

            // Get Order Category
            $category = $observer->getEvent()->getCategory();

            if($category->getId() == $order_category):

                if($thisweek->getIsActive()):
                    //$controllerAction = $observer->getEvent()->getControllerAction();
                    $redirect_url = $thisweek->getUrl();
                    $this->_responseFactory->create()->setRedirect($redirect_url)->sendResponse();
                    exit;
                else:
                    //$controllerAction = $observer->getEvent()->getControllerAction();
                    $redirect_url = $nextweek->getUrl() . "?nextweek=1";
                    $this->_responseFactory->create()->setRedirect($redirect_url)->sendResponse();
                    exit;
                endif;

            endif;    




        }




    }