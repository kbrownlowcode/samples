<?php
/**
 * @namespace   Brownlow
 * @module      SLISearch
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        9/12/2016 10:03 PM
 */

class Brownlow_SLISearch_Model_Generators_ProductGenerator extends SLI_Search_Model_Generators_ProductGenerator
{
    protected $_productCallbackTotal = 0;

    public function _productCallback($args)
    {
        $logger    = $args['logger'];
        $xmlWriter = $args['xml_writer'];

        $this->_productCallbackTotal++;

        $product = Mage::getModel('catalog/product')
            ->setData($args['row']);

        $product->getCategoryIds();

        if ($product->getData('left_rating_summary')) {
            $rating = new Varien_Object(array(
                'reviews_count'  => $product->getData('left_reviews_count'),
                'rating_summary' => $product->getData('left_rating_summary'),
            ));

            $product->setData('rating_summary', $rating);
        }

        $product
            ->unsetData('left_reviews_count')
            ->unsetData('left_rating_summary');

        $this->writeProductData($product, $xmlWriter);

        if ($this->_productCallbackTotal % 10000 == 0) {
            $logger->debug(sprintf(
                'Finished processing products: %s',
                $this->_productCallbackTotal
            ));
        }
    }

    /**
     * Add the stores products to the feed with the selected attributes
     *
     * @param int $storeId
     * @param SLI_Search_Model_Generators_GeneratorContext $generatorContext
     */
    protected function addProductsToFeed(
        $storeId,
        SLI_Search_Model_Generators_GeneratorContext $generatorContext
    ) {
        $logger = $generatorContext->getLogger();
        $xmlWriter = $generatorContext->getXmlWriter();

        /** @var $feedHelper SLI_Search_Helper_Feed */
        $feedHelper = Mage::helper('sli_search/feed');
        $extraAttributes = $feedHelper->getExtraAttributes();

        $logger->trace("Adding products");

        $collection = $generatorContext->getProductCollection($storeId);

        foreach ($extraAttributes as $extraAttribute) {
            $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $extraAttribute);

            if (
                   !$attribute->getId()
                || in_array($extraAttribute, array('sku'))
            ) {
                continue;
            }

            $collection->addAttributeToSelect($extraAttribute, 'left');
        }

        $resource = Mage::getSingleton('core/resource');

        // adding rating_summary attribute
        $collection
            ->getSelect()
            ->joinLeft(
                array('res' => $resource->getTableName('review/review_aggregate')),
                sprintf('res.entity_pk_value = e.entity_id AND res.store_id = 0'),
                array(
                    'left_reviews_count'  => 'reviews_count',
                    'left_rating_summary' => 'rating_summary',
                )
            );

        Mage::getSingleton('core/resource_iterator')->walk(
            $collection->getSelect(),
            array(
                array(
                    $this, '_productCallback'
                ),
            ),
            array(
                'logger'           => $logger,
                'xml_writer'       => $xmlWriter,
                'extra_attributes' => $extraAttributes,
            ));

        $logger->debug("Finished adding products");
    }
}
