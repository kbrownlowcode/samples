<?php

/**
 * jjuleff@Brownlowagility.com
 */
class Brownlow_Weee_Helper_Weee_Data extends Mage_Weee_Helper_Data
{
    /**
     * Check if fixed taxes are used in system
     *
     * @param Mage_Core_Model_Store $store
     *
     * @return bool
     */
    public function isEnabled($store = null)
    {
        if ($store == null && $this->_store) {
            //This is needed when order is created from backend
            $store = $this->_store;
        }
        //If allow calculation is not equal to 1, we do not calculate weee.
        if (
            Mage::registry('category_list_layout_render')
            && is_object(Mage::registry('current_category'))
            && Mage::registry('current_category')->getData('allow_weee_calculation') !== 1
        ) {   //if we are rending list.phtml, and have a category (we are on a category page)
            //and our category has weee disabled, we return 0;
            return 0;
        }

        return Mage::getStoreConfig(self::XML_PATH_FPT_ENABLED, $store);
    }
}