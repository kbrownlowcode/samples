<?php
class Brownlow_Brands_Model_Mysql4_Brand extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init("brands/brand", "page_id");
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        /*
         * For two attributes which represent timestamp data in DB
         * we should make converting such as:
         * If they are empty we need to convert them into DB
         * type NULL so in DB they will be empty and not some default value
         */


        if (!$this->getIsUniqueUrl($object)) {
            Mage::throwException(Mage::helper('brands')->__('The Url specified is already in use '));
        }
        if(!$this->getBrandExists($object)){
            Mage::throwException(Mage::helper('brands')->__('The Brand specified is assigned '));
        }

        // modify create / update dates
        if ($object->isObjectNew() && !$object->hasCreationTime()) {
            $object->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
        }

        $object->setUpdateTime(Mage::getSingleton('core/date')->gmtDate());

        return parent::_beforeSave($object);
    }


    protected function getIsUniqueUrl($object){
        $urls = Mage::getModel('brands/brand')->load($object ->getData('url_key'),'url_key');
        if($urls->getId() && $urls->getId() != Mage::app()->getRequest()->getParam('id')){
            return false;
        }
        return true;
    }


    protected function getBrandExists($object){
        $urls = Mage::getModel('brands/brand')->load($object ->getData('brand'),'brand');
        if($urls->getId() && $urls->getId() != Mage::app()->getRequest()->getParam('id')){
            return false;
        }
        return true;
    }




}

