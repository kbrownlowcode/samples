<?php class Brownlow_Brands_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    /**
     * Initialize Controller Router
     *
     * @param Varien_Event_Observer $observer
     */
    public function initControllerRouters($observer)
    {
        /* @var $front Mage_Core_Controller_Varien_Front */
        $front = $observer->getEvent()->getFront();

        $front->addRouter('brands', $this);
    }

    /**
     * Validate and Match Cms Page and modify request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }
        $path = trim($request->getPathInfo(),"/");
        $page = Mage::getModel('brands/brand')->load($path,'url_key');
        $pageId = $page->getId();

        if($pageId && $page->getEnabled()) {
            $request
                ->setRouteName('oem-parts')
                ->setControllerName('index')
                ->setActionName('index')
                ->setParam('page_id', $pageId);
            $request->setAlias(
                Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
                $path
            );
            return true;

        }

        return false;
    }



}