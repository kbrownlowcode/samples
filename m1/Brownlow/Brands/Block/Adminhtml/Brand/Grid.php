<?php

class Brownlow_Brands_Block_Adminhtml_Brand_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("brandGrid");
				$this->setDefaultSort("page_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("brands/brand")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("page_id", array(
				"header" => Mage::helper("brands")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "page_id",
				));
                
				$this->addColumn("url_key", array(
				"header" => Mage::helper("brands")->__("Url Key"),
				"index" => "url_key",
				));

			$this->addColumn("name", array(
				"header" => Mage::helper("brands")->__("Name"),
				"index" => "name",
				));
			$this->addColumn("brand", array(
				"header" => Mage::helper("brands")->__("Brand"),
				"index" => "brand",
				));
						$this->addColumn('enabled', array(
						'header' => Mage::helper('brands')->__('Status'),
						'index' => 'enabled',
						'type' => 'options',
						'options'=>Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
						));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('page_id');
			$this->getMassactionBlock()->setFormFieldName('page_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_brand', array(
					 'label'=> Mage::helper('brands')->__('Remove Brand'),
					 'url'  => $this->getUrl('*/adminhtml_brand/massRemove'),
					 'confirm' => Mage::helper('brands')->__('Are you sure?')
				));
			return $this;
		}
			

		

}