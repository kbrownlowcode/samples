<?php

class Brownlow_Brands_Block_Adminhtml_Brand_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {


        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("brands_form", array("legend" => Mage::helper("brands")->__("Item information")));


        $fieldset->addField("name", "text", array(
            "label" => Mage::helper("brands")->__("Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "name",
        ));

        $fieldset->addField("brand", "text", array(
            "label" => Mage::helper("brands")->__("Brand"),
            "class" => "required-entry",
            "required" => true,
            "name" => "brand",
        ));


        $fieldset->addField("url_key", "text", array(
            "label" => Mage::helper("brands")->__("Url Key"),
            "class" => "required-entry",
            "required" => true,
            "name" => "url_key",
        ));

        $fieldset->addField('enabled', 'select', array(
            'label' => Mage::helper('brands')->__('Status'),
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
            'name' => 'enabled',
        ));

        $fieldset->addField('content', 'editor', array(
            'name' => 'content',
            'label' => Mage::helper('brands')->__('Content'),
            'title' => Mage::helper('brands')->__('Content'),
            'style' => 'height:36em',
            'required' => true,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig()
        ));


        if (Mage::getSingleton("adminhtml/session")->getBrandData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getBrandData());
            Mage::getSingleton("adminhtml/session")->setBrandData(null);
        } elseif (Mage::registry("brand_data")) {
            $form->setValues(Mage::registry("brand_data")->getData());
        }
        return parent::_prepareForm();
    }

}
