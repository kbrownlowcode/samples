<?php


class Brownlow_Brands_Block_Adminhtml_Brand extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_brand";
	$this->_blockGroup = "brands";
	$this->_headerText = Mage::helper("brands")->__("Brand Manager");
	$this->_addButtonLabel = Mage::helper("brands")->__("Add New Item");
	parent::__construct();
	
	}

}