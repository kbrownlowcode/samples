<?php

/**
 * Retrieve Page instance
 *
 * @return Mage_Cms_Model_Page
 */
class Brownlow_Brands_Block_Brand extends Mage_Core_Block_Abstract
{
    public function getPage()
    {
        if (!$this->hasData('page')) {
            if ($this->getPageId()) {
                $page = Mage::getModel('brands/brand')
                    ->load($this->getBrandId());
            } else {
                $page = Mage::getSingleton('brands/brand');
            }
            $this->addModelTags($page);
            $this->setData('page', $page);
        }
        return $this->getData('page');
    }

    /**
     * Prepare global layout
     *
     * @return Mage_Cms_Block_Page
     */
    protected function _prepareLayout()
    {
        $page = $this->getPage();

        // show breadcrumbs
        if (Mage::getStoreConfig('web/default/show_cms_breadcrumbs')
            && ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs'))

        ) {
            $breadcrumbs->addCrumb('home', array('label' => Mage::helper('brands')->__('Home'), 'title' => Mage::helper('brands')->__('Go to Home Page'), 'link' => Mage::getBaseUrl()));
            $breadcrumbs->addCrumb('brands', array('label' => $page->getName(), 'title' => $page->getName()));
        }

        $root = $this->getLayout()->getBlock('root');
        if ($root) {
            $root->addBodyClass('brand-' . $page->getUrlKey());
        }


        return parent::_prepareLayout();
    }

    /**
     * Prepare HTML content
     *
     * @return string
     */
    protected function _toHtml()
    {
        /* @var $helper Mage_Cms_Helper_Data */
        if($this->hasData('page')) {
            $helper = Mage::helper('cms');
            $processor = $helper->getPageTemplateProcessor();
            $html = $processor->filter($this->getPage()->getContent());
            $html = $this->getMessagesBlock()->toHtml() . $html;
            return $html;
        }
        return "";
    }
}