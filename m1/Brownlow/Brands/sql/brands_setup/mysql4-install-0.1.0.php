<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
drop table chms_brand_page;
create table chms_brand_page(page_id int not null auto_increment, url_key varchar(100), content text, enabled tinyint(2), name varchar(120) not null, brand varchar(60) NOT NULL,  primary key(page_id));
		
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 