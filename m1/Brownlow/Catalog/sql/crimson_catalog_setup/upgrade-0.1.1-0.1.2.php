<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/22/2016 7:43 PM
 */

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'price_range_ids', array(
    'type'     => 'varchar',
    'label'    => 'Price Range - Product Ids',
    'input'    => 'text',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => false,
));

$installer->endSetup();
