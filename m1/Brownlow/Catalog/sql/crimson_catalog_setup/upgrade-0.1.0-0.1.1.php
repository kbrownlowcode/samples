<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/12/2016 7:27 AM
 * @brief
 */

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('Brownlow_catalog/product_oemparts'))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary' => true,
    ), 'Product ID')
    ->setComment('Catalog Product Oem Part Index');
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('Brownlow_catalog/product_non_oemparts'))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary' => true,
    ), 'Product ID')
    ->setComment('Catalog Product Non-Oem Part Index');
$installer->getConnection()->createTable($table);

$installer->endSetup();