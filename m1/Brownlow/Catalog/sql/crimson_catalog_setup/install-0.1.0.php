<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/11/2016 4:43 PM
 * @brief
 */

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->addAttribute(
    'catalog_category', 'exclude_from_landing_page', array(
        'type'     => 'int',
        'label'    => 'Exclude From Landing Page',
        'input'    => 'select',
        'source'   => 'eav/entity_attribute_source_boolean',
        'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required' => false,
        'default'  => 0,
        'group'    => 'Display Settings',
    )
);

$categoryIds = array(2517, 2518, 2519, 2520, 4946, 4947, 4948, 4949, 4950);
foreach ($categoryIds as $categoryId) {
    $category = Mage::getModel('catalog/category')->load($categoryId);
    $category->setData('exclude_from_landing_page', 1);
    $category->save();
}

/*
 * jjuleff@Brownlowagility.com
 * installs the allow_weee_calculation attribute to general.
 * this attribute only affects tax calculation
 * for certain categories on the list page (and maybe product view),
 * and does not affect the cart or checkout.
 **/

$installer->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, 'allow_weee_calculation', array(
        'group'            => 'General Information',
        'type'             => 'int',
        'label'            => 'Weee Tax Calculated',
        'source'           => 'eav/entity_attribute_source_boolean',
        'input'            => 'select',
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'          => true,
        'visible_on_front' => true,
        'required'         => true,
        'user_defined'     => false,
        'default'          => 0,
        'note'             => 'Should Weee be calculated on category list page. This will not affect cart/checkout.'
    )
);

$installer->endSetup();