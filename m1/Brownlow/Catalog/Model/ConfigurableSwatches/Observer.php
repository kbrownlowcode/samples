<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/11/2016 2:18 PM
 * @brief
 */

/**
 * Class Brownlow_Catalog_Model_ConfigurableSwatches_Observer
 */
class Brownlow_Catalog_Model_ConfigurableSwatches_Observer extends Mage_ConfigurableSwatches_Model_Observer
{
    /**
     * Per Crystal Ashby, 8/11/16 we will not do swatches on homepage, exit this process in this case.
     *
     * @param Varien_Event_Observer $observer
     */
    public function productListCollectionLoadAfter(Varien_Event_Observer $observer)
    {
        try {
            $action = Mage::app()->getFrontController()->getAction();
            if ($action && in_array($action->getFullActionName(),array('cms_index_index'))) {
                return;
            }
        } catch (Exception $e) {
        }

        parent::productListCollectionLoadAfter($observer);
    }
}