<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        7/15/2016 4:26 PM
 * @brief
 */

/**
 * Class Brownlow_Catalog_Model_Eav_Entity_Attribute_Source_Table
 */
class Brownlow_Catalog_Model_Eav_Entity_Attribute_Source_Table extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    /**
     * Retrieve Full Option values array
     *
     * @param bool $withEmpty       Add empty option to array
     * @param bool $defaultValues
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        //make sure the admin is working with fresh data.
        if (Mage::helper('Brownlow_catalog')->isAdmin()) {
            return parent::getAllOptions($withEmpty, $defaultValues);
        }

        $storeId = $this->getAttribute()->getStoreId();
        if (!is_array($this->_options)) {
            $this->_options = array();
        }
        if (!is_array($this->_optionsDefault)) {
            $this->_optionsDefault = array();
        }
        if (!isset($this->_options[$storeId])) {
            $key           = 'alloptions_' . $storeId . '_' . $this->getAttribute()->getId();
            if (($options = Mage::app()->loadCache($key))) {
                $options = unserialize($options);
                $this->_options[$storeId]        = $options['option_array'];
                $this->_optionsDefault[$storeId] = $options['option_array_default'];
            } else {
                $collection                      = Mage::getResourceModel('eav/entity_attribute_option_collection')
                    ->setPositionOrder('asc')
                    ->setAttributeFilter($this->getAttribute()->getId())
                    ->setStoreFilter($this->getAttribute()->getStoreId())
                    ->load();

                $this->_options[$storeId]        = $collection->toOptionArray();
                $this->_optionsDefault[$storeId] = $collection->toOptionArray('default_value');

                Mage::app()->saveCache(serialize(array(
                    'option_array' => $this->_options[$storeId],
                    'option_array_default' => $this->_optionsDefault[$storeId]
                )), $key, array(), 43200);
            }
        }
        $options = ($defaultValues ? $this->_optionsDefault[$storeId] : $this->_options[$storeId]);
        if ($withEmpty) {
            array_unshift($options, array('label' => '', 'value' => ''));
        }

        return $options;
    }
}