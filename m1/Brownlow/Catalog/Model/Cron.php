<?php

class Brownlow_Catalog_Model_Cron
{
    const VEHICLE_FILE_IMPORT_FILE = "var/import/fitments.csv";
    const VEHICLE_LOG_IMPORT_FILE = 'var/vehicles-list-import.csv.log';

    public function bootstrap($event = null)
    {
        $elite_path = Mage::getBaseDir() . '/app/code/local/Elite';
        defined('ELITE_PATH') or define('ELITE_PATH', $elite_path); // put path to app/code/local/Elite
        defined('ELITE_CONFIG_DEFAULT') or define('ELITE_CONFIG_DEFAULT', ELITE_PATH . '/Vaf/config.default.ini');
        defined('ELITE_CONFIG') or define('ELITE_CONFIG', ELITE_PATH . '/Vaf/config.ini');
        defined('MAGE_PATH') or define('MAGE_PATH', Mage::getBaseDir());
        require('Elite/vendor/autoload.php');

        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        VF_Singleton::getInstance()->setReadAdapter($read);
        VF_Singleton::getInstance()->setProcessURL(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK, isset($_SERVER['HTTPS'])) . 'vaf/ajax/process?');
    }

    public function importVehicleFitments()
    {

        $this->bootstrap();
        try {
            $writer = new Zend_Log_Writer_Stream(Mage::getBaseDir() . DS . self::VEHICLE_LOG_IMPORT_FILE, 'a+');
            $log    = new Zend_Log($writer);
        } catch (Exception $e) {
            Mage::logException($e);
        }
        $importer = new VF_Import_ProductFitments_CSV_Import(Mage::getBaseDir() . DS . self::VEHICLE_FILE_IMPORT_FILE);
        if (isset($log)) {
            $importer->setLog($log);
        }
        $importer->import();

        return;
    }

    public function indexPriceRangeIds()
    {
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE);

        Mage::getSingleton('core/resource_iterator')->walk(
            $collection->getSelect(),
            array(array($this, 'indexPriceRangeIdsCallback'))
        );
        return;
    }

    public function indexPriceRangeIdsCallback($args)
    {
        $product = Mage::getModel('catalog/product')
            ->setData($args['row']);

        Mage::helper('Brownlow_catalog')->updatePriceRange($product);
        return;
    }
}
