<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/12/2016 5:57 PM
 * @brief
 */

/**
 * Class Brownlow_Catalog_Model_Meta_Observer
 */
class Brownlow_Catalog_Model_Meta_Observer extends Amasty_Meta_Model_Observer
{
    public function updateCategoryProducts($observer)
    {
        if (Mage::registry('amlanding_page')) {
            return;
        }

        parent::updateCategoryProducts($observer);
    }
}