<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        7/15/2016 11:53 AM
 * @brief
 */

/**
 * Class Brownlow_Catalog_Model_Resource_ConfigurableSwatches_Catalog_Product_Attribute_Super_Collection
 */
class Brownlow_Catalog_Model_Resource_ConfigurableSwatches_Catalog_Product_Attribute_Super_Collection
    extends Mage_ConfigurableSwatches_Model_Resource_Catalog_Product_Attribute_Super_Collection
{
    /**
     * Load attribute option labels for current store and default (fallback)
     *
     * @return $this
     */
    protected function _loadOptionLabels()
    {
        if ($this->count()) {
            $labels = $this->_getOptionLabels();
            foreach ($this->getItems() as $item) {
                $item->setOptionLabels($labels);
            }
        }

        return $this;
    }

    /**
     * Get Option Labels
     *
     * @return array
     */
    protected function _getOptionLabels()
    {
        $attributeIds = $this->_getAttributeIds();

        $attributeHash = sha1(implode(',', $attributeIds));
        $storeHash     = md5(implode(',', array(Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID, $this->getStoreId())));
        $key           = 'super_option_labels_' . $attributeHash . '_' . $storeHash;

        if (($labels = Mage::app()->loadCache($key))) {
            $labels = unserialize($labels);
        } else {
            $select = $this->getConnection()->select();
            $select->from(array('options' => $this->getTable('eav/attribute_option')))
                ->join(
                    array('labels' => $this->getTable('eav/attribute_option_value')),
                    'labels.option_id = options.option_id',
                    array(
                        'label'    => 'labels.value',
                        'store_id' => 'labels.store_id',
                    )
                )
                ->where('options.attribute_id IN (?)', $attributeIds)
                ->where(
                    'labels.store_id IN (?)',
                    array(Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID, $this->getStoreId())
                );

            $resultSet = $this->getConnection()->query($select);
            $labels    = array();
            while ($option = $resultSet->fetch()) {
                $labels[$option['option_id']][$option['store_id']] = $option['label'];
            }
            Mage::app()->saveCache(serialize($labels), $key, array(Mage_Catalog_Model_Product::CACHE_TAG,), 604800);
        }

        return $labels;
    }

    /**
     * Get Attribute IDs
     *
     * @return array
     */
    protected function _getAttributeIds()
    {
        $attributeIds = array();
        foreach ($this->getItems() as $item) {
            $attributeIds[] = $item->getAttributeId();
        }
        return array_unique($attributeIds);
    }
}