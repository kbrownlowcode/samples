<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/12/2016 7:27 AM
 * @brief
 */

/**
 * Class Brownlow_Catalog_Model_Resource_Indexer_Product_Oemparts
 */
class Brownlow_Catalog_Model_Resource_Indexer_Product_Oemparts extends Mage_Index_Model_Resource_Abstract
{
    /**
     * Category table
     *
     * @var string
     */
    protected $_categoryTable;

    /**
     * Category product table
     *
     * @var string
     */
    protected $_categoryProductTable;

    /**
     * @var string
     */
    protected $_nonOemProductTable;

    /**
     * @var string
     */
    protected $_oemProductTable;

    /**
     * Product website table
     *
     * @var string
     */
    protected $_productWebsiteTable;

    /**
     * Store table
     *
     * @var string
     */
    protected $_storeTable;

    /**
     * Group table
     *
     * @var string
     */
    protected $_groupTable;

    /**
     * Array of info about stores
     *
     * @var array
     */
    protected $_storesInfo;

    /**
     * Model initialization
     *
     */
    protected function _construct()
    {
        $this->_init('Brownlow_catalog/product_oemparts', 'product_id');
        $this->_oemProductTable      = $this->getTable('Brownlow_catalog/product_oemparts');
        $this->_nonOemProductTable   = $this->getTable('Brownlow_catalog/product_non_oemparts');
        $this->_categoryProductTable = $this->getTable('catalog/category_product');
    }

    /**
     * Process product save.
     * Method is responsible for index support
     * when product was saved and assigned categories was changed.
     *
     * @param Mage_Index_Model_Event $event
     *
     * @return Brownlow_Catalog_Model_Resource_Indexer_Product_Oemparts
     */
    public function catalogProductSave(Mage_Index_Model_Event $event)
    {
        $productId = $event->getEntityPk();
        $data      = $event->getNewData();

        /**
         * Check if category ids were updated
         */
        if (!isset($data['category_ids'])) {
            return $this;
        }

        /**
         * Determine if product is in oem part.
         */
        $isOemPart = $this->_getHelper()->isProductIdOemPart($productId);

        if ($isOemPart) {
            $this->_getWriteAdapter()->delete(
                $this->_nonOemProductTable,
                array('product_id = ?' => $productId)
            );
            $this->_getWriteAdapter()->insertIgnore($this->_oemProductTable, array('product_id' => $productId));
        } else {
            $this->_getWriteAdapter()->delete(
                $this->_oemProductTable,
                array('product_id = ?' => $productId)
            );
            $this->_getWriteAdapter()->insertIgnore($this->_nonOemProductTable, array('product_id' => $productId));
        }

        return $this;
    }

    /**
     * Process category index after category save
     *
     * @param Mage_Index_Model_Event $event
     */
    public function catalogCategorySave(Mage_Index_Model_Event $event)
    {
        $data = $event->getNewData();

        /**
         * Check if we have reindex category move results
         */
        if (isset($data['affected_category_ids'])) {
            $categoryIds = $data['affected_category_ids'];
        } elseif (isset($data['products_was_changed'])) {
            $categoryIds = array($event->getEntityPk());
        } else {
            return;
        }

        //we can't know what products are where after a category has been changed.
        $this->reindexAll();
    }

    /**
     * Rebuild all index data
     *
     * @return Brownlow_Catalog_Model_Resource_Indexer_Product_Oemparts
     */
    public function reindexAll()
    {
        $this->useIdxTable(false);
        $this->beginTransaction();
        try {
            $oemProductIdQuery    = $this->_getHelper()->getOemProductIdQuery(false);
            $nonOemProductIdQuery = $this->_getHelper()->getNonOemProductIdQuery(false);
            $idxAdapter           = $this->_getIndexAdapter();

            //delete from oem parts table where product id is not oem part
            $idxAdapter->delete(
                $this->_oemProductTable, $idxAdapter->quoteInto('product_id NOT IN (?)', $oemProductIdQuery)
            );

            //delete from non oem parts table where product id is oem part
            $idxAdapter->delete(
                $this->_nonOemProductTable, $idxAdapter->quoteInto('product_id IN (?)', $oemProductIdQuery)
            );

            //insert oem parts into oem parts table
            $insertSelect = $idxAdapter->insertFromSelect(
                $oemProductIdQuery, $this->_oemProductTable, array(), Varien_Db_Adapter_Interface::INSERT_IGNORE
            );
            $idxAdapter->query($insertSelect);

            //insert non oem parts into non oem parts table.
            $insertSelect = $idxAdapter->insertFromSelect(
                $nonOemProductIdQuery, $this->_nonOemProductTable, array(), Varien_Db_Adapter_Interface::INSERT_IGNORE
            );
            $idxAdapter->query($insertSelect);

            $this->commit();
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }

        return $this;
    }

    protected function _getHelper()
    {
        return Mage::helper('Brownlow_catalog');
    }
}