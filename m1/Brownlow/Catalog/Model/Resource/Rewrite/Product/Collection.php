<?php
/** FALLBACK array_column only exists on version 5.5 and above */
if (!function_exists("array_column")) {
    function array_column($array, $column_name)
    {
        return array_map(function ($element) use ($column_name) {
            return $element[$column_name];
        }, $array);
    }
}

class Brownlow_Catalog_Model_Resource_Rewrite_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{

    protected function _productLimitationPrice($joinLeft = false)
    {
        $filters = $this->_productLimitationFilters;
        if (empty($filters['use_price_index'])) {
            return $this;
        }

        $helper = Mage::getResourceHelper('core');
        $connection = $this->getConnection();
        $select = $this->getSelect();

        $fromPart = $select->getPart(Zend_Db_Select::FROM);
        $sortPart = $select->getPart(Zend_Db_Select::ORDER);
        $columns = array_column($select->getPart(Zend_Db_Select::COLUMNS), 1);

        $dir = 'asc';

        if (in_array('price_range_ids', $columns)) {
            foreach ($sortPart as $_sort) {
                if (is_array($_sort)) {
                    if (strpos('price_index', $_sort[0]) && $_sort[1] == 'desc') {
                        $dir = 'desc';
                    }
                }
            }
            $subquery = "(SELECT entity_id FROM catalog_product_index_price WHERE entity_id IN (`e`.`price_range_ids`) ORDER BY min_price $dir LIMIT 1)";
            $entityId = $connection->getCheckSql('e.type_id = "configurable"', $subquery, 'e.entity_id');
            $joinCond = join(' AND ', array(
                'price_index.entity_id = ' . $entityId,
                $connection->quoteInto('price_index.website_id = ?', $filters['website_id']),
                $connection->quoteInto('price_index.customer_group_id = ?', $filters['customer_group_id'])
            ));
        } else {
            $joinCond = join(' AND ', array(
                'price_index.entity_id = e.entity_id',
                $connection->quoteInto('price_index.website_id = ?', $filters['website_id']),
                $connection->quoteInto('price_index.customer_group_id = ?', $filters['customer_group_id'])
            ));
        }

        if (!isset($fromPart['price_index'])) {
            $least = $connection->getLeastSql(array('price_index.min_price', 'price_index.tier_price'));
            $minimalExpr = $connection->getCheckSql('price_index.tier_price IS NOT NULL',
                $least, 'price_index.min_price');
            $colls = array('price', 'tax_class_id', 'final_price',

                'minimal_price' => $minimalExpr, 'min_price', 'max_price', 'tier_price');
            $tableName = array('price_index' => $this->getTable('catalog/product_index_price'));
            if ($joinLeft) {
                $select->joinLeft($tableName, $joinCond, $colls);
            } else {
                $select->join($tableName, $joinCond, $colls);
            }
            // Set additional field filters
            foreach ($this->_priceDataFieldFilters as $filterData) {
                $select->where(call_user_func_array('sprintf', $filterData));
            }
        } else {
            $fromPart['price_index']['joinCondition'] = $joinCond;
            $select->setPart(Zend_Db_Select::FROM, $fromPart);
        }
        //Clean duplicated fields
        $helper->prepareColumnsList($select);


        return $this;
    }
}