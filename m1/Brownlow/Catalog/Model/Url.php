<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/30/2016 2:57 PM
 */

class Brownlow_Catalog_Model_Url extends Mage_Catalog_Model_Url
{
//    public function refreshRewrites($storeId = null)
//    {
//        return $this;
//    }
//
//    protected function _refreshCategoryRewrites(Varien_Object $category, $parentPath = null, $refreshProducts = true)
//    {
//        return $this;
//    }
//
    protected function _refreshProductRewrite(Varien_Object $product, Varien_Object $category)
    {
        $rootCategory = $this->getStoreRootCategory($category->getStoreId());

        if($category->getId() == $rootCategory->getId()) {
            return parent::_refreshProductRewrite($product, $category);
        }

        return $this;
    }

//    protected function _refreshCategoryProductRewrites(Varien_Object $category)
//    {
//        return $this;
//    }
//
//    public function refreshCategoryRewrite($categoryId, $storeId = null, $refreshProducts = true)
//    {
//        return $this;
//    }
//
//    public function refreshProductRewrite($productId, $storeId = null)
//    {
//        return $this;
//    }
//
//    public function refreshProductRewrites($storeId)
//    {
//        return $this;
//    }
}
