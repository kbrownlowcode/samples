<?php
/**
 *
 */
class Brownlow_Catalog_Model_Cache_Vf_Vehiclesearch extends Enterprise_PageCache_Model_Container_Abstract
{
    /**
     * Get cache identifier
     *
     * @return string
     */
    protected function _getCacheId()
    {
        return 'vaf_search_button_' . md5($_COOKIE["frontend"]);
    }

    public function applyInApp(&$content)
    {
        $block = $this->_getPlaceHolderBlock();

        $blockContent = $block->toHtml();

        $this->_applyToContent($content, $blockContent);

        return true;
    }

    protected function _saveCache($data, $id, $tags = array(), $lifetime = null)
    {
        return false;
    }
}