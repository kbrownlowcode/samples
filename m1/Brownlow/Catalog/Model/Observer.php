<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/11/2016 5:12 PM
 * @brief
 */

/**
 * Class Brownlow_Catalog_Model_Observer
 */
class Brownlow_Catalog_Model_Observer
{
    const APPLIED_OEM_FILTER_KEY = '_applied_oem_filter';

    public function catalogProductCollectionApplyLimitationsBefore(Varien_Event_Observer $observer)
    {
        if (!$this->_getHelper()->shouldExcludeOemParts()) {
            return;
        }

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = $observer->getCollection();

        $categoryId = $observer->getCategoryId();
        if (!$categoryId) {
            return;
        }

        $this->_joinNonOemParts($collection);
    }

    //21.76
    public function catalogProductCollectionLoadBefore(Varien_Event_Observer $observer)
    {
        if (!$this->_getHelper()->shouldExcludeOemParts()) {
            return;
        }

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = $observer->getCollection();
        $this->_joinNonOemParts($collection);
    }

    /**
     * by joining on-oem part index, we remove oem parts from collection.
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     *
     * @return $this
     */
    protected function _joinNonOemParts($collection)
    {
        //don't apply same join twice.
        if ($collection->getFlag(self::APPLIED_OEM_FILTER_KEY)) {
            return $this;
        }

        $alias = Mage::helper('core')->uniqHash('non_oem_');
        $collection->getSelect()
            ->joinInner(array($alias => $this->_getResource()->getTableName('Brownlow_catalog/product_non_oemparts')),
                $alias.'.product_id=e.entity_id',''
            );

        $collection->setFlag(self::APPLIED_OEM_FILTER_KEY,true);

        return $this;
    }
    
    public function catalogProductSaveAfter($observer)
    {
        $product = $observer->getEvent()->getProduct();

        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
            Mage::helper('Brownlow_catalog')->updatePriceRange($product);
        }
    }

    /**
     * @return \Brownlow_Catalog_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('Brownlow_catalog');
    }

    /**
     * @return Mage_Core_Model_Resource
     */
    protected function _getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    /**
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    protected function _getConnection()
    {
        return getResource()->getConnection('core_read');
    }
}