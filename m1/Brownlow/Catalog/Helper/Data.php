<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        7/12/2016 1:48 PM
 * @brief
 */

/**
 * Class Brownlow_Catalog_Helper_Data
 */
class Brownlow_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_oemPartProductIdQuery = null;
    protected $_nonOemPartProductIdQuery = null;
    protected $_shouldExcludeOemParts = null;
    protected $_excludedCategoryIdsFromLandingPages = null;

    public function isAdmin()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return true;
        }

        if (Mage::getDesign()->getArea() == 'adminhtml') {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getCategoryIdsExcludedFromLandingPages()
    {
        if (is_null($this->_excludedCategoryIdsFromLandingPages)) {
            $this->_excludedCategoryIdsFromLandingPages = Mage::getResourceModel('catalog/category_collection')
                ->addAttributeToFilter('exclude_from_landing_page', 1)->getAllIds();
        }

        return $this->_excludedCategoryIdsFromLandingPages;
    }

    /**
     * @return bool
     */
    public function shouldExcludeOemParts()
    {
        if (is_null($this->_shouldExcludeOemParts)) {
            $action = Mage::app()->getFrontController()->getAction();
            if (!$action) {
                $this->_shouldExcludeOemParts = false;
            } else {
                //full action names in this list should exclude oem parts.
                $this->_shouldExcludeOemParts = in_array(
                    $action->getFullActionName(), array(
                        'cms_index_index',
                        'amlanding_page_view',
                    )
                );
            }
        }

        return $this->_shouldExcludeOemParts;
    }

    /**
     * @param bool $useCache
     *
     * @param null $productId
     *
     * @return \Varien_Db_Select
     */
    public function getOemProductIdQuery($useCache = true, $productId = null)
    {
        if (is_null($this->_oemPartProductIdQuery) || !$useCache || !is_null($productId)) {
            $alias = Mage::helper('core')->uniqHash('ccp_tmp_');
            $select = $this->_getConnection()->select()
                ->from(array($alias => $this->_getResource()->getTableName('catalog/category_product')), 'product_id')
                ->distinct(true)
                ->where($alias.'.category_id IN (?)', $this->getCategoryIdsExcludedFromLandingPages());

            if (!is_null($productId)) {
                if (is_array($productId)) {
                    $select->where($alias . '.product_id IN (?)', $productId);
                } else {
                    $select->where($alias . '.product_id = ?', $productId);
                }
            }

            if (!$useCache || !is_null($productId)) {
                return $select;
            }

            $this->_oemPartProductIdQuery = $select;
        }

        return $this->_oemPartProductIdQuery;
    }

    /**
     * @param bool $useCache
     *
     * @return \Varien_Db_Select
     */
    public function getNonOemProductIdQuery($useCache = true)
    {
        if (is_null($this->_nonOemPartProductIdQuery) || !$useCache) {
            $alias = Mage::helper('core')->uniqHash('ccp_tmp_');
            $select = $this->_getConnection()->select()
                ->from(array($alias => $this->_getResource()->getTableName('catalog/category_product')), 'product_id')
                ->distinct(true)
                ->where($alias.'.category_id NOT IN (?)', $this->getCategoryIdsExcludedFromLandingPages());

            if (!$useCache) {
                return $select;
            }

            $this->_nonOemPartProductIdQuery = $select;
        }

        return $this->_nonOemPartProductIdQuery;
    }

    public function isProductIdOemPart($productId)
    {
        $select = $this->getOemProductIdQuery(false, $productId);
        return (bool) $this->_getConnection()->fetchOne($select);
    }

    public function excludeOemPartsFromProductCollection(Mage_Catalog_Model_Resource_Product_Collection $collection)
    {
        $this->excludeOemPartsFromProductSelect($collection->getSelect());

        return $this;
    }

    public function excludeOemPartsFromProductSelect(Varien_Db_Select $select, $alias = 'e')
    {
        $select->where($alias.'.entity_id NOT IN (?)', $this->getOemProductIdQuery());
    }

    /**
     * @return Mage_Core_Model_Resource
     */
    protected function _getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    /**
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    protected function _getConnection()
    {
        return $this->_getResource()->getConnection('core_read');
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @throws Exception
     */
    public function updatePriceRange(Mage_Catalog_Model_Product $product)
    {
        $min          = null;
        $max          = null;
        $productIdMin = null;
        $productIdMax = null;

        $simpleProducts = $product->getTypeInstance()->getUsedProductIds();

        $product->setData('price_range_ids', implode(",", $simpleProducts));

        $product
            ->getResource()
            ->saveAttribute($product, 'price_range_ids');
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param string $type
     * @return array
     */
    public function getConfigurablePriceRange(Mage_Catalog_Model_Product $product, $type = 'final')
    {
        if (!$product->hasData('price_range_ids')) {
          return  array(
                'min' => 0,
                'max' => 0,
            );
        }
        $ids = explode(',', $product->getData('price_range_ids'));
    /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(array(
                'price',
                'special_price',
                'group_price',
                'tier_price',
                'msrp'
            ))
            ->addAttributeToFilter('entity_id', array('in' => array($ids)));

        $min = 0;
        $max = 0;

    /** @var Mage_Catalog_Model_Product $product */
        foreach ($collection as $product) {
            switch ($type){
                case 'price':
                    $price = $product->getPrice();
                    break;
                case 'special':
                    $price = $product->getSpecialPrice();
                    break;
                case 'msrp':
                    $price = $product->getMsrp();
                    break;
                default :
                    $price = $product->getFinalPrice();
                    break;
            }

            if (!$min || $price < $min) {
                $min = $price;
            }

            if (!$max || $price > $max) {
                $max = (float)$price;
            }
        }


        return array(
                'min' => (float)$min,
                'max' => (float)$max,
            );
    }
}
