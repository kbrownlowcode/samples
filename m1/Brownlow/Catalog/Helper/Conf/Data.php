<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/11/2016 2:54 PM
 * @brief
 */

/**
 * Class Brownlow_Catalog_Helper_Conf_Data
 */
class Brownlow_Catalog_Helper_Conf_Data extends Amasty_Conf_Helper_Data
{

    /**
     * This rewrite attempts to reduce the amount of data from mysql, we only need first and last record.  in pre-production
     * tests this change resulted in a 1/2 the amount of time needed for the equivalent single call with no limit specified.
     *
     * set configurable price as min from simple price
    * templates:
    * app\design\frontend\base\default\template\catalog\product\view\tierprices.phtml
    * app\design\frontend\base\default\template\catalog\product\price.phtml
    * $_product = Mage::helper('amconf')->getSimpleProductWithMinPrice($_product);
    */
    public function getSimpleProductWithMinPrice($_product)
    {
        $flag           = true;

        $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($_product);

        /** @var Mage_Catalog_Model_Resource_Product_Type_Configurable_Product_Collection $firstCollection */
        $firstCollection = $conf->getUsedProductCollection()
            ->addAttributeToSelect('*')
            ->addFilterByRequiredOptions()
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addStoreFilter(Mage::app()->getStore()->getId());

        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($firstCollection);

        $firstCollection->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents();
        $firstCollection->getSelect()->reset('order');
        $firstCollection->getSelect()->limit(1);

        $lastCollection = clone $firstCollection;

        $firstCollection->getSelect()->order('minimal_price', 'asc');
        $lastCollection->getSelect()->order('minimal_price', 'desc');

        $first = $firstCollection->getFirstItem();
        $last  = $lastCollection->getFirstItem();

        if ($first->getMinimalPrice() == $last->getMinimalPrice()) {
            $flag = false;
        }

        return array($first, $flag);
    }
}