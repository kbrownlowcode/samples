<?php
/**
 * @namespace   Brownlow
 * @module      Catalog
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/5/2016 9:08 AM
 * @brief       Remove global wild card search for text fields on admin grid.  This increases speed by actually using
 *              indexes.
 */

/**
 * Class Brownlow_Catalog_Block_Adminhtml_Widget_Grid_Column_Filter_Text
 */
class Brownlow_Catalog_Block_Adminhtml_Widget_Grid_Column_Filter_Text
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Text
{
    /**
     * Retrieve condition
     *
     * @return array
     */
    public function getCondition()
    {
        $helper         = Mage::getResourceHelper('core');
        $likeExpression = $helper->addLikeEscape($this->getValue(), array('position' => 'start'));

        $message = $this->__(
            'Notice: Your search was not performed as a wildcard search.  Only entries starting with "%s" will be returned.',
            $this->getValue()
        );
        $message = Mage::getSingleton('core/message')->notice($message);

        /** @var Mage_Adminhtml_Controller_Action $controllerAction */
        $controllerAction = Mage::app()->getFrontController()->getAction();

        if ($controllerAction instanceof Mage_Adminhtml_Controller_Action) {
            Mage::getSingleton('adminhtml/session')->addUniqueMessages($message);
            $controllerAction->initLayoutMessages('adminhtml/session');
        }

        return array('like' => $likeExpression);
    }
}