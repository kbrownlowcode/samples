<?php
/**
 * @namespace   Brownlow
 * @module      PageCache
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/17/2016 3:14 PM
 */

class Brownlow_PageCache_Model_Resource_Crawler extends Enterprise_PageCache_Model_Resource_Crawler
{
    public function getRequestPaths($storeId, $batchSize, $offset)
    {
        $selectCategory = $this->_getReadAdapter()->select()
            ->from(array('url_rewrite' => $this->getTable('enterprise_urlrewrite/url_rewrite')),
                array(
                    'request_path',
                    'entity_type',
                    'category_id' => new Zend_Db_Expr('NULL'),
                )
            )
            ->where('url_rewrite.store_id = ?', $storeId)
            ->where('url_rewrite.entity_type = ?', Enterprise_Catalog_Model_Category::URL_REWRITE_ENTITY_TYPE)
            ->limit($batchSize, $offset);

        return $this->_getReadAdapter()->fetchAll($selectCategory);
    }
}
