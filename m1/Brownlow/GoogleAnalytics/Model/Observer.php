<?php
/**
 * @namespace   Brownlow
 * @module      GoogleAnalytics
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        9/1/16 9:34 PM
 */

class Brownlow_GoogleAnalytics_Model_Observer
{
    public function checkoutCartSaveBefore(Varien_Event_Observer $observer)
    {
        $cart = clone $observer->getEvent()->getCart();

        $add    = array();
        $remove = array();

        foreach ($cart->getQuote()->getAllVisibleItems() as $item) {
            $product = $item->getProduct();

            /**
             * NEW ITEM TO THE CART
             */

            if ($item->isObjectNew()) {
                $add[] = array(
                    'id'       => $product->getSku(),
                    'name'     => $product->getName(),
                    'category' => 'None',
                    'brand'    => 'None',
                    'variant'  => 'None',
                    'price'    => $item->getPrice(),
                    'quantity' => $item->getQty(),
                );

                continue;
            }

            /**
             * EXISTING ITEM TO THE CART
             */

            $oldQty = $item->getOrigData('qty');
            $newQty = $item->getData('qty');

            if ($oldQty != $newQty) {
                if ($oldQty < $newQty) {
                    $add[] = array(
                        'id'       => $product->getSku(),
                        'name'     => $product->getName(),
                        'category' => 'None',
                        'brand'    => 'None',
                        'variant'  => 'None',
                        'price'    => $item->getPrice(),
                        'quantity' => $newQty - $oldQty,
                    );
                } else {
                    $remove[] = array(
                        'id'       => $product->getSku(),
                        'name'     => $product->getName(),
                        'category' => 'None',
                        'brand'    => 'None',
                        'variant'  => 'None',
                        'price'    => $item->getPrice(),
                        'quantity' => $oldQty - $newQty,
                    );
                }
            }
        }


        /**
         * REMOVED ITEMS FROM THE CART
         */

        $itemIds = array();

        foreach ($cart->getQuote()->getAllVisibleItems() as $item) {
            $itemIds[] = $item->getId();
        }

        $originalQuote = Mage::getModel('sales/quote')->load($cart->getQuote()->getEntityId());

        foreach ($originalQuote->getAllVisibleItems() as $item) {
            $product = $item->getProduct();

            if (!in_array($item->getId(), $itemIds)) {
                $remove[] = array(
                    'id'       => $product->getSku(),
                    'name'     => $product->getName(),
                    'category' => 'None',
                    'brand'    => 'None',
                    'variant'  => 'None',
                    'price'    => $item->getPrice(),
                    'quantity' => $item->getQty(),
                );
            }
        }

        /**
         * ADD ITEMS TO SESSION
         */

        $session = Mage::getSingleton('core/session');

        $existingData = $session->getGoogleAnalyticsAddToCartItems();

        if ($existingData) {
            if (isset($existingData['add'])) {
                foreach ($existingData['add'] as $item) {
                    $add[] = $item;
                }
            }

            if (isset($existingData['remove'])) {
                foreach ($existingData['remove'] as $item) {
                    $remove[] = $item;
                }
            }
        }

        $data = array();

        if ($add) {
            $data['add'] = $add;
        }

        if ($remove) {
            $data['remove'] = $remove;
        }

        if ($data) {
            $session->setGoogleAnalyticsAddToCartItems($data);
        }
    }
}
