<?php
/**
 * Created by PhpStorm.
 * User: jarrod
 * Date: 8/25/16
 * Time: 11:41 AM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$table = $installer
    ->getConnection()
    ->newTable($installer->getTable('Brownlow_sitemap/Brownlow_sitemap'))
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
        ),'UId'
    )
    ->addColumn(
        'start_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
        array(
            'nullable' => true,
            'default' => null,
        ),
        'start_time'
    )
    ->addColumn(
        'modified_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
        array(
            'nullable' => true,
            'default' => null,
        ),
        'modified_time'
    )
    ->addColumn(
        'mode', Varien_Db_Ddl_Table::TYPE_TINYINT, null,
        array(),
        'mode'
    )
    ->addColumn(
        'active', Varien_Db_Ddl_Table::TYPE_TINYINT, null,
        array(
            'nullable' => false,
            'default' => 0
        ),
        'is xml being built currently'
    )
    ->addColumn(
        'count', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'nullable' => false,
            'default' => 0
        ),
        'total urls'
    )
    ->addColumn(
        'current_file_number', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'nullable' => false,
            'default' => 0
        ),
        'current file number being worked on'
    )
    ->addColumn(
        'current_file_count', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'nullable' => false,
            'default' => 0
        ),
        'current number in file'
    )
    ->addColumn(
        'product_ids',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        Varien_Db_Ddl_Table::MAX_TEXT_SIZE,
        array(),
        'Product ids (serialized array) included in sitemap'
    )
    ->addColumn(
        'files',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        Varien_Db_Ddl_Table::MAX_TEXT_SIZE,
        array(),
        'files being used (serialized array) during build'
    )
;
$installer->getConnection()->createTable($table);

$installer->endSetup();