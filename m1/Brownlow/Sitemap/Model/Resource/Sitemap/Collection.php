<?php

/**
 * jjuleff@Brownlowagility.com
 * 2016-08-25
 */
class Brownlow_Sitemap_Model_Resource_Sitemap_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('Brownlow_sitemap/sitemap');
    }

    public function reset()
    {
        $this->clear();
        $this->getSelect()->reset('where');
    }
}