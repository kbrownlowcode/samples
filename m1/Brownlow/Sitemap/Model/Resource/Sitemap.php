<?php
/**
 * jjuleff@Brownlowagility.com
 * 2016-08-25
 */

class Brownlow_Sitemap_Model_Resource_Sitemap extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('Brownlow_sitemap/Brownlow_sitemap', 'entity_id');
    }
}