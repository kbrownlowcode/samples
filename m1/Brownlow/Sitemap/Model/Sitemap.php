<?php

/**
 * jjuleff@Brownlowagility.com
 * 2016-08-25
 * */
error_reporting(E_ALL);
Mage::setIsDeveloperMode(true);
ini_set("display_errors", 1);

class Brownlow_Sitemap_Model_Sitemap extends Mage_Core_Model_Abstract
{
    protected $_productsIds = array();
    protected $_files = array();
    /**
     * @var $_sitemap          Brownlow_Sitemap_Model_Resource_Sitemap
     */
    protected $_sitemap;

    protected $_startTime;
    protected $_storeId;

    public function _construct()
    {
        parent::_construct();
        $this->_init('Brownlow_sitemap/sitemap');
    }

    //gets time script has been running, or time difference from timestamp to now.
    //in seconds.
    public function getTimeDifference($timestamp = false, $datetonum = false)
    {
        if ($timestamp) {
            if ($datetonum) {
                $timestamp = strtotime($timestamp);
            }
            return (time() - $timestamp) / 60;
        }
        if (is_numeric($this->_startTime))
        {
            return (time() - $this->_startTime) / 60;
        }
        $this->_startTime = time();
        $this->setStartTime($this->_startTime);
        return 0;
    }

    public function getModifiedTimestamp()
    {
        $time = $this->getModifiedTime();
        if (!is_null($time))
        {
            return strtotime($time);
        }
        $this->setModifiedTime(time());
        return 0;
    }

    //this is expected to be fired off by a cron.
    public function run($storeId = false)
    {
        $customSession = false;
        if (!isset($_SESSION)) {
            session_start();
            $customSession = true;
        }

        if ($storeId === false)
        {
            $this->_storeId    = Mage::app()
                ->getWebsite(true)
                ->getDefaultGroup()
                ->getDefaultStoreId();
        }
        $collection = $this->getCollection();
        $this->getTimeDifference();

        if ($collection->getSize() == 0) {
            $this->setData('mode', 0);
            $this->setStartTime(time());
            $this->setModifiedTime(time());
            $this->save();
        } else {
            $collection->addFieldToFilter('active', 1);
            if ($collection->count()) {
                $this->load($collection->getFirstItem()->getData('entity_id'));
                $timestamp = $this->getModifiedTimestamp();
                //assumes a crash and resets
                if ($this->getTimeDifference($timestamp) > ((double)75) ) {
                    $this->setData('active', 0);
                    $this->save();
                } else {
                    //the script should already be running.
                    return;
                }
            } else {
                $collection->reset();
                $this->load(
                    $collection
                        ->addOrder("modified_time", Varien_Data_Collection_Db::SORT_ORDER_DESC)
                        ->getFirstItem()
                        ->getId()
                );
                $difference = $this->getTimeDifference($this->getModifiedTimestamp());
                //if it has been 48 hours since we last rebuilt, its time to rebuild.
                if ($difference > 2880) {
                    $this->setData('entity_id', null);
                    $this->setMode(0);
                    $this->setCount(0);
                    $this->setCurrentFileCount(0);
                    $this->setCurrentFileNumber(0);
                    $this->_files = array();
                    $this->save();

                }
            }
        }

        //prepare sitemap.
        if ($this->getMode() == 0) {
            $this->_mode0();
        }
        //build sitemap.
        if ($this->getMode() == 1) {
            $this->_mode1();
        }
        //move files
        if ($this->getMode() == 2) {
            $this->_mode2();
        }
        //mode 3 do nothing.
        if ($customSession) {
            session_destroy();
        }
    }

    protected function _mode0()
    {
        //set up for new sitemap.
        $config  = VF_Singleton::getInstance()->getConfig();
        $this->_sitemap = new Elite_Vafsitemap_Model_Sitemap_Product_XML($config);
        $this->_sitemap->setStoreId($this->_storeId);
        $size = $this->_sitemap->productCount();
        $this->_productsIds = $this->_sitemap->getCachedProductsIds();
        if (!empty($this->_productsIds))
        {
            $this->setMode(1);
            $this->save();
        }

        $path = Mage::getBaseDir() . '/var/vaf-sitemap-xml-temp';

        if (file_exists($path)) {
            $this->vafSitemapRecursiveDelete($path);
        }
        @mkdir($path);
    }

    protected function _mode1()
    {
        $this->setActive(1);
        $this->save();

        try {
            if (!is_object($this->_sitemap)) {
                $config         = VF_Singleton::getInstance()->getConfig();
                $this->_sitemap = new Elite_Vafsitemap_Model_Sitemap_Product_XML($config);
                $this->_sitemap->setStoreId($this->_storeId);
                $this->_sitemap->setCachedProductsIds($this->_productsIds);
            }

            $limit = ini_get('memory_limit');
            $unit  = substr($limit, -1);
            $limit = intval($limit);
            //convert memory limit to a usable number for our while loop.
            if ($unit == 'K' || $unit == 'M' || $unit == 'G') {
                $limit *= 1024;
            }
            if ($unit == 'M' || $unit == 'G') {
                $limit *= 1024;
            }
            if ($unit == 'G') {
                $limit *= 1024;
            }
            //90 percent of memory limit.
            $limit = round($limit * .9);

            $path      = Mage::getBaseDir() . '/var/vaf-sitemap-xml-temp';
            $chunkSize = 49900;
            if (!isset($this->_files['active_file']) || empty($this->_files['active_file'])) {
                $this->_files['active_file'] = $path . '/' . $this->getCurrentFileNumber() . '.xml';
                $fh                          = @fopen($this->_files['active_file'], 'a');
                $header                      = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
                fwrite($fh, $header);
                $this->resetFileCount();
            } else {
                $fh = @fopen($this->_files['active_file'], 'a');
            }
            $write        = '';
            $writeCounter = 100;
            //loops through all our products before our memory runs out.
            while (
                ($productId = $this->getNextCachedProductsId())
                && (memory_get_usage() < $limit)
            ) {

                set_time_limit(300);
                //gets a line of xml and drops it in memory for write.
                $xmlArray = $this->_sitemap->xmlOne($this->_storeId, $productId);
                if ($xmlArray !== false && ($xmlArray['counter'] > 0)) {
                    $this->count($xmlArray['counter']);
                    $write .= $xmlArray['xml'];
                }
                //writes every 100 products
                if ($writeCounter-- < 1) {
                    fwrite($fh, $write);
                    $write        = '';
                    $writeCounter = 100;

                    $this->save();
                }

                //writes when chunk size is reached.  xml may contain more than 1 link, so chunk size is below 50K to allow for a product to have up to 100
                //urls associated with it.
                if ($this->getCurrentFileCount() >= $chunkSize) {
                    //writes and closes out the file.
                    fwrite($fh, $write);
                    $write = '';
                    fwrite($fh, '</urlset>');
                    fclose($fh);
                    $file = $this->_files['active_file'];
                    $this->incrementFileNumber();
                    $this->_files['active_file'] = $path . '/' . $this->getCurrentFileNumber() . '.xml';
                    array_push($this->_files, basename($file));
                    $fh     = @fopen($this->_files['active_file'], 'a');
                    $header = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
                    fwrite($fh, $header);
                    $this->resetFileCount();
                }
            }
            if (!empty($write)) {
                fwrite($fh, $write);
                $write = '';
            }
            //this indicates that there are no more products to be processed.
            if ($productId === false) {
                fwrite($fh, '</urlset>');
                $file = $this->_files['active_file'];
                array_push($this->_files, basename($file));
                unset($this->_files['active_file']);
                //writes our sitemap index.
                file_put_contents($path . '/sitemap-index.xml', $this->_sitemap->sitemapIndex($this->_files));
                $this->setMode(2);
            }
            if (is_resource($fh)) {
                fclose($fh);
            }
        }
        catch (Exception $e)
        {
            Mage::log($e->getMessage(),null,'vaf-sitemap-generation.log',true);
            Mage::log($e->getTraceAsString(),null,'vaf-sitemap-generation.log',true);
        }

        $this->setActive(0);
        $this->save();
    }

    protected function _mode2()
    {
        $path     = Mage::getBaseDir() . '/var/vaf-sitemap-xml-temp';
        $basePath = Mage::getBaseDir() . '/var/vaf-sitemap-xml';
        $this->vafSitemapRecursiveDelete($basePath);
        @mkdir($basePath);
        $files = scandir($path);
        foreach ($files as $file) {
            if ($file != "." && $file != "..") {
                rename($path.'/'.$file, $basePath.'/'.$file);
            }
        }
        if (file_exists($path)) {
            $this->vafSitemapRecursiveDelete($path);
        }
        $this->setMode(3);
        $this->save();
    }

    public function getCount() {
        $count = $this->getData('count');
        if (!is_numeric($count))
        {
            $this->setData('count',0);
            $count = 0;
        }
        return $count;

    }

    public function getCurrentFileNumber()
    {
        $number = $this->getData('current_file_number');
        if (!is_numeric($number))
        {
            $number = 0;
            $this->setData('current_file_number', 0);
        }
        return $number;
    }

    public function incrementFileNumber()
    {
        $number = $this->getCurrentFileNumber();
        $number++;
        $this->setData('current_file_number', $number);
        return $number;
    }

    public function count($inc = null)
    {
        if (is_null($inc)) $inc = 1;
        if ($inc == 0 || !is_numeric($inc)) return;
        $count = $this->getCount();
        $fileCount = $this->getCurrentFileCount();
        if(!is_numeric($count)) { $count = 0; }
        if(!is_numeric($fileCount)) { $fileCount = 0; }
        $count += $inc; $fileCount += $inc;
        $this->setData('count', $count);
        $this->setData('current_file_count', $fileCount);
    }

    public function resetFileCount()
    {
        $this->setData('current_file_count', 0);
    }

    public function load($id, $field = null)
    {
        parent::load($id, $field);
        $this->_productsIds = unserialize($this->getProductIds());
        $this->_files = unserialize($this->getFiles());
        $this->_startTime = $this->getStartTime();
    }

    public function save()
    {
        $this->setModifiedTime(time());
        $this->setProductIds(serialize($this->_productsIds));
        $this->setFiles(serialize($this->_files));
        parent::save();
    }

    //removes the next product id for processing.
    public function getNextCachedProductsId()
    {
        if (empty ($this->_productsIds)) {
            return false;
        }

        return array_shift($this->_productsIds);

    }

    protected function vafSitemapRecursiveDelete($str)
    {
        if (is_file($str)) {
            return @unlink($str);
        } elseif (is_dir($str)) {
            $scan = glob(rtrim($str, '/') . '/*');
            foreach ($scan as $index => $path) {
                $this->vafSitemapRecursiveDelete($path);
            }

            return @rmdir($str);
        }
    }
}