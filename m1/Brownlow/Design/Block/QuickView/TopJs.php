<?php
/**
 * Created by PhpStorm.
 * User: jarrod
 * Date: 9/21/16
 * Time: 1:03 PM
 */ 
class Brownlow_Design_Block_QuickView_TopJs extends Smartwave_QuickView_Block_TopJs {
    public function _prepareLayout()
    {
        if (!Mage::getStoreConfig('quickview/general/enableview')) return;
        $layout = $this->getLayout();
        /** @var Smartwave_Porto_Block_Html_Head $head */
        $head = $layout->getBlock('head');

        if (is_object($head)) {
            $head->addJs('smartwave/jquery/jquery-1.12.4.min.js');
            $head->addJs('smartwave/jquery/jquery-noconflict.js');
            $head->addJs('smartwave/jquery/plugins/fancybox/js/jquery.fancybox.js');
            $head->addJs('varien/product.js');
            $head->addJs('varien/configurable.js');
            $head->addJs('calendar/calendar.js');
            $head->addJs('calendar/calendar-setup.js');
            $head->addItem('skin_js', 'js/bundle.js');
            $head->addItem('skin_js', 'quickview/js/sw_quickview.js');
            $head->addItem('skin_css', 'fancybox/css/jquery.fancybox.css');
            $head->addItem('js_css', 'calendar/calendar-win2k-1.css');
            $head->addItem('skin_css', 'quickview/css/styles.css');
        }
        $this->setTemplate('smartwave/quickview/page/lablequickview.phtml');
    }
}