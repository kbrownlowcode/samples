<?php
/**
 * @namespace   Brownlow
 * @module      RewardPointsRule
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        10/26/2016 4:13 PM
 * @brief
 */

/**
 * Class Brownlow_RewardPointsRule_Model_Service_ReindexEarnRules
 */
class Brownlow_RewardPointsRule_Model_Service_ReindexEarnRules
{
    protected $_totalCount = 0;

    /** @var */
    protected $_transaction;

    protected $_rows = array();
    protected $_productIds = array();
    protected $_appliedCount = 0;
    protected $_insertLimit = 2000;
    protected $_timeStart;

    public function execute($scheduler = null)
    {
        $this->_timeStart = microtime(true);
        $this->_indexRules();
        $this->_log('Complete');
    }

    /**
     * @return Mage_Core_Model_Resource_Iterator
     */
    protected function _getIterator()
    {
        return Mage::getSingleton('core/resource_iterator');
    }

    protected function _indexRules()
    {
        Mage::app()->loadAreaPart(
            Mage_Core_Model_App_Area::AREA_FRONTEND, Mage_Core_Model_App_Area::PART_EVENTS
        );
        $productCollection = $this->_getProductCollection();

        $this->_totalCount = $productCollection->getSize();

        $this->_getIterator()->walk(
            $productCollection->getSelect(), array(array($this, 'collectionCallback')), array()
        );

        //make sure remaining have been inserted.
        $this->_insertRows(true);
    }

    /**
     * @param array $args
     */
    public function collectionCallback($args)
    {
        try {
            $row = $args['row'];

            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product')->setData($row);
            $datas   = Mage::getSingleton('rewardpointsrule/indexer_product')
                ->getIndexProduct($product);

            foreach ($datas as $data) {
                $this->_rows[] = $data;
            }
            $this->_productIds[] = $product->getId();

            $this->_insertRows();
        } catch (Exception $e) {
            $this->_log('Exception: '.$e->getMessage().' '.$e->getFile().' '.$e->getLine());
        }

        unset($product);
    }

    /**
     * Insert rows into product table, if force is false it will look at count mod, if true it will insert the array
     *
     * @param bool $force
     *
     * @return $this
     * @throws \Exception
     */
    protected function _insertRows($force = false)
    {
        if (empty($this->_rows)) {
            return $this;
        }

        if ($force === true || count($this->_rows) >= $this->_insertLimit) {
            try {
                $this->_getWriteAdapter()->beginTransaction();
                $this->_clearProducts($this->_productIds);
                $this->_getWriteAdapter()->insertMultiple($this->_getEarnProductTable(), $this->_rows);
                $this->_getWriteAdapter()->commit();

                $this->_appliedCount += count($this->_productIds);
                $this->_log('Progress Update');
            } catch (Exception $e) {
                $this->_getWriteAdapter()->rollback();
                throw $e;
            }

            //now that we inserted, remove records
            $this->_rows = array();

            //clear out the product ids now that they've been inserted.
            $this->_productIds = array();
        }

        return $this;
    }

    /**
     * We only want to clear out the products that are being affected, no reason to delete the entire table.  This would
     * make products not have any reward points while we waited for the system to index.
     *
     * @param array $productIds
     *
     * @return $this
     */
    protected function _clearProducts(array $productIds)
    {
        if (!$productIds) {
            return $this;
        }

        $deleteCondition = $this->_getWriteAdapter()->quoteInto('product_id IN (?)', $productIds);
        $this->_getWriteAdapter()->delete($this->_getEarnProductTable(), $deleteCondition);

        return $this;
    }

    protected function _getProductCollection()
    {
        $productCollection = Mage::getResourceModel('catalog/product_collection');

        // Prepare Catalog Product Collection to used for Rules
        $productCollection
            ->addAttributeToSelect('price', 'left')
            ->addAttributeToSelect('special_price', 'left')
            ->addAttributeToSelect('cost', 'left')
            ->addAttributeToSelect('rewardpoints_spend', 'left')
            ->addAttributeToSelect('tax_class_id', 'left'); //Hai.Tran 14/11/2013

        $productCollection->addPriceData(null,Mage::app()->getDefaultStoreView()->getWebsiteId());

        $rules = Mage::getResourceModel('rewardpointsrule/earning_catalog_collection')
            ->addFieldToFilter('is_active', 1);
        $rules->getSelect()
            ->where("(from_date IS NULL) OR (DATE(from_date) <= ?)", now(true))
            ->where("(to_date IS NULL) OR (DATE(to_date) >= ?)", now(true));
        foreach ($rules as $rule) {
            $rule->afterLoad();
            $rule->getConditions()->collectValidatedAttributes($productCollection);
        }

        return $productCollection;
    }

    /**
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    protected function _getWriteAdapter()
    {
        return $this->_getEarnProductResource()->getWriteAdapter();
    }

    /**
     * @return Magestore_RewardPointsRule_Model_Mysql4_Earning_Product
     */
    protected function _getEarnProductResource()
    {
        return Mage::getResourceSingleton('rewardpointsrule/earning_product');
    }

    /**
     * @return string
     */
    protected function _getEarnProductTable()
    {
        return $this->_getEarnProductResource()->getTable('rewardpointsrule/earning_product');
    }

    /**
     * @param int $insertLimit
     *
     * @return $this
     */
    public function setInsertLimit($insertLimit)
    {
        $this->_insertLimit = $insertLimit;

        return $this;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    protected function _log($message)
    {
        $message .= '| Runtime: ' . $this->_getRunTime() . '.  Processed: ' . $this->_appliedCount . '/'
            . $this->_totalCount.'. Memory: '.$this->getMemoryUsage();

        Mage::log($message, null, 'Brownlow_reindex_rules_cron.log', true);

        return $this;
    }

    /**
     * @return float
     */
    protected function _getRunTime()
    {
        return microtime(true) - $this->_timeStart;
    }

    public function getMemoryUsage()
    {
        $unit = array(
            'b',
            'kb',
            'mb',
            'gb',
            'tb',
            'pb'
        );
        $size = memory_get_usage(true);

        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2).' '.$unit[$i];
    }
}