<?php
/**
 * @namespace   Brownlow
 * @module      RewardPointsRule
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        10/27/2016 10:16 AM
 * @brief
 */

/**
 * Class Brownlow_RewardPointsRule_Model_Service_ManualScheduleIndexJob
 */
class Brownlow_RewardPointsRule_Model_Service_ManualScheduleIndexJob
{
    public function execute()
    {
        foreach ($this->_getCronCodes() as $code) {
            //if it's not false, it's already scheduled or running
            if ($this->_getStatusService()->get() !== false) {
                continue;
            }

            Mage::getModel('cron/schedule')
                ->setJobCode($code)
                ->setScheduledReason(Aoe_Scheduler_Model_Schedule::REASON_SCHEDULENOW_WEB)
                ->schedule()
                ->save();
        }
    }

    protected function _getCronCodes()
    {
        return Mage::helper('Brownlow_rewardpointsrule')->getReindexCronCodes();
    }

    /**
     * @return Brownlow_RewardPointsRule_Model_Service_GetJobStatus
     */
    protected function _getStatusService()
    {
        return Mage::getSingleton('Brownlow_rewardpointsrule/service_getJobStatus');
    }
}