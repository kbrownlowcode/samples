<?php
/**
 * @namespace   Brownlow
 * @module      RewardPointsRule
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        10/27/2016 10:26 AM
 * @brief
 */

/**
 * Class Brownlow_RewardPointsRule_Model_Service_GetJobStatus
 */
class Brownlow_RewardPointsRule_Model_Service_GetJobStatus
{
    /**
     * returns false on scheduled nor running
     *
     * @return bool|string
     */
    public function get()
    {
        foreach ($this->_getCronCodes() as $jobCode) {
            $status = $this->_getJobCodeStatus($jobCode);
            if ($status !== false) {
                return $status;
            }
        }

        return false;
    }

    protected function _getCronCodes()
    {
        return Mage::helper('Brownlow_rewardpointsrule')->getReindexCronCodes();
    }

    protected function _getJobCodeStatus($jobCode)
    {
        $collection = Mage::getModel('cron/schedule')
            ->getCollection()
            ->addFieldToFilter(
                'status', ['in' => [
                    Aoe_Scheduler_Model_Schedule::STATUS_PENDING,
                    Aoe_Scheduler_Model_Schedule::STATUS_RUNNING,
                ]]
            )
            ->addFieldToFilter('job_code', $jobCode);

        foreach ($collection as $schedule) {
            /* @var $schedule Aoe_Scheduler_Model_Schedule */
            if ($schedule->getStatus() === Aoe_Scheduler_Model_Schedule::STATUS_PENDING
                || ($schedule->getStatus() === Aoe_Scheduler_Model_Schedule::STATUS_RUNNING
                    && $schedule->isAlive() !== false)
            ) {
                return $schedule->getStatus();
            }
        }

        return false;
    }
}