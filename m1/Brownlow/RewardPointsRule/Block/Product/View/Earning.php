<?php
/**
 * @namespace   Brownlow
 * @module      RewardPointsRule
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/18/2016 7:07 PM
 */

class Brownlow_RewardPointsRule_Block_Product_View_Earning
    extends Magestore_RewardPointsRule_Block_Product_View_Earning
{
    protected $_configurableEarningValue = null;

    public function getEarningPoints()
    {
        $product = $this->_getProduct();

        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
            return parent::getEarningPoints();
        } else if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
            $value = $this->getEarningPointsForConfigurableProducts();

            if (!is_array($value)) {
                return false;
            }

            return $value['min'];
        }

        return parent::getEarningPoints();
    }

    public function getEarningPointsValue()
    {
        $product = $this->_getProduct();
        $helper  = Mage::helper('rewardpoints/point');

        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
            return $helper->format(parent::getEarningPoints());

        } else if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
            $value = $this->getEarningPointsForConfigurableProducts();

            if (!is_array($value)) {
                return false;
            }

            $minFormatted = $helper->format($value['min']);
            $maxFormatted = $helper->format($value['max']);

            if ($minFormatted == $maxFormatted) {
                return $minFormatted;
            }

            return sprintf(
                'between %d and %d',
                $helper->format($value['min']),
                $helper->format($value['max'])
            );
        }

        return $helper->format(parent::getEarningPoints());
    }

    protected function getEarningPointsForConfigurableProducts()
    {
        if ($this->_configurableEarningValue) {
            return $this->_configurableEarningValue;
        }

        $product = $this->_getProduct();

        $customerGroupId = Mage::getSingleton('customer/session')->getCustomer()->getGroupId();
        $websiteId       = Mage::app()->getStore()->getWebsiteId();
        $date            = null;

        $rules = Mage::getResourceModel('rewardpointsrule/earning_catalog_collection')
            ->setAvailableFilter($customerGroupId, $websiteId, $date);

        foreach ($rules as $rule) {
            $rule->afterLoad();
        }

        $isConfigurableProductValid = false;

        foreach ($rules as $rule) {
            if ($rule->validate($product)) {
                $isConfigurableProductValid = true;
                break;
            }
        }

        if (!$isConfigurableProductValid) {
            $this->_configurableEarningValue = false;
            return $this->_configurableEarningValue;
        }

        $max = null;
        $min = null;

        $simpleProducts = $product->getTypeInstance()->getUsedProducts();

        foreach ($simpleProducts as $simple){
            $points = Mage::helper('rewardpointsrule/calculation_earning')->getCatalogEarningPoints($simple);

            if (!$max || $points > $max) {
                $max = $points;
            }

            if (!$min || $points < $min) {
                $min = $points;
            }
        }

        $this->_configurableEarningValue = array(
            'min' => $min,
            'max' => $max,
        );

        return $this->_configurableEarningValue;
    }

    protected function _getProduct()
    {
        return Mage::registry('product');
    }
}
