<?php
/**
 * @namespace   Brownlow
 * @module      RewardPointsRule
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        10/27/2016 10:10 AM
 * @brief       Change action of reindex button.
 */

/**
 * Class Brownlow_RewardPointsRule_Block_RewardPointsRule_Adminhtml_Earning_Catalog
 */
class Brownlow_RewardPointsRule_Block_RewardPointsRule_Adminhtml_Earning_Catalog
    extends Magestore_RewardPointsRule_Block_Adminhtml_Earning_Catalog
{
    protected function _construct()
    {
        parent::_construct();

        $this->_updateButton('applyRuleLabel','label', $this->__('Schedule Reindex'));
        $this->_updateButton('applyRuleLabel','onclick', 'setLocation(\'' . $this->linkApply() .'\')');
    }
    /**
     * get link show apply rule popup
     *
     * @return string
     */
    public function linkApply() {
        return $this->getUrl('*/adminhtml_earning_catalog/scheduleRuleApply');
    }
}