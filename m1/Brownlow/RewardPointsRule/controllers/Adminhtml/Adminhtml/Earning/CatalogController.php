<?php
 /**
 * @namespace   Brownlow
 * @module      RewardPointsRule
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        10/24/2016 5:31 PM
 * @brief
 */

require_once 'Magestore/RewardPointsRule/controllers/Adminhtml/Earning/CatalogController.php';

/**
 * Class Brownlow_RewardPointsRule_Adminhtml_Adminhtml_Earning_CatalogController
 */
class Brownlow_RewardPointsRule_Adminhtml_Adminhtml_Earning_CatalogController extends Magestore_RewardPointsRule_Adminhtml_Earning_CatalogController
{
    public function scheduleRuleApplyAction()
    {
        $helper = Mage::helper('Brownlow_rewardpointsrule');

        try {
            if ($this->_getStatusService()->get() === false ) {
                Mage::getSingleton('Brownlow_rewardpointsrule/service_manualScheduleIndexJob')->execute();
                $this->_getSession()->addSuccess(
                    $helper->__('Reindex job has been successfully scheduled and will begin shortly.')
                );
            } else {
                $this->_getSession()->addNotice(
                    $helper->__('Job is already running or scheduled to run.  No changes made.')
                );
            }
        } catch (Exception $e) {
            Mage::logException($e);
        } finally {
            $this->_redirect('*/*/index');
        }
    }

    public function indexAction()
    {
        $status = $this->_getStatusService()->get();
        if ($status !== false) {
            $helper = Mage::helper('Brownlow_rewardpointsrule');
            switch ($status) {
                case Aoe_Scheduler_Model_Schedule::STATUS_PENDING:
                    $this->_getSession()->addNotice($helper->__('Reindex job is scheduled to run shortly.'));
                    break;
                case Aoe_Scheduler_Model_Schedule::STATUS_RUNNING:
                default:
                $this->_getSession()->addNotice($helper->__('Reindex job is currently running.'));
                break;
            }
        }

        parent::indexAction();
    }

    /**
     * @return Brownlow_RewardPointsRule_Model_Service_GetJobStatus
     */
    protected function _getStatusService()
    {
        return Mage::getSingleton('Brownlow_rewardpointsrule/service_getJobStatus');
    }

    public function applyRuleAjaxAction()
    {
        parent::applyRuleAjaxAction();

        $page = $this->getRequest()->getParam('page');
        $body = $this->getResponse()->getBody();

        if (!$body) {
            return;
        }

        /*
         * If total applied is true, that means we are on the product count that was applied.
         * spelling errors are intentional, matches parent class logic.
         *
         * This is meant to fix a bug where the total indexed is not being calculated as it runs
         */
        $bodyPieces = explode('-', $body);
        $totalApplied = false;
        foreach ($bodyPieces as $piece) {
            if ($piece == 'continue') {
                $totalApplied = true;
            } elseif ($totalApplied === true) {
                $poductsApplied = $piece;
                $totalApplied = $poductsApplied * $page;
                break;
            }
        }

        if (is_bool($totalApplied)) {
            return;
        } else {
            $this->getResponse()->setBody($totalApplied.$body);
        }
    }
}