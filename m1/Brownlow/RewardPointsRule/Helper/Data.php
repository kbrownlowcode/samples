<?php
/**
 * @namespace   Brownlow
 * @module      RewardPointsRule
 * @author      Kevin Brownlow
 * @email       kevin.brownlow@icloud.com
 * @date        8/18/2016 7:07 PM
 */

/**
 * Class Brownlow_RewardPointsRule_Helper_Data
 */
class Brownlow_RewardPointsRule_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getReindexCronCodes()
    {
        return [
            'Brownlow_rewardearnrules_reindex',
        ];
    }
}
