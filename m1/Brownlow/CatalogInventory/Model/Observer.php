<?php

class Brownlow_CatalogInventory_Model_Observer
{
    public function catalogProductSaveCommitAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_CatalogInventory_Model_Stock_Item $item */
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getProduct();

        $this->updateProductStock($product->getId());
    }

    public function updateProductCron()
    {
        $this->updateProductStock();
    }

    public function updateProductStock($productId = null) {
        /** @var Mage_Core_Model_Resource_Db_Abstract $config */
        $config = Mage::getSingleton('core/resource_config');
        /** @var Mage_Eav_Model_Config $navTable */
        $nav = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'nav_item_status');
        $navTable = $nav->getBackendTable();
        $navAttributeId = $nav->getAttributeId();
        $itemTable = $config->getTable('cataloginventory/stock_item');
        $db = Mage::getSingleton('core/resource')->getConnection('core_write');

        $productCondition = '';
        if ($productId) {
            $productCondition = " AND i.`product_id` = {$productId}";
        }

        $query = <<<MYSQL
              UPDATE {$itemTable} i
              LEFT JOIN {$navTable} v
                ON v.entity_id = i.product_id
                AND v.attribute_id = {$navAttributeId}
              SET i.`is_in_stock` = 1
              WHERE (v.value != '3SELL2ZERO' OR v.value IS NULL){$productCondition};

              UPDATE {$itemTable} i
              INNER JOIN {$navTable} v
                ON v.`entity_id`=i.`product_id` AND v.`attribute_id` = {$navAttributeId}
              SET i.`is_in_stock` = 0
              WHERE v.`value` = '3SELL2ZERO' AND i.`qty` <= 0{$productCondition};
MYSQL;

        $db->query($query);
    }
}
