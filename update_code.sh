#!/bin/bash

cd /var/www/html

sudo chmod +x ./bin/magento

sudo ./bin/magento maintenance:enable
sudo ./bin/magento deploy:mode:set developer

sudo rm -rf pub/static/frontend/* var/view_preprocessed/css/frontend/*

sudo ./bin/magento cache:flush
sudo ./bin/magento cache:clean

sudo ./bin/magento setup:upgrade

sudo ./bin/magento setup:di:compile

sudo ./bin/magento setup:static-content:deploy

sudo ./bin/magento index:reindex

sudo ./bin/magento maintenance:disable

sudo chmod 0755 -R var/ pub/ grocery/
sudo chown www-data:www-data -R .

## Duplicated chmod and chown to avoid issue
sudo chmod 0755 -R var/ pub/ grocery/
sudo chown www-data:www-data -R .